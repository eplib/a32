<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A32265">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>His Majesties gracious speech to both houses of Parliament at their prorogation, Novemb. 4, 1673</title>
    <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A32265 of text R29959 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing C3055). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 3 1-bit group-IV TIFF page images.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A32265</idno>
    <idno type="STC">Wing C3055</idno>
    <idno type="STC">ESTC R29959</idno>
    <idno type="EEBO-CITATION">11228508</idno>
    <idno type="OCLC">ocm 11228508</idno>
    <idno type="VID">46939</idno>
    <idno type="PROQUESTGOID">2240887345</idno>
    <availability>
     <p>This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. Searching, reading, printing, or downloading EEBO-TCP texts is reserved for the authorized users of these project partner institutions. Permission must be granted for subsequent distribution, in print or electronically, of this EEBO-TCP Phase II text, in whole or in part.</p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 2, no. A32265)</note>
    <note>Transcribed from: (Early English Books Online ; image set 46939)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1457:14)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>His Majesties gracious speech to both houses of Parliament at their prorogation, Novemb. 4, 1673</title>
      <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
      <author>Charles II, King of England, 1630-1685.</author>
      <author>England and Wales. Parliament.</author>
     </titleStmt>
     <extent>4 [i.e. 2] p.</extent>
     <publicationStmt>
      <publisher>Printed by the assigns of John Bill and Christopher Barker ...,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1673.</date>
     </publicationStmt>
     <notesStmt>
      <note>Reproduction of the original in the Cambridge University Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Great Britain -- Politics and government -- 1660-1688.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>His Majesties gracious speech to both Houses of Parliament, at their prorogation, November 4. 1673. By His Majesties special command.</ep:title>
    <ep:author>England and Wales. Sovereign </ep:author>
    <ep:publicationYear>1673</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>6</ep:pageCount>
    <ep:wordCount>319</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2009-04</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2009-05</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2009-06</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2009-06</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-09</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A32265-e10010">
  <front xml:id="A32265-e10020">
   <div type="title_page" xml:id="A32265-e10030">
    <pb facs="tcp:46939:1" xml:id="A32265-001-a"/>
    <pb facs="tcp:46939:1" rend="simple:additions" xml:id="A32265-001-b"/>
    <p xml:id="A32265-e10040">
     <w lemma="his" pos="po" xml:id="A32265-001-b-0010">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A32265-001-b-0020">Majesties</w>
     <w lemma="gracious" pos="j" xml:id="A32265-001-b-0030">GRACIOUS</w>
     <w lemma="speech" pos="n1" xml:id="A32265-001-b-0040">SPEECH</w>
     <w lemma="to" pos="acp" xml:id="A32265-001-b-0050">To</w>
     <w lemma="both" pos="d" xml:id="A32265-001-b-0060">both</w>
     <w lemma="house" pos="n2" xml:id="A32265-001-b-0070">Houses</w>
     <w lemma="of" pos="acp" xml:id="A32265-001-b-0080">of</w>
     <w lemma="parliament" pos="n1" xml:id="A32265-001-b-0090">PARLIAMENT</w>
     <pc xml:id="A32265-001-b-0100">,</pc>
     <w lemma="at" pos="acp" xml:id="A32265-001-b-0110">AT</w>
     <w lemma="their" pos="po" xml:id="A32265-001-b-0120">THEIR</w>
     <w lemma="prorogation" pos="n1" xml:id="A32265-001-b-0130">Prorogation</w>
     <pc xml:id="A32265-001-b-0140">,</pc>
     <w lemma="November" pos="nn1" rend="hi" xml:id="A32265-001-b-0150">November</w>
     <w lemma="4." pos="crd" xml:id="A32265-001-b-0160">4.</w>
     <w lemma="1673." pos="crd" xml:id="A32265-001-b-0170">1673.</w>
     <pc unit="sentence" xml:id="A32265-001-b-0180"/>
    </p>
    <figure xml:id="A32265-e10060">
     <head xml:id="A32265-e10070">
      <w lemma="c" pos="sy" xml:id="A32265-001-b-0190">C</w>
      <w lemma="r" pos="sy" xml:id="A32265-001-b-0200">R</w>
     </head>
     <q xml:id="A32265-e10080">
      <w lemma="n/a" pos="ffr" xml:id="A32265-001-b-0210">DIEV</w>
      <w lemma="n/a" pos="ffr" xml:id="A32265-001-b-0220">ET</w>
      <w lemma="n/a" pos="ffr" xml:id="A32265-001-b-0230">MON</w>
      <w lemma="n/a" pos="ffr" xml:id="A32265-001-b-0240">DROIT</w>
     </q>
     <q xml:id="A32265-e10090">
      <w lemma="n/a" pos="ffr" xml:id="A32265-001-b-0250">HONI</w>
      <w lemma="n/a" pos="ffr" xml:id="A32265-001-b-0260">SOIT</w>
      <w lemma="n/a" pos="ffr" xml:id="A32265-001-b-0270">QVI</w>
      <w lemma="n/a" pos="ffr" xml:id="A32265-001-b-0280">MAL</w>
      <w lemma="n/a" pos="ffr" xml:id="A32265-001-b-0290">Y</w>
      <w lemma="n/a" pos="ffr" xml:id="A32265-001-b-0300">PENSE</w>
     </q>
     <figDesc xml:id="A32265-e10100">royal blazon or coat of arms</figDesc>
    </figure>
    <p xml:id="A32265-e10110">
     <w lemma="by" pos="acp" xml:id="A32265-001-b-0310">By</w>
     <w lemma="his" pos="po" xml:id="A32265-001-b-0320">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A32265-001-b-0330">Majesties</w>
     <w lemma="special" pos="j" xml:id="A32265-001-b-0340">special</w>
     <w lemma="command" pos="n1" xml:id="A32265-001-b-0350">Command</w>
     <pc unit="sentence" xml:id="A32265-001-b-0360">.</pc>
    </p>
    <p xml:id="A32265-e10120">
     <w lemma="LONDON" pos="nn1" rend="hi" xml:id="A32265-001-b-0370">LONDON</w>
     <pc xml:id="A32265-001-b-0380">,</pc>
     <w lemma="print" pos="vvn" xml:id="A32265-001-b-0390">Printed</w>
     <w lemma="by" pos="acp" xml:id="A32265-001-b-0400">by</w>
     <w lemma="the" pos="d" xml:id="A32265-001-b-0410">the</w>
     <w lemma="assign" pos="vvz" xml:id="A32265-001-b-0420">Assigns</w>
     <w lemma="of" pos="acp" xml:id="A32265-001-b-0430">of</w>
     <hi xml:id="A32265-e10140">
      <w lemma="John" pos="nn1" xml:id="A32265-001-b-0440">John</w>
      <w lemma="bill" pos="n1" xml:id="A32265-001-b-0450">Bill</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="A32265-001-b-0460">and</w>
     <hi xml:id="A32265-e10150">
      <w lemma="Christopher" pos="nn1" xml:id="A32265-001-b-0470">Christopher</w>
      <w lemma="Barker" pos="nn1" xml:id="A32265-001-b-0480">Barker</w>
     </hi>
     <pc rend="follows-hi" xml:id="A32265-001-b-0490">,</pc>
     <w lemma="printer" pos="n2" xml:id="A32265-001-b-0500">Printers</w>
     <w lemma="to" pos="acp" xml:id="A32265-001-b-0510">to</w>
     <w lemma="the" pos="d" xml:id="A32265-001-b-0520">the</w>
     <w lemma="king" pos="n2" xml:id="A32265-001-b-0530">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A32265-001-b-0540">most</w>
     <w lemma="excellent" pos="j" xml:id="A32265-001-b-0550">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A32265-001-b-0560">Majesty</w>
     <pc xml:id="A32265-001-b-0570">,</pc>
     <w lemma="1673." pos="crd" xml:id="A32265-001-b-0580">1673.</w>
     <pc unit="sentence" xml:id="A32265-001-b-0590"/>
    </p>
    <p xml:id="A32265-e10160">
     <w lemma="cum" pos="crd" reg="CUM" xml:id="A32265-001-b-0600">CVM</w>
     <w lemma="n/a" pos="fla" xml:id="A32265-001-b-0610">PRIVILEGIO</w>
     <pc unit="sentence" xml:id="A32265-001-b-0620">.</pc>
    </p>
    <pb facs="tcp:46939:2" rend="simple:additions" xml:id="A32265-002-a"/>
   </div>
  </front>
  <body xml:id="A32265-e10170">
   <div type="speech" xml:id="A32265-e10180">
    <pb facs="tcp:46939:2" n="3" xml:id="A32265-002-b"/>
    <head xml:id="A32265-e10190">
     <w lemma="his" pos="po" xml:id="A32265-002-b-0010">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A32265-002-b-0020">Majesties</w>
     <w lemma="gracious" pos="j" xml:id="A32265-002-b-0030">GRACIOUS</w>
     <w lemma="speech" pos="n1" xml:id="A32265-002-b-0040">SPEECH</w>
     <w lemma="to" pos="acp" xml:id="A32265-002-b-0050">To</w>
     <w lemma="both" pos="d" xml:id="A32265-002-b-0060">both</w>
     <w lemma="house" pos="n2" xml:id="A32265-002-b-0070">Houses</w>
     <w lemma="of" pos="acp" xml:id="A32265-002-b-0080">of</w>
     <w lemma="parliament" pos="n1" xml:id="A32265-002-b-0090">PARLIAMENT</w>
     <pc xml:id="A32265-002-b-0100">,</pc>
     <hi xml:id="A32265-e10200">
      <w lemma="at" pos="acp" xml:id="A32265-002-b-0110">At</w>
      <w lemma="their" pos="po" xml:id="A32265-002-b-0120">their</w>
      <w lemma="prorogation" pos="n1" xml:id="A32265-002-b-0130">Prorogation</w>
     </hi>
     <pc rend="follows-hi" unit="sentence" xml:id="A32265-002-b-0140">.</pc>
    </head>
    <opener xml:id="A32265-e10210">
     <salute xml:id="A32265-e10220">
      <w lemma="my" pos="po" xml:id="A32265-002-b-0150">My</w>
      <w lemma="lord" pos="n2" xml:id="A32265-002-b-0160">Lords</w>
      <pc xml:id="A32265-002-b-0170">,</pc>
      <w lemma="and" pos="cc" xml:id="A32265-002-b-0180">and</w>
      <w lemma="gentleman" pos="n2" xml:id="A32265-002-b-0190">Gentlemen</w>
      <pc xml:id="A32265-002-b-0200">,</pc>
     </salute>
    </opener>
    <p xml:id="A32265-e10230">
     <w lemma="I" pos="pns" rend="decorinit" xml:id="A32265-002-b-0210">I</w>
     <w lemma="need" pos="vvb" xml:id="A32265-002-b-0220">Need</w>
     <w lemma="not" pos="xx" xml:id="A32265-002-b-0230">not</w>
     <w lemma="tell" pos="vvi" xml:id="A32265-002-b-0240">tell</w>
     <w lemma="you" pos="pn" xml:id="A32265-002-b-0250">you</w>
     <pc xml:id="A32265-002-b-0260">,</pc>
     <w lemma="how" pos="crq" xml:id="A32265-002-b-0270">how</w>
     <w lemma="unwilling" pos="av-j" xml:id="A32265-002-b-0280">unwillingly</w>
     <w lemma="I" pos="pns" xml:id="A32265-002-b-0290">I</w>
     <w lemma="call" pos="vvb" xml:id="A32265-002-b-0300">call</w>
     <w lemma="you" pos="pn" xml:id="A32265-002-b-0310">you</w>
     <w lemma="hither" pos="av" xml:id="A32265-002-b-0320">hither</w>
     <w lemma="at" pos="acp" xml:id="A32265-002-b-0330">at</w>
     <w lemma="this" pos="d" xml:id="A32265-002-b-0340">this</w>
     <w lemma="time" pos="n1" xml:id="A32265-002-b-0350">time</w>
     <pc xml:id="A32265-002-b-0360">,</pc>
     <w lemma="be" pos="vvg" xml:id="A32265-002-b-0370">being</w>
     <w lemma="enough" pos="d" xml:id="A32265-002-b-0380">enough</w>
     <w lemma="sensible" pos="j" xml:id="A32265-002-b-0390">sensible</w>
     <w lemma="what" pos="crq" xml:id="A32265-002-b-0400">what</w>
     <w lemma="advantage" pos="n2" xml:id="A32265-002-b-0410">advantages</w>
     <w lemma="my" pos="po" xml:id="A32265-002-b-0420">My</w>
     <w lemma="enemy" pos="n2" xml:id="A32265-002-b-0430">Enemies</w>
     <w lemma="both" pos="av-d" xml:id="A32265-002-b-0440">both</w>
     <w lemma="abroad" pos="av" xml:id="A32265-002-b-0450">abroad</w>
     <w lemma="and" pos="cc" xml:id="A32265-002-b-0460">and</w>
     <w lemma="at" pos="acp" xml:id="A32265-002-b-0470">at</w>
     <w lemma="home" pos="av-n" xml:id="A32265-002-b-0480">home</w>
     <w lemma="will" pos="vmb" xml:id="A32265-002-b-0490">will</w>
     <w lemma="reap" pos="vvi" xml:id="A32265-002-b-0500">reap</w>
     <w lemma="by" pos="acp" xml:id="A32265-002-b-0510">by</w>
     <w lemma="the" pos="d" xml:id="A32265-002-b-0520">the</w>
     <w lemma="least" pos="ds" xml:id="A32265-002-b-0530">least</w>
     <w lemma="appearance" pos="n1" xml:id="A32265-002-b-0540">appearance</w>
     <w lemma="of" pos="acp" xml:id="A32265-002-b-0550">of</w>
     <w lemma="a" pos="d" xml:id="A32265-002-b-0560">a</w>
     <w lemma="difference" pos="n1" xml:id="A32265-002-b-0570">difference</w>
     <w lemma="betwixt" pos="acp" xml:id="A32265-002-b-0580">betwixt</w>
     <w lemma="i" pos="pno" xml:id="A32265-002-b-0590">Me</w>
     <w lemma="and" pos="cc" xml:id="A32265-002-b-0600">and</w>
     <w lemma="my" pos="po" xml:id="A32265-002-b-0610">My</w>
     <w lemma="parliament" pos="n1" xml:id="A32265-002-b-0620">Parliament</w>
     <pc xml:id="A32265-002-b-0630">;</pc>
     <w lemma="nay" pos="uhx" xml:id="A32265-002-b-0640">nay</w>
     <pc xml:id="A32265-002-b-0650">,</pc>
     <w lemma="be" pos="vvg" xml:id="A32265-002-b-0660">being</w>
     <w lemma="assure" pos="vvn" xml:id="A32265-002-b-0670">assured</w>
     <pc xml:id="A32265-002-b-0680">,</pc>
     <w lemma="they" pos="pns" xml:id="A32265-002-b-0690">they</w>
     <w lemma="expect" pos="vvb" xml:id="A32265-002-b-0700">expect</w>
     <w lemma="more" pos="dc" xml:id="A32265-002-b-0710">more</w>
     <w lemma="success" pos="n1" xml:id="A32265-002-b-0720">success</w>
     <w lemma="from" pos="acp" xml:id="A32265-002-b-0730">from</w>
     <w lemma="such" pos="d" xml:id="A32265-002-b-0740">such</w>
     <w lemma="a" pos="d" xml:id="A32265-002-b-0750">a</w>
     <w lemma="breach" pos="n1" xml:id="A32265-002-b-0760">Breach</w>
     <pc join="right" xml:id="A32265-002-b-0770">(</pc>
     <w lemma="can" pos="vmd" xml:id="A32265-002-b-0780">could</w>
     <w lemma="they" pos="pns" xml:id="A32265-002-b-0790">they</w>
     <w lemma="procure" pos="vvi" xml:id="A32265-002-b-0800">procure</w>
     <w lemma="it" pos="pn" xml:id="A32265-002-b-0810">it</w>
     <pc xml:id="A32265-002-b-0820">)</pc>
     <w lemma="then" pos="av" xml:id="A32265-002-b-0830">then</w>
     <w lemma="from" pos="acp" xml:id="A32265-002-b-0840">from</w>
     <w lemma="their" pos="po" xml:id="A32265-002-b-0850">their</w>
     <w lemma="Arms." pos="nn1" xml:id="A32265-002-b-0860">Arms.</w>
     <pc unit="sentence" xml:id="A32265-002-b-0870"/>
    </p>
    <pb facs="tcp:46939:3" n="4" xml:id="A32265-003-a"/>
    <p xml:id="A32265-e10240">
     <w lemma="this" pos="d" xml:id="A32265-003-a-0010">This</w>
     <pc xml:id="A32265-003-a-0020">,</pc>
     <w lemma="I" pos="pns" xml:id="A32265-003-a-0030">I</w>
     <w lemma="say" pos="vvb" xml:id="A32265-003-a-0040">say</w>
     <pc xml:id="A32265-003-a-0050">,</pc>
     <w lemma="shall" pos="vmb" xml:id="A32265-003-a-0060">shall</w>
     <pc join="right" xml:id="A32265-003-a-0070">(</pc>
     <w lemma="while" pos="cs" reg="whilst" xml:id="A32265-003-a-0080">whilest</w>
     <w lemma="I" pos="pns" xml:id="A32265-003-a-0090">I</w>
     <w lemma="live" pos="vvb" xml:id="A32265-003-a-0100">live</w>
     <pc xml:id="A32265-003-a-0110">)</pc>
     <w lemma="be" pos="vvb" xml:id="A32265-003-a-0120">be</w>
     <w lemma="my" pos="po" xml:id="A32265-003-a-0130">My</w>
     <w lemma="chief" pos="j" xml:id="A32265-003-a-0140">chief</w>
     <w lemma="endeavour" pos="n1" xml:id="A32265-003-a-0150">endeavour</w>
     <w lemma="to" pos="prt" xml:id="A32265-003-a-0160">to</w>
     <w lemma="prevent" pos="vvi" xml:id="A32265-003-a-0170">prevent</w>
     <pc xml:id="A32265-003-a-0180">,</pc>
     <w lemma="and" pos="cc" xml:id="A32265-003-a-0190">and</w>
     <w lemma="for" pos="acp" xml:id="A32265-003-a-0200">for</w>
     <w lemma="that" pos="d" xml:id="A32265-003-a-0210">that</w>
     <w lemma="reason" pos="n1" xml:id="A32265-003-a-0220">reason</w>
     <w lemma="I" pos="pns" xml:id="A32265-003-a-0230">I</w>
     <w lemma="think" pos="vvb" xml:id="A32265-003-a-0240">think</w>
     <w lemma="it" pos="pn" xml:id="A32265-003-a-0250">it</w>
     <w lemma="necessary" pos="j" xml:id="A32265-003-a-0260">necessary</w>
     <w lemma="to" pos="prt" xml:id="A32265-003-a-0270">to</w>
     <w lemma="make" pos="vvi" xml:id="A32265-003-a-0280">make</w>
     <w lemma="a" pos="d" xml:id="A32265-003-a-0290">a</w>
     <w lemma="short" pos="j" xml:id="A32265-003-a-0300">short</w>
     <w lemma="recess" pos="n1" xml:id="A32265-003-a-0310">Recess</w>
     <pc xml:id="A32265-003-a-0320">,</pc>
     <w lemma="that" pos="cs" xml:id="A32265-003-a-0330">that</w>
     <w lemma="all" pos="d" xml:id="A32265-003-a-0340">all</w>
     <w lemma="good" pos="j" xml:id="A32265-003-a-0350">Good</w>
     <w lemma="man" pos="n2" xml:id="A32265-003-a-0360">Men</w>
     <w lemma="may" pos="vmb" xml:id="A32265-003-a-0370">may</w>
     <w lemma="recollect" pos="vvi" xml:id="A32265-003-a-0380">recollect</w>
     <w lemma="themselves" pos="pr" xml:id="A32265-003-a-0390">themselves</w>
     <w lemma="against" pos="acp" xml:id="A32265-003-a-0400">against</w>
     <w lemma="the" pos="d" xml:id="A32265-003-a-0410">the</w>
     <w lemma="next" pos="ord" xml:id="A32265-003-a-0420">next</w>
     <w lemma="meeting" pos="n1" xml:id="A32265-003-a-0430">Meeting</w>
     <pc xml:id="A32265-003-a-0440">,</pc>
     <w lemma="and" pos="cc" xml:id="A32265-003-a-0450">and</w>
     <w lemma="consider" pos="vvb" xml:id="A32265-003-a-0460">consider</w>
     <pc xml:id="A32265-003-a-0470">,</pc>
     <w lemma="whether" pos="cs" xml:id="A32265-003-a-0480">Whether</w>
     <w lemma="the" pos="d" xml:id="A32265-003-a-0490">the</w>
     <w lemma="present" pos="j" xml:id="A32265-003-a-0500">present</w>
     <w lemma="posture" pos="n1" xml:id="A32265-003-a-0510">posture</w>
     <w lemma="of" pos="acp" xml:id="A32265-003-a-0520">of</w>
     <w lemma="affair" pos="n2" xml:id="A32265-003-a-0530">Affairs</w>
     <w lemma="will" pos="vmb" xml:id="A32265-003-a-0540">will</w>
     <w lemma="not" pos="xx" xml:id="A32265-003-a-0550">not</w>
     <w lemma="rather" pos="av" xml:id="A32265-003-a-0560">rather</w>
     <w lemma="require" pos="vvi" xml:id="A32265-003-a-0570">require</w>
     <w lemma="their" pos="po" xml:id="A32265-003-a-0580">their</w>
     <w lemma="application" pos="n2" xml:id="A32265-003-a-0590">applications</w>
     <w lemma="to" pos="acp" xml:id="A32265-003-a-0600">to</w>
     <w lemma="matter" pos="n2" xml:id="A32265-003-a-0610">matters</w>
     <w lemma="of" pos="acp" xml:id="A32265-003-a-0620">of</w>
     <w lemma="religion" pos="n1" xml:id="A32265-003-a-0630">Religion</w>
     <pc xml:id="A32265-003-a-0640">,</pc>
     <w lemma="and" pos="cc" xml:id="A32265-003-a-0650">and</w>
     <w lemma="support" pos="vvb" xml:id="A32265-003-a-0660">Support</w>
     <w lemma="against" pos="acp" xml:id="A32265-003-a-0670">against</w>
     <w lemma="our" pos="po" xml:id="A32265-003-a-0680">Our</w>
     <w lemma="only" pos="j" reg="only" xml:id="A32265-003-a-0690">onely</w>
     <w lemma="competitor" pos="n2" xml:id="A32265-003-a-0700">Competitors</w>
     <w lemma="at" pos="acp" xml:id="A32265-003-a-0710">at</w>
     <w lemma="sea" pos="n1" xml:id="A32265-003-a-0720">Sea</w>
     <pc xml:id="A32265-003-a-0730">,</pc>
     <w lemma="then" pos="av" xml:id="A32265-003-a-0740">then</w>
     <w lemma="to" pos="acp" xml:id="A32265-003-a-0750">to</w>
     <w lemma="thing" pos="n2" xml:id="A32265-003-a-0760">things</w>
     <w lemma="of" pos="acp" xml:id="A32265-003-a-0770">of</w>
     <w lemma="less" pos="dc" xml:id="A32265-003-a-0780">less</w>
     <w lemma="importance" pos="n1" xml:id="A32265-003-a-0790">importance</w>
     <pc xml:id="A32265-003-a-0800">;</pc>
     <w lemma="and" pos="cc" xml:id="A32265-003-a-0810">and</w>
     <w lemma="in" pos="acp" xml:id="A32265-003-a-0820">in</w>
     <w lemma="the" pos="d" xml:id="A32265-003-a-0830">the</w>
     <w lemma="mean" pos="j" xml:id="A32265-003-a-0840">mean</w>
     <w lemma="while" pos="n1" xml:id="A32265-003-a-0850">while</w>
     <pc xml:id="A32265-003-a-0860">,</pc>
     <w lemma="I" pos="pns" xml:id="A32265-003-a-0870">I</w>
     <w lemma="will" pos="vmb" xml:id="A32265-003-a-0880">will</w>
     <w lemma="not" pos="xx" xml:id="A32265-003-a-0890">not</w>
     <w lemma="be" pos="vvi" xml:id="A32265-003-a-0900">be</w>
     <w lemma="want" pos="vvg" xml:id="A32265-003-a-0910">wanting</w>
     <w lemma="to" pos="prt" xml:id="A32265-003-a-0920">to</w>
     <w lemma="let" pos="vvi" xml:id="A32265-003-a-0930">let</w>
     <w lemma="all" pos="d" xml:id="A32265-003-a-0940">all</w>
     <w lemma="my" pos="po" xml:id="A32265-003-a-0950">My</w>
     <w lemma="subject" pos="n2" xml:id="A32265-003-a-0960">Subjects</w>
     <w lemma="see" pos="vvi" xml:id="A32265-003-a-0970">see</w>
     <pc xml:id="A32265-003-a-0980">,</pc>
     <w lemma="that" pos="cs" xml:id="A32265-003-a-0990">that</w>
     <w lemma="no" pos="dx" xml:id="A32265-003-a-1000">no</w>
     <w lemma="care" pos="n1" xml:id="A32265-003-a-1010">care</w>
     <w lemma="can" pos="vmb" xml:id="A32265-003-a-1020">can</w>
     <w lemma="be" pos="vvi" xml:id="A32265-003-a-1030">be</w>
     <w lemma="great" pos="jc" xml:id="A32265-003-a-1040">greater</w>
     <w lemma="than" pos="cs" reg="than" xml:id="A32265-003-a-1050">then</w>
     <w lemma="my" pos="po" xml:id="A32265-003-a-1060">My</w>
     <w lemma="own" pos="d" xml:id="A32265-003-a-1070">Own</w>
     <pc xml:id="A32265-003-a-1080">,</pc>
     <w lemma="in" pos="acp" xml:id="A32265-003-a-1090">in</w>
     <w lemma="the" pos="d" xml:id="A32265-003-a-1100">the</w>
     <w lemma="effectual" pos="j" xml:id="A32265-003-a-1110">effectual</w>
     <w lemma="suppress" pos="n1-vg" xml:id="A32265-003-a-1120">suppressing</w>
     <w lemma="of" pos="acp" xml:id="A32265-003-a-1130">of</w>
     <w lemma="popery" pos="n1" xml:id="A32265-003-a-1140">Popery</w>
     <pc xml:id="A32265-003-a-1150">:</pc>
     <w lemma="and" pos="cc" xml:id="A32265-003-a-1160">And</w>
     <w lemma="it" pos="pn" xml:id="A32265-003-a-1170">it</w>
     <w lemma="shall" pos="vmb" xml:id="A32265-003-a-1180">shall</w>
     <w lemma="be" pos="vvi" xml:id="A32265-003-a-1190">be</w>
     <w lemma="your" pos="po" xml:id="A32265-003-a-1200">your</w>
     <w lemma="fault" pos="n2" xml:id="A32265-003-a-1210">faults</w>
     <pc xml:id="A32265-003-a-1220">,</pc>
     <w lemma="if" pos="cs" xml:id="A32265-003-a-1230">if</w>
     <w lemma="in" pos="acp" xml:id="A32265-003-a-1240">in</w>
     <w lemma="your" pos="po" xml:id="A32265-003-a-1250">your</w>
     <w lemma="several" pos="j" xml:id="A32265-003-a-1260">several</w>
     <w lemma="country" pos="n2" xml:id="A32265-003-a-1270">Countries</w>
     <pc xml:id="A32265-003-a-1280">,</pc>
     <w lemma="the" pos="d" xml:id="A32265-003-a-1290">the</w>
     <w lemma="law" pos="n2" reg="Laws" xml:id="A32265-003-a-1300">Lawes</w>
     <w lemma="be" pos="vvb" xml:id="A32265-003-a-1310">be</w>
     <w lemma="not" pos="xx" xml:id="A32265-003-a-1320">not</w>
     <w lemma="effectual" pos="av-j" xml:id="A32265-003-a-1330">effectually</w>
     <w lemma="execute" pos="vvn" xml:id="A32265-003-a-1340">executed</w>
     <w lemma="against" pos="acp" xml:id="A32265-003-a-1350">against</w>
     <w lemma="the" pos="d" xml:id="A32265-003-a-1360">the</w>
     <w lemma="growth" pos="n1" xml:id="A32265-003-a-1370">Growth</w>
     <w lemma="of" pos="acp" xml:id="A32265-003-a-1380">of</w>
     <w lemma="it" pos="pn" xml:id="A32265-003-a-1390">it</w>
     <pc unit="sentence" xml:id="A32265-003-a-1400">.</pc>
    </p>
    <p xml:id="A32265-e10250">
     <w lemma="I" pos="pns" xml:id="A32265-003-a-1410">I</w>
     <w lemma="will" pos="vmb" xml:id="A32265-003-a-1420">will</w>
     <w lemma="not" pos="xx" xml:id="A32265-003-a-1430">not</w>
     <w lemma="be" pos="vvi" xml:id="A32265-003-a-1440">be</w>
     <w lemma="idle" pos="j" xml:id="A32265-003-a-1450">idle</w>
     <w lemma="neither" pos="avx-d" xml:id="A32265-003-a-1460">neither</w>
     <w lemma="in" pos="acp" xml:id="A32265-003-a-1470">in</w>
     <w lemma="some" pos="d" xml:id="A32265-003-a-1480">some</w>
     <w lemma="other" pos="d" xml:id="A32265-003-a-1490">other</w>
     <w lemma="thing" pos="n2" xml:id="A32265-003-a-1500">things</w>
     <w lemma="which" pos="crq" xml:id="A32265-003-a-1510">which</w>
     <w lemma="may" pos="vmb" xml:id="A32265-003-a-1520">may</w>
     <w lemma="add" pos="vvi" xml:id="A32265-003-a-1530">add</w>
     <w lemma="to" pos="acp" xml:id="A32265-003-a-1540">to</w>
     <w lemma="your" pos="po" xml:id="A32265-003-a-1550">your</w>
     <w lemma="satisfaction" pos="n1" xml:id="A32265-003-a-1560">Satisfaction</w>
     <pc xml:id="A32265-003-a-1570">,</pc>
     <w lemma="and" pos="cc" xml:id="A32265-003-a-1580">and</w>
     <w lemma="then" pos="av" xml:id="A32265-003-a-1590">then</w>
     <w lemma="I" pos="pns" xml:id="A32265-003-a-1600">I</w>
     <w lemma="shall" pos="vmb" xml:id="A32265-003-a-1610">shall</w>
     <w lemma="expect" pos="vvi" xml:id="A32265-003-a-1620">expect</w>
     <w lemma="a" pos="d" xml:id="A32265-003-a-1630">a</w>
     <w lemma="suitable" pos="j" xml:id="A32265-003-a-1640">suitable</w>
     <w lemma="return" pos="n1" xml:id="A32265-003-a-1650">Return</w>
     <w lemma="from" pos="acp" xml:id="A32265-003-a-1660">from</w>
     <w lemma="you" pos="pn" xml:id="A32265-003-a-1670">you</w>
     <pc unit="sentence" xml:id="A32265-003-a-1680">.</pc>
    </p>
    <trailer xml:id="A32265-e10260">
     <w lemma="n/a" pos="fla" xml:id="A32265-003-a-1690">FINIS</w>
     <pc unit="sentence" xml:id="A32265-003-a-1700">.</pc>
    </trailer>
    <pb facs="tcp:46939:3" xml:id="A32265-003-b"/>
   </div>
  </body>
  <back xml:id="A32265-e10010-b"/>
 </text>
</TEI>
