<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A32485">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>By the King, a proclamation for quieting possessions</title>
    <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A32485 of text R33370 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing C3396). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A32485</idno>
    <idno type="STC">Wing C3396</idno>
    <idno type="STC">ESTC R33370</idno>
    <idno type="EEBO-CITATION">13285086</idno>
    <idno type="OCLC">ocm 13285086</idno>
    <idno type="VID">98786</idno>
    <idno type="PROQUESTGOID">2248541561</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A32485)</note>
    <note>Transcribed from: (Early English Books Online ; image set 98786)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1546:27)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>By the King, a proclamation for quieting possessions</title>
      <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
      <author>Charles II, King of England, 1630-1685.</author>
     </titleStmt>
     <extent>1 broadside.</extent>
     <publicationStmt>
      <publisher>Printed by John Bill and Christopher Barker ...,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1660.</date>
     </publicationStmt>
     <notesStmt>
      <note>"Given at the court at Whitehall, the first day of June, in the twelfth year of our reign, 1660."</note>
      <note>Reproduction of original in the Harvard Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Great Britain -- History -- Charles II, 1660-1685.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A32485</ep:tcp>
    <ep:estc> R33370</ep:estc>
    <ep:stc> (Wing C3396). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>By the King. A proclamation for quieting possessions.</ep:title>
    <ep:author>England and Wales. Sovereign</ep:author>
    <ep:publicationYear>1660</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>338</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2003-04</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2003-05</date>
    <label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2003-06</date>
    <label>John Latta</label>
        Sampled and proofread
      </change>
   <change>
    <date>2003-06</date>
    <label>John Latta</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2003-08</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A32485-e10">
  <body xml:id="A32485-e20">
   <pb facs="tcp:98786:1" rend="simple:additions" xml:id="A32485-001-a"/>
   <div type="proclamation" xml:id="A32485-e30">
    <head xml:id="A32485-e40">
     <w lemma="by" pos="acp" xml:id="A32485-001-a-0010">By</w>
     <w lemma="the" pos="d" xml:id="A32485-001-a-0020">the</w>
     <w lemma="king" pos="n1" xml:id="A32485-001-a-0030">King</w>
     <pc unit="sentence" xml:id="A32485-001-a-0040">.</pc>
     <w lemma="a" pos="d" xml:id="A32485-001-a-0050">A</w>
     <w lemma="proclamation" pos="n1" xml:id="A32485-001-a-0060">PROCLAMATION</w>
     <w lemma="for" pos="acp" xml:id="A32485-001-a-0070">For</w>
     <w lemma="quiet" pos="vvg" xml:id="A32485-001-a-0080">Quieting</w>
     <w lemma="possession" pos="n2" xml:id="A32485-001-a-0090">POSSESSIONS</w>
     <pc unit="sentence" xml:id="A32485-001-a-0100">.</pc>
    </head>
    <opener xml:id="A32485-e50">
     <signed xml:id="A32485-e60">
      <w lemma="CHARLES" pos="nn1" xml:id="A32485-001-a-0110">CHARLES</w>
      <w lemma="r." pos="ab" xml:id="A32485-001-a-0120">R.</w>
      <pc unit="sentence" xml:id="A32485-001-a-0130"/>
     </signed>
     <signed xml:id="A32485-e70">
      <w lemma="CHARLES" pos="nn1" rend="decorinit" xml:id="A32485-001-a-0140">CHARLES</w>
      <pc xml:id="A32485-001-a-0150">,</pc>
      <w lemma="by" pos="acp" xml:id="A32485-001-a-0160">By</w>
      <w lemma="the" pos="d" xml:id="A32485-001-a-0170">the</w>
      <w lemma="grace" pos="n1" xml:id="A32485-001-a-0180">Grace</w>
      <w lemma="of" pos="acp" xml:id="A32485-001-a-0190">of</w>
      <w lemma="God" pos="nn1" xml:id="A32485-001-a-0200">God</w>
      <pc xml:id="A32485-001-a-0210">,</pc>
      <w lemma="of" pos="acp" xml:id="A32485-001-a-0220">of</w>
      <w lemma="England" pos="nn1" xml:id="A32485-001-a-0230">England</w>
      <pc xml:id="A32485-001-a-0240">,</pc>
      <w lemma="scotland" pos="nn1" xml:id="A32485-001-a-0250">Scotland</w>
      <pc xml:id="A32485-001-a-0260">,</pc>
      <w lemma="france" pos="nn1" xml:id="A32485-001-a-0270">France</w>
      <pc xml:id="A32485-001-a-0280">,</pc>
      <w lemma="and" pos="cc" rend="hi" xml:id="A32485-001-a-0290">and</w>
      <w lemma="Ireland" pos="nn1" xml:id="A32485-001-a-0300">Ireland</w>
      <pc xml:id="A32485-001-a-0310">,</pc>
      <w lemma="king" pos="n1" xml:id="A32485-001-a-0320">King</w>
      <pc xml:id="A32485-001-a-0330">,</pc>
      <w lemma="defender" pos="n1" xml:id="A32485-001-a-0340">Defender</w>
      <w lemma="of" pos="acp" xml:id="A32485-001-a-0350">of</w>
      <w lemma="the" pos="d" xml:id="A32485-001-a-0360">the</w>
      <w lemma="faith" pos="n1" xml:id="A32485-001-a-0370">Faith</w>
      <pc xml:id="A32485-001-a-0380">,</pc>
      <hi xml:id="A32485-e90">
       <w lemma="etc." pos="ab" xml:id="A32485-001-a-0390">&amp;c.</w>
       <pc unit="sentence" xml:id="A32485-001-a-0400"/>
      </hi>
     </signed>
     <salute xml:id="A32485-e100">
      <w lemma="to" pos="acp" xml:id="A32485-001-a-0410">To</w>
      <w lemma="all" pos="d" xml:id="A32485-001-a-0420">all</w>
      <w lemma="our" pos="po" xml:id="A32485-001-a-0430">Our</w>
      <w lemma="love" pos="j-vg" xml:id="A32485-001-a-0440">loving</w>
      <w lemma="subject" pos="n2" xml:id="A32485-001-a-0450">Subjects</w>
      <w lemma="of" pos="acp" xml:id="A32485-001-a-0460">of</w>
      <w lemma="our" pos="po" xml:id="A32485-001-a-0470">Our</w>
      <w lemma="realm" pos="n1" xml:id="A32485-001-a-0480">Realm</w>
      <w lemma="of" pos="acp" xml:id="A32485-001-a-0490">of</w>
      <hi xml:id="A32485-e110">
       <w lemma="England" pos="nn1" xml:id="A32485-001-a-0500">England</w>
       <pc xml:id="A32485-001-a-0510">,</pc>
      </hi>
      <w lemma="and" pos="cc" xml:id="A32485-001-a-0520">and</w>
      <w lemma="dominion" pos="n1" xml:id="A32485-001-a-0530">Dominion</w>
      <w lemma="of" pos="acp" xml:id="A32485-001-a-0540">of</w>
      <hi xml:id="A32485-e120">
       <w lemma="Wales" pos="nn1" xml:id="A32485-001-a-0550">Wales</w>
       <pc xml:id="A32485-001-a-0560">,</pc>
      </hi>
      <w lemma="greeting" pos="n1" xml:id="A32485-001-a-0570">Greeting</w>
      <pc unit="sentence" xml:id="A32485-001-a-0580">.</pc>
     </salute>
    </opener>
    <p xml:id="A32485-e130">
     <w lemma="we" pos="pns" xml:id="A32485-001-a-0590">We</w>
     <w lemma="take" pos="vvg" xml:id="A32485-001-a-0600">taking</w>
     <w lemma="notice" pos="n1" xml:id="A32485-001-a-0610">notice</w>
     <w lemma="by" pos="acp" xml:id="A32485-001-a-0620">by</w>
     <w lemma="the" pos="d" xml:id="A32485-001-a-0630">the</w>
     <w lemma="information" pos="n1" xml:id="A32485-001-a-0640">Information</w>
     <w lemma="of" pos="acp" xml:id="A32485-001-a-0650">of</w>
     <w lemma="the" pos="d" xml:id="A32485-001-a-0660">the</w>
     <w lemma="lord" pos="n2" xml:id="A32485-001-a-0670">Lords</w>
     <w lemma="and" pos="cc" xml:id="A32485-001-a-0680">and</w>
     <w lemma="commons" pos="n2" xml:id="A32485-001-a-0690">Commons</w>
     <w lemma="now" pos="av" xml:id="A32485-001-a-0700">now</w>
     <w lemma="assemble" pos="vvn" xml:id="A32485-001-a-0710">assembled</w>
     <w lemma="in" pos="acp" xml:id="A32485-001-a-0720">in</w>
     <w lemma="parliament" pos="n1" xml:id="A32485-001-a-0730">Parliament</w>
     <pc xml:id="A32485-001-a-0740">,</pc>
     <w lemma="that" pos="cs" xml:id="A32485-001-a-0750">That</w>
     <w lemma="several" pos="j" xml:id="A32485-001-a-0760">several</w>
     <w lemma="riot" pos="n2" xml:id="A32485-001-a-0770">Riots</w>
     <w lemma="have" pos="vvb" xml:id="A32485-001-a-0780">have</w>
     <w lemma="be" pos="vvn" reg="been" xml:id="A32485-001-a-0790">beén</w>
     <w lemma="commit" pos="vvn" xml:id="A32485-001-a-0800">committed</w>
     <pc xml:id="A32485-001-a-0810">,</pc>
     <w lemma="and" pos="cc" xml:id="A32485-001-a-0820">and</w>
     <w lemma="forcible" pos="j" xml:id="A32485-001-a-0830">forcible</w>
     <w lemma="entry" pos="n2" xml:id="A32485-001-a-0840">Entries</w>
     <w lemma="make" pos="vvn" xml:id="A32485-001-a-0850">made</w>
     <w lemma="upon" pos="acp" xml:id="A32485-001-a-0860">upon</w>
     <w lemma="the" pos="d" xml:id="A32485-001-a-0870">the</w>
     <w lemma="possession" pos="n2" xml:id="A32485-001-a-0880">Possessions</w>
     <w lemma="of" pos="acp" xml:id="A32485-001-a-0890">of</w>
     <w lemma="divers" pos="j" xml:id="A32485-001-a-0900">divers</w>
     <w lemma="of" pos="acp" xml:id="A32485-001-a-0910">of</w>
     <w lemma="our" pos="po" xml:id="A32485-001-a-0920">Our</w>
     <w lemma="subject" pos="n2" xml:id="A32485-001-a-0930">Subjects</w>
     <pc xml:id="A32485-001-a-0940">,</pc>
     <w lemma="as" pos="acp" xml:id="A32485-001-a-0950">as</w>
     <w lemma="well" pos="av" xml:id="A32485-001-a-0960">well</w>
     <w lemma="ecclesiastical" pos="j" xml:id="A32485-001-a-0970">Ecclesiastical</w>
     <w lemma="as" pos="acp" xml:id="A32485-001-a-0980">as</w>
     <w lemma="temporal" pos="j" xml:id="A32485-001-a-0990">Temporal</w>
     <pc xml:id="A32485-001-a-1000">,</pc>
     <w lemma="who" pos="crq" xml:id="A32485-001-a-1010">who</w>
     <w lemma="have" pos="vvb" xml:id="A32485-001-a-1020">have</w>
     <w lemma="be" pos="vvn" reg="been" xml:id="A32485-001-a-1030">beén</w>
     <w lemma="settle" pos="vvn" reg="settled" xml:id="A32485-001-a-1040">setled</w>
     <w lemma="in" pos="acp" xml:id="A32485-001-a-1050">in</w>
     <w lemma="the" pos="d" xml:id="A32485-001-a-1060">the</w>
     <w lemma="say" pos="j-vn" xml:id="A32485-001-a-1070">said</w>
     <w lemma="possession" pos="n2" xml:id="A32485-001-a-1080">Possessions</w>
     <w lemma="by" pos="acp" xml:id="A32485-001-a-1090">by</w>
     <w lemma="any" pos="d" xml:id="A32485-001-a-1100">any</w>
     <w lemma="lawful" pos="j" xml:id="A32485-001-a-1110">lawful</w>
     <w lemma="or" pos="cc" xml:id="A32485-001-a-1120">or</w>
     <w lemma="pretend" pos="j-vn" xml:id="A32485-001-a-1130">pretended</w>
     <w lemma="authority" pos="n1" xml:id="A32485-001-a-1140">Authority</w>
     <pc xml:id="A32485-001-a-1150">,</pc>
     <w lemma="and" pos="cc" xml:id="A32485-001-a-1160">and</w>
     <w lemma="that" pos="cs" xml:id="A32485-001-a-1170">that</w>
     <w lemma="without" pos="acp" xml:id="A32485-001-a-1180">without</w>
     <w lemma="any" pos="d" xml:id="A32485-001-a-1190">any</w>
     <w lemma="order" pos="n1" xml:id="A32485-001-a-1200">Order</w>
     <w lemma="of" pos="acp" xml:id="A32485-001-a-1210">of</w>
     <w lemma="parliament" pos="n1" xml:id="A32485-001-a-1220">Parliament</w>
     <w lemma="or" pos="cc" xml:id="A32485-001-a-1230">or</w>
     <w lemma="legal" pos="j" xml:id="A32485-001-a-1240">Legal</w>
     <w lemma="eviction" pos="n1" xml:id="A32485-001-a-1250">Eviction</w>
     <pc xml:id="A32485-001-a-1260">,</pc>
     <w lemma="to" pos="acp" xml:id="A32485-001-a-1270">to</w>
     <w lemma="the" pos="d" xml:id="A32485-001-a-1280">the</w>
     <w lemma="disturbance" pos="n1" xml:id="A32485-001-a-1290">disturbance</w>
     <w lemma="of" pos="acp" xml:id="A32485-001-a-1300">of</w>
     <w lemma="the" pos="d" xml:id="A32485-001-a-1310">the</w>
     <w lemma="public" pos="j" reg="public" xml:id="A32485-001-a-1320">Publick</w>
     <w lemma="peace" pos="n1" xml:id="A32485-001-a-1330">Peace</w>
     <pc xml:id="A32485-001-a-1340">,</pc>
     <w lemma="while" pos="cs" reg="whilst" xml:id="A32485-001-a-1350">whilest</w>
     <w lemma="these" pos="d" xml:id="A32485-001-a-1360">these</w>
     <w lemma="matter" pos="n2" xml:id="A32485-001-a-1370">Matters</w>
     <w lemma="be" pos="vvb" xml:id="A32485-001-a-1380">are</w>
     <w lemma="under" pos="acp" xml:id="A32485-001-a-1390">under</w>
     <w lemma="the" pos="d" xml:id="A32485-001-a-1400">the</w>
     <w lemma="consideration" pos="n1" xml:id="A32485-001-a-1410">consideration</w>
     <w lemma="of" pos="acp" xml:id="A32485-001-a-1420">of</w>
     <w lemma="our" pos="po" xml:id="A32485-001-a-1430">Our</w>
     <w lemma="parliament" pos="n1" xml:id="A32485-001-a-1440">Parliament</w>
     <pc unit="sentence" xml:id="A32485-001-a-1450">.</pc>
     <w lemma="we" pos="pns" xml:id="A32485-001-a-1460">We</w>
     <w lemma="therefore" pos="av" xml:id="A32485-001-a-1470">therefore</w>
     <pc xml:id="A32485-001-a-1480">,</pc>
     <w lemma="by" pos="acp" xml:id="A32485-001-a-1490">by</w>
     <w lemma="the" pos="d" xml:id="A32485-001-a-1500">the</w>
     <w lemma="advice" pos="n1" xml:id="A32485-001-a-1510">advice</w>
     <w lemma="of" pos="acp" xml:id="A32485-001-a-1520">of</w>
     <w lemma="our" pos="po" xml:id="A32485-001-a-1530">Our</w>
     <w lemma="lord" pos="n2" xml:id="A32485-001-a-1540">Lords</w>
     <w lemma="and" pos="cc" xml:id="A32485-001-a-1550">and</w>
     <w lemma="commons" pos="n2" xml:id="A32485-001-a-1560">Commons</w>
     <w lemma="aforesaid" pos="j" xml:id="A32485-001-a-1570">aforesaid</w>
     <pc xml:id="A32485-001-a-1580">,</pc>
     <w lemma="for" pos="acp" xml:id="A32485-001-a-1590">for</w>
     <w lemma="prevention" pos="n1" xml:id="A32485-001-a-1600">prevention</w>
     <w lemma="of" pos="acp" xml:id="A32485-001-a-1610">of</w>
     <w lemma="the" pos="d" xml:id="A32485-001-a-1620">the</w>
     <w lemma="like" pos="j" xml:id="A32485-001-a-1630">like</w>
     <w lemma="riot" pos="n2" xml:id="A32485-001-a-1640">Riots</w>
     <w lemma="and" pos="cc" xml:id="A32485-001-a-1650">and</w>
     <w lemma="forcible" pos="j" xml:id="A32485-001-a-1660">forcible</w>
     <w lemma="entry" pos="n2" xml:id="A32485-001-a-1670">Entries</w>
     <pc xml:id="A32485-001-a-1680">,</pc>
     <w lemma="and" pos="cc" xml:id="A32485-001-a-1690">and</w>
     <w lemma="preservation" pos="n1" xml:id="A32485-001-a-1700">preservation</w>
     <w lemma="of" pos="acp" xml:id="A32485-001-a-1710">of</w>
     <w lemma="the" pos="d" xml:id="A32485-001-a-1720">the</w>
     <w lemma="public" pos="j" reg="public" xml:id="A32485-001-a-1730">Publick</w>
     <w lemma="peace" pos="n1" xml:id="A32485-001-a-1740">Peace</w>
     <w lemma="of" pos="acp" xml:id="A32485-001-a-1750">of</w>
     <w lemma="this" pos="d" xml:id="A32485-001-a-1760">this</w>
     <w lemma="our" pos="po" xml:id="A32485-001-a-1770">Our</w>
     <w lemma="realm" pos="n1" xml:id="A32485-001-a-1780">Realm</w>
     <pc xml:id="A32485-001-a-1790">,</pc>
     <w lemma="do" pos="vvb" xml:id="A32485-001-a-1800">Do</w>
     <w lemma="by" pos="acp" xml:id="A32485-001-a-1810">by</w>
     <w lemma="this" pos="d" xml:id="A32485-001-a-1820">this</w>
     <w lemma="our" pos="po" xml:id="A32485-001-a-1830">Our</w>
     <w lemma="proclamation" pos="n1" xml:id="A32485-001-a-1840">Proclamation</w>
     <pc xml:id="A32485-001-a-1850">,</pc>
     <w lemma="command" pos="n1" xml:id="A32485-001-a-1860">command</w>
     <pc xml:id="A32485-001-a-1870">,</pc>
     <w lemma="publish" pos="vvb" xml:id="A32485-001-a-1880">publish</w>
     <pc xml:id="A32485-001-a-1890">,</pc>
     <w lemma="and" pos="cc" xml:id="A32485-001-a-1900">and</w>
     <w lemma="declare" pos="vvi" xml:id="A32485-001-a-1910">declare</w>
     <pc xml:id="A32485-001-a-1920">,</pc>
     <w lemma="that" pos="cs" xml:id="A32485-001-a-1930">That</w>
     <w lemma="no" pos="dx" xml:id="A32485-001-a-1940">no</w>
     <w lemma="person" pos="n1" xml:id="A32485-001-a-1950">Person</w>
     <w lemma="or" pos="cc" xml:id="A32485-001-a-1960">or</w>
     <w lemma="person" pos="n2" xml:id="A32485-001-a-1970">Persons</w>
     <pc xml:id="A32485-001-a-1980">,</pc>
     <w lemma="ecclesiastical" pos="j" xml:id="A32485-001-a-1990">Ecclesiastical</w>
     <w lemma="or" pos="cc" xml:id="A32485-001-a-2000">or</w>
     <w lemma="temporal" pos="j" xml:id="A32485-001-a-2010">Temporal</w>
     <pc xml:id="A32485-001-a-2020">,</pc>
     <w lemma="shall" pos="vmb" xml:id="A32485-001-a-2030">shall</w>
     <w lemma="presume" pos="vvi" xml:id="A32485-001-a-2040">presume</w>
     <w lemma="forcible" pos="av-j" xml:id="A32485-001-a-2050">forcibly</w>
     <w lemma="to" pos="prt" xml:id="A32485-001-a-2060">to</w>
     <w lemma="enter" pos="vvi" xml:id="A32485-001-a-2070">enter</w>
     <w lemma="upon" pos="acp" xml:id="A32485-001-a-2080">upon</w>
     <pc xml:id="A32485-001-a-2090">,</pc>
     <w lemma="or" pos="cc" xml:id="A32485-001-a-2100">or</w>
     <w lemma="disturb" pos="vvi" xml:id="A32485-001-a-2110">disturb</w>
     <w lemma="the" pos="d" xml:id="A32485-001-a-2120">the</w>
     <w lemma="say" pos="j-vn" xml:id="A32485-001-a-2130">said</w>
     <w lemma="possession" pos="n2" xml:id="A32485-001-a-2140">Possessions</w>
     <pc xml:id="A32485-001-a-2150">,</pc>
     <w lemma="or" pos="cc" xml:id="A32485-001-a-2160">or</w>
     <w lemma="any" pos="d" xml:id="A32485-001-a-2170">any</w>
     <w lemma="of" pos="acp" xml:id="A32485-001-a-2180">of</w>
     <w lemma="they" pos="pno" xml:id="A32485-001-a-2190">them</w>
     <pc xml:id="A32485-001-a-2200">,</pc>
     <w lemma="till" pos="acp" xml:id="A32485-001-a-2210">till</w>
     <w lemma="our" pos="po" xml:id="A32485-001-a-2220">Our</w>
     <w lemma="parliament" pos="n1" xml:id="A32485-001-a-2230">Parliament</w>
     <w lemma="shall" pos="vmb" xml:id="A32485-001-a-2240">shall</w>
     <w lemma="take" pos="vvi" xml:id="A32485-001-a-2250">take</w>
     <w lemma="order" pos="n1" xml:id="A32485-001-a-2260">order</w>
     <w lemma="therein" pos="av" xml:id="A32485-001-a-2270">therein</w>
     <pc xml:id="A32485-001-a-2280">,</pc>
     <w lemma="or" pos="cc" xml:id="A32485-001-a-2290">or</w>
     <w lemma="a" pos="d" xml:id="A32485-001-a-2300">an</w>
     <w lemma="eviction" pos="n1" xml:id="A32485-001-a-2310">Eviction</w>
     <w lemma="be" pos="vvb" xml:id="A32485-001-a-2320">be</w>
     <w lemma="have" pos="vvn" xml:id="A32485-001-a-2330">had</w>
     <w lemma="by" pos="acp" xml:id="A32485-001-a-2340">by</w>
     <w lemma="due" pos="j" xml:id="A32485-001-a-2350">due</w>
     <w lemma="course" pos="n1" xml:id="A32485-001-a-2360">course</w>
     <w lemma="of" pos="acp" xml:id="A32485-001-a-2370">of</w>
     <w lemma="law" pos="n1" xml:id="A32485-001-a-2380">Law</w>
     <pc unit="sentence" xml:id="A32485-001-a-2390">.</pc>
    </p>
    <p xml:id="A32485-e140">
     <w lemma="and" pos="cc" xml:id="A32485-001-a-2400">And</w>
     <w lemma="all" pos="d" xml:id="A32485-001-a-2410">all</w>
     <w lemma="our" pos="po" xml:id="A32485-001-a-2420">Our</w>
     <w lemma="justice" pos="n2" reg="justices" xml:id="A32485-001-a-2430">Iustices</w>
     <w lemma="of" pos="acp" xml:id="A32485-001-a-2440">of</w>
     <w lemma="the" pos="d" xml:id="A32485-001-a-2450">the</w>
     <w lemma="peace" pos="n1" xml:id="A32485-001-a-2460">Peace</w>
     <pc xml:id="A32485-001-a-2470">,</pc>
     <w lemma="major" pos="n2" xml:id="A32485-001-a-2480">Majors</w>
     <pc xml:id="A32485-001-a-2490">,</pc>
     <w lemma="sheriff" pos="n2" xml:id="A32485-001-a-2500">Sheriffs</w>
     <pc xml:id="A32485-001-a-2510">,</pc>
     <w lemma="and" pos="cc" xml:id="A32485-001-a-2520">and</w>
     <w lemma="other" pos="d" xml:id="A32485-001-a-2530">other</w>
     <w lemma="minister" pos="n2" xml:id="A32485-001-a-2540">Ministers</w>
     <w lemma="of" pos="acp" xml:id="A32485-001-a-2550">of</w>
     <w lemma="justice" pos="n1" reg="justice" xml:id="A32485-001-a-2560">Iustice</w>
     <pc xml:id="A32485-001-a-2570">,</pc>
     <w lemma="and" pos="cc" xml:id="A32485-001-a-2580">and</w>
     <w lemma="all" pos="d" xml:id="A32485-001-a-2590">all</w>
     <w lemma="other" pos="d" xml:id="A32485-001-a-2600">other</w>
     <w lemma="our" pos="po" xml:id="A32485-001-a-2610">Our</w>
     <w lemma="love" pos="j-vg" xml:id="A32485-001-a-2620">loving</w>
     <w lemma="subject" pos="n2" xml:id="A32485-001-a-2630">Subjects</w>
     <pc xml:id="A32485-001-a-2640">,</pc>
     <w lemma="be" pos="vvb" xml:id="A32485-001-a-2650">are</w>
     <w lemma="hereby" pos="av" xml:id="A32485-001-a-2660">hereby</w>
     <w lemma="require" pos="vvn" xml:id="A32485-001-a-2670">required</w>
     <w lemma="to" pos="prt" xml:id="A32485-001-a-2680">to</w>
     <w lemma="be" pos="vvi" xml:id="A32485-001-a-2690">be</w>
     <w lemma="aid" pos="vvg" xml:id="A32485-001-a-2700">aiding</w>
     <w lemma="and" pos="cc" xml:id="A32485-001-a-2710">and</w>
     <w lemma="assist" pos="vvg" xml:id="A32485-001-a-2720">assisting</w>
     <w lemma="in" pos="acp" xml:id="A32485-001-a-2730">in</w>
     <w lemma="the" pos="d" xml:id="A32485-001-a-2740">the</w>
     <w lemma="execution" pos="n1" xml:id="A32485-001-a-2750">Execution</w>
     <w lemma="of" pos="acp" xml:id="A32485-001-a-2760">of</w>
     <w lemma="this" pos="d" xml:id="A32485-001-a-2770">this</w>
     <w lemma="our" pos="po" xml:id="A32485-001-a-2780">Our</w>
     <w lemma="proclamation" pos="n1" xml:id="A32485-001-a-2790">Proclamation</w>
     <pc xml:id="A32485-001-a-2800">,</pc>
     <w lemma="as" pos="acp" xml:id="A32485-001-a-2810">as</w>
     <w lemma="often" pos="av" xml:id="A32485-001-a-2820">often</w>
     <w lemma="as" pos="acp" xml:id="A32485-001-a-2830">as</w>
     <w lemma="occasion" pos="n1" xml:id="A32485-001-a-2840">occasion</w>
     <w lemma="shall" pos="vmb" xml:id="A32485-001-a-2850">shall</w>
     <w lemma="require" pos="vvi" xml:id="A32485-001-a-2860">require</w>
     <pc xml:id="A32485-001-a-2870">,</pc>
     <w lemma="as" pos="acp" xml:id="A32485-001-a-2880">as</w>
     <w lemma="they" pos="pns" xml:id="A32485-001-a-2890">they</w>
     <w lemma="will" pos="vmb" xml:id="A32485-001-a-2900">will</w>
     <w lemma="avoid" pos="vvi" xml:id="A32485-001-a-2910">avoid</w>
     <w lemma="our" pos="po" xml:id="A32485-001-a-2920">Our</w>
     <w lemma="royal" pos="j" xml:id="A32485-001-a-2930">Royal</w>
     <w lemma="displeasure" pos="n1" xml:id="A32485-001-a-2940">displeasure</w>
     <pc unit="sentence" xml:id="A32485-001-a-2950">.</pc>
    </p>
    <closer xml:id="A32485-e150">
     <dateline xml:id="A32485-e160">
      <w lemma="give" pos="vvn" xml:id="A32485-001-a-2960">Given</w>
      <w lemma="at" pos="acp" xml:id="A32485-001-a-2970">at</w>
      <w lemma="our" pos="po" xml:id="A32485-001-a-2980">Our</w>
      <w lemma="court" pos="n1" xml:id="A32485-001-a-2990">Court</w>
      <w lemma="at" pos="acp" xml:id="A32485-001-a-3000">at</w>
      <hi xml:id="A32485-e170">
       <w lemma="Whitehall" pos="nn1" xml:id="A32485-001-a-3010">Whitehall</w>
       <pc xml:id="A32485-001-a-3020">,</pc>
      </hi>
      <date xml:id="A32485-e180">
       <w lemma="the" pos="d" xml:id="A32485-001-a-3030">the</w>
       <w lemma="first" pos="ord" xml:id="A32485-001-a-3040">First</w>
       <w lemma="day" pos="n1" xml:id="A32485-001-a-3050">day</w>
       <w lemma="of" pos="acp" xml:id="A32485-001-a-3060">of</w>
       <hi xml:id="A32485-e190">
        <w lemma="june" pos="nn1" reg="June" xml:id="A32485-001-a-3070">Iune</w>
        <pc xml:id="A32485-001-a-3080">,</pc>
       </hi>
       <w lemma="in" pos="acp" xml:id="A32485-001-a-3090">in</w>
       <w lemma="the" pos="d" xml:id="A32485-001-a-3100">the</w>
       <w lemma="twelve" pos="ord" xml:id="A32485-001-a-3110">Twelfth</w>
       <w lemma="year" pos="n1" xml:id="A32485-001-a-3120">year</w>
       <w lemma="of" pos="acp" xml:id="A32485-001-a-3130">of</w>
       <w lemma="our" pos="po" xml:id="A32485-001-a-3140">Our</w>
       <w lemma="reign" pos="n1" xml:id="A32485-001-a-3150">Reign</w>
       <pc xml:id="A32485-001-a-3160">,</pc>
       <w lemma="1660." pos="crd" xml:id="A32485-001-a-3170">1660.</w>
       <pc unit="sentence" xml:id="A32485-001-a-3180"/>
      </date>
     </dateline>
    </closer>
   </div>
  </body>
  <back xml:id="A32485-e200">
   <div type="colophon" xml:id="A32485-e210">
    <p xml:id="A32485-e220">
     <hi xml:id="A32485-e230">
      <w lemma="LONDON" pos="nn1" xml:id="A32485-001-a-3190">LONDON</w>
      <pc xml:id="A32485-001-a-3200">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A32485-001-a-3210">Printed</w>
     <w lemma="by" pos="acp" xml:id="A32485-001-a-3220">by</w>
     <hi xml:id="A32485-e240">
      <w lemma="John" pos="nn1" reg="John" xml:id="A32485-001-a-3230">Iohn</w>
      <w lemma="bill" pos="nn1" xml:id="A32485-001-a-3240">Bill</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="A32485-001-a-3250">and</w>
     <hi xml:id="A32485-e250">
      <w lemma="Christopher" pos="nn1" xml:id="A32485-001-a-3260">Christopher</w>
      <w lemma="Barker" pos="nn1" xml:id="A32485-001-a-3270">Barker</w>
      <pc xml:id="A32485-001-a-3280">,</pc>
     </hi>
     <w lemma="printer" pos="n2" xml:id="A32485-001-a-3290">Printers</w>
     <w lemma="to" pos="acp" xml:id="A32485-001-a-3300">to</w>
     <w lemma="the" pos="d" xml:id="A32485-001-a-3310">the</w>
     <w lemma="king" pos="n2" xml:id="A32485-001-a-3320">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A32485-001-a-3330">most</w>
     <w lemma="excellent" pos="j" xml:id="A32485-001-a-3340">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A32485-001-a-3350">Majesty</w>
     <pc xml:id="A32485-001-a-3360">,</pc>
     <w lemma="1660." pos="crd" xml:id="A32485-001-a-3370">1660.</w>
     <pc unit="sentence" xml:id="A32485-001-a-3380"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
