<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A32365">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>By the King, a proclamation appointing the general fast which according to former order falleth out to be on Wednesday the first of November, being All Saints Day, to be kept on the Wednesday following, being the eighth of that moneth.</title>
    <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A32365 of text R39950 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing C3232). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A32365</idno>
    <idno type="STC">Wing C3232</idno>
    <idno type="STC">ESTC R39950</idno>
    <idno type="EEBO-CITATION">18570788</idno>
    <idno type="OCLC">ocm 18570788</idno>
    <idno type="VID">108042</idno>
    <idno type="PROQUESTGOID">2240865444</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A32365)</note>
    <note>Transcribed from: (Early English Books Online ; image set 108042)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1647:18)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>By the King, a proclamation appointing the general fast which according to former order falleth out to be on Wednesday the first of November, being All Saints Day, to be kept on the Wednesday following, being the eighth of that moneth.</title>
      <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
      <author>Charles II, King of England, 1630-1685.</author>
     </titleStmt>
     <extent>1 broadside.</extent>
     <publicationStmt>
      <publisher>Printed by John Bill and Christopher Barker ...,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1665.</date>
     </publicationStmt>
     <notesStmt>
      <note>"Given at the court at Oxford the twenty sixth day of September, in the seventeenth year of His Majesties reign. 1665."</note>
      <note>Imperfect: stained, with slight loss of print.</note>
      <note>Reproduction of original in the Cambridge University Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Fasts and feasts -- Church of England.</term>
     <term>Great Britain -- History -- Charles II, 1660-1685.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>By the King. A proclamation appointing the general fast, which according to former order falleth out to be on Wednesday the first of November, being A</ep:title>
    <ep:author>England and Wales. Sovereign </ep:author>
    <ep:publicationYear>1665</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>447</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-03</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-08</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-09</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-09</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A32365-t">
  <body xml:id="A32365-e0">
   <div type="royal_proclamation" xml:id="A32365-e10">
    <pb facs="tcp:108042:1" xml:id="A32365-001-a"/>
    <head xml:id="A32365-e20">
     <figure xml:id="A32365-e30">
      <p xml:id="A32365-e40">
       <w lemma="c" pos="sy" xml:id="A32365-001-a-0010">C</w>
       <w lemma="r" pos="sy" xml:id="A32365-001-a-0020">R</w>
      </p>
      <p xml:id="A32365-e50">
       <w lemma="n/a" pos="ffr" xml:id="A32365-001-a-0030">DIEV</w>
       <w lemma="n/a" pos="ffr" xml:id="A32365-001-a-0040">ET</w>
       <w lemma="n/a" pos="ffr" xml:id="A32365-001-a-0050">MON</w>
       <w lemma="n/a" pos="ffr" xml:id="A32365-001-a-0060">DROIT</w>
      </p>
      <p xml:id="A32365-e60">
       <w lemma="n/a" pos="ffr" xml:id="A32365-001-a-0070">HONI</w>
       <w lemma="n/a" pos="ffr" xml:id="A32365-001-a-0080">SOIT</w>
       <w lemma="n/a" pos="ffr" xml:id="A32365-001-a-0090">QVI</w>
       <w lemma="n/a" pos="ffr" xml:id="A32365-001-a-0100">MAL</w>
       <w lemma="n/a" pos="ffr" xml:id="A32365-001-a-0110">Y</w>
       <w lemma="n/a" pos="ffr" xml:id="A32365-001-a-0120">PENSE</w>
      </p>
      <figDesc xml:id="A32365-e70">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <byline xml:id="A32365-e80">
     <w lemma="by" pos="acp" xml:id="A32365-001-a-0130">By</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-0140">the</w>
     <w lemma="King" pos="n1" xml:id="A32365-001-a-0150">King</w>
     <pc unit="sentence" xml:id="A32365-001-a-0151">.</pc>
    </byline>
    <head xml:id="A32365-e90">
     <w lemma="a" pos="d" xml:id="A32365-001-a-0170">A</w>
     <w lemma="proclamation" pos="n1" xml:id="A32365-001-a-0180">PROCLAMATION</w>
     <w lemma="appoint" pos="vvg" xml:id="A32365-001-a-0190">Appointing</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-0200">the</w>
     <w lemma="general" pos="n1" xml:id="A32365-001-a-0210">General</w>
     <w lemma="fast" pos="av-j" xml:id="A32365-001-a-0220">Fast</w>
     <pc xml:id="A32365-001-a-0230">,</pc>
     <w lemma="which" pos="crq" xml:id="A32365-001-a-0240">which</w>
     <w lemma="according" pos="j" xml:id="A32365-001-a-0250">according</w>
     <w lemma="to" pos="acp" xml:id="A32365-001-a-0260">to</w>
     <w lemma="former" pos="j" xml:id="A32365-001-a-0270">former</w>
     <w lemma="order" pos="n1" xml:id="A32365-001-a-0280">Order</w>
     <w lemma="fall" pos="vvz" xml:id="A32365-001-a-0290">falleth</w>
     <w lemma="out" pos="av" xml:id="A32365-001-a-0300">out</w>
     <w lemma="to" pos="prt" xml:id="A32365-001-a-0310">to</w>
     <w lemma="be" pos="vvi" xml:id="A32365-001-a-0320">be</w>
     <w lemma="on" pos="acp" xml:id="A32365-001-a-0330">on</w>
     <w lemma="Wednesday" pos="nn1" xml:id="A32365-001-a-0340">Wednesday</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-0350">the</w>
     <w lemma="first" pos="ord" xml:id="A32365-001-a-0360">First</w>
     <w lemma="of" pos="acp" xml:id="A32365-001-a-0370">of</w>
     <hi xml:id="A32365-e100">
      <w lemma="November" pos="nn1" xml:id="A32365-001-a-0380">November</w>
      <pc xml:id="A32365-001-a-0390">,</pc>
     </hi>
     <w lemma="be" pos="vvg" xml:id="A32365-001-a-0400">being</w>
     <hi xml:id="A32365-e110">
      <w lemma="all" pos="d" xml:id="A32365-001-a-0410">All</w>
      <w lemma="saint" pos="ng1" reg="Saint's" xml:id="A32365-001-a-0420">Saints</w>
     </hi>
     <w lemma="day" pos="n1" xml:id="A32365-001-a-0430">day</w>
     <pc xml:id="A32365-001-a-0440">,</pc>
     <w lemma="to" pos="prt" xml:id="A32365-001-a-0450">to</w>
     <w lemma="be" pos="vvi" xml:id="A32365-001-a-0460">be</w>
     <w lemma="keep" pos="vvn" xml:id="A32365-001-a-0470">kept</w>
     <w lemma="on" pos="acp" xml:id="A32365-001-a-0480">on</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-0490">the</w>
     <w lemma="Wednesday" pos="nn1" xml:id="A32365-001-a-0500">Wednesday</w>
     <w lemma="follow" pos="vvg" xml:id="A32365-001-a-0510">following</w>
     <pc xml:id="A32365-001-a-0520">,</pc>
     <w lemma="be" pos="vvg" xml:id="A32365-001-a-0530">being</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-0540">the</w>
     <w lemma="eight" pos="ord" xml:id="A32365-001-a-0550">Eighth</w>
     <w lemma="of" pos="acp" xml:id="A32365-001-a-0560">of</w>
     <w lemma="that" pos="d" xml:id="A32365-001-a-0570">that</w>
     <w lemma="month" pos="n1" reg="Month" xml:id="A32365-001-a-0580">Moneth</w>
     <pc unit="sentence" xml:id="A32365-001-a-0590">.</pc>
    </head>
    <opener xml:id="A32365-e120">
     <signed xml:id="A32365-e130">
      <w lemma="CHARLES" pos="nn1" xml:id="A32365-001-a-0600">CHARLES</w>
      <w lemma="r." pos="ab" xml:id="A32365-001-a-0610">R.</w>
      <pc unit="sentence" xml:id="A32365-001-a-0620"/>
     </signed>
    </opener>
    <p xml:id="A32365-e140">
     <w lemma="whereas" pos="cs" rend="decorinit" xml:id="A32365-001-a-0630">WHereas</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-0640">the</w>
     <w lemma="king" pos="n2" xml:id="A32365-001-a-0650">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A32365-001-a-0660">most</w>
     <w lemma="excellent" pos="j" xml:id="A32365-001-a-0670">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A32365-001-a-0680">Majesty</w>
     <w lemma="do" pos="vvd" xml:id="A32365-001-a-0690">did</w>
     <w lemma="by" pos="acp" xml:id="A32365-001-a-0700">by</w>
     <w lemma="his" pos="po" xml:id="A32365-001-a-0710">His</w>
     <w lemma="royal" pos="j" xml:id="A32365-001-a-0720">Royal</w>
     <w lemma="proclamation" pos="n1" xml:id="A32365-001-a-0730">Proclamation</w>
     <pc xml:id="A32365-001-a-0740">,</pc>
     <w lemma="bear" pos="vvg" xml:id="A32365-001-a-0750">bearing</w>
     <w lemma="date" pos="n1" xml:id="A32365-001-a-0760">Date</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-0770">the</w>
     <w lemma="six" pos="ord" xml:id="A32365-001-a-0780">Sixth</w>
     <w lemma="day" pos="n1" xml:id="A32365-001-a-0790">day</w>
     <w lemma="of" pos="acp" xml:id="A32365-001-a-0800">of</w>
     <w lemma="July" pos="nn1" rend="hi" xml:id="A32365-001-a-0810">July</w>
     <w lemma="last" pos="ord" xml:id="A32365-001-a-0820">last</w>
     <pc xml:id="A32365-001-a-0830">,</pc>
     <w lemma="appoint" pos="vvb" xml:id="A32365-001-a-0840">appoint</w>
     <w lemma="that" pos="cs" xml:id="A32365-001-a-0850">that</w>
     <w lemma="from" pos="acp" xml:id="A32365-001-a-0860">from</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-0870">the</w>
     <w lemma="time" pos="n1" xml:id="A32365-001-a-0880">time</w>
     <w lemma="therein" pos="av" xml:id="A32365-001-a-0890">therein</w>
     <w lemma="mention" pos="vvn" xml:id="A32365-001-a-0900">mentioned</w>
     <pc xml:id="A32365-001-a-0910">,</pc>
     <w lemma="the" pos="d" xml:id="A32365-001-a-0920">the</w>
     <w lemma="first" pos="ord" xml:id="A32365-001-a-0930">First</w>
     <w lemma="Wednesday" pos="nn1" xml:id="A32365-001-a-0940">Wednesday</w>
     <w lemma="of" pos="acp" xml:id="A32365-001-a-0950">of</w>
     <w lemma="every" pos="d" xml:id="A32365-001-a-0960">every</w>
     <w lemma="month" pos="n1" reg="Month" xml:id="A32365-001-a-0970">Moneth</w>
     <w lemma="successive" pos="av-j" xml:id="A32365-001-a-0980">successively</w>
     <w lemma="shall" pos="vmd" xml:id="A32365-001-a-0990">should</w>
     <w lemma="be" pos="vvi" xml:id="A32365-001-a-1000">be</w>
     <w lemma="observe" pos="vvn" xml:id="A32365-001-a-1010">observed</w>
     <w lemma="and" pos="cc" xml:id="A32365-001-a-1020">and</w>
     <w lemma="keep" pos="vvn" xml:id="A32365-001-a-1030">kept</w>
     <w lemma="in" pos="acp" xml:id="A32365-001-a-1040">in</w>
     <w lemma="all" pos="d" xml:id="A32365-001-a-1050">all</w>
     <w lemma="part" pos="n2" xml:id="A32365-001-a-1060">parts</w>
     <w lemma="of" pos="acp" xml:id="A32365-001-a-1070">of</w>
     <w lemma="this" pos="d" xml:id="A32365-001-a-1080">this</w>
     <w lemma="realm" pos="n1" xml:id="A32365-001-a-1090">Realm</w>
     <pc xml:id="A32365-001-a-1100">,</pc>
     <w lemma="as" pos="acp" xml:id="A32365-001-a-1110">as</w>
     <w lemma="a" pos="d" xml:id="A32365-001-a-1120">a</w>
     <w lemma="day" pos="n1" xml:id="A32365-001-a-1130">day</w>
     <w lemma="of" pos="acp" xml:id="A32365-001-a-1140">of</w>
     <w lemma="fast" pos="vvg" xml:id="A32365-001-a-1150">Fasting</w>
     <w lemma="and" pos="cc" xml:id="A32365-001-a-1160">and</w>
     <w lemma="humiliation" pos="n1" xml:id="A32365-001-a-1170">Humiliation</w>
     <pc xml:id="A32365-001-a-1180">,</pc>
     <w lemma="until" pos="acp" xml:id="A32365-001-a-1190">until</w>
     <w lemma="it" pos="pn" xml:id="A32365-001-a-1200">it</w>
     <w lemma="shall" pos="vmb" xml:id="A32365-001-a-1210">shall</w>
     <w lemma="please" pos="vvi" xml:id="A32365-001-a-1220">please</w>
     <w lemma="God" pos="nn1" xml:id="A32365-001-a-1230">God</w>
     <w lemma="to" pos="prt" xml:id="A32365-001-a-1240">to</w>
     <w lemma="withdraw" pos="vvi" xml:id="A32365-001-a-1250">withdraw</w>
     <w lemma="this" pos="d" xml:id="A32365-001-a-1260">this</w>
     <w lemma="plague" pos="n1" xml:id="A32365-001-a-1270">Plague</w>
     <w lemma="and" pos="cc" xml:id="A32365-001-a-1280">and</w>
     <w lemma="grievous" pos="j" xml:id="A32365-001-a-1290">grievous</w>
     <w lemma="sickness" pos="n1" xml:id="A32365-001-a-1300">Sickness</w>
     <pc unit="sentence" xml:id="A32365-001-a-1310">.</pc>
     <w lemma="and" pos="cc" xml:id="A32365-001-a-1320">And</w>
     <w lemma="to" pos="acp" xml:id="A32365-001-a-1330">to</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-1340">the</w>
     <w lemma="end" pos="n1" xml:id="A32365-001-a-1350">end</w>
     <w lemma="that" pos="cs" xml:id="A32365-001-a-1360">that</w>
     <w lemma="prayer" pos="n2" xml:id="A32365-001-a-1370">Prayers</w>
     <w lemma="and" pos="cc" xml:id="A32365-001-a-1380">and</w>
     <w lemma="supplication" pos="n1" xml:id="A32365-001-a-1390">Supplication</w>
     <w lemma="may" pos="vmb" xml:id="A32365-001-a-1400">may</w>
     <w lemma="every" pos="d" xml:id="A32365-001-a-1410">every</w>
     <w lemma="where" pos="crq" xml:id="A32365-001-a-1420">where</w>
     <w lemma="be" pos="vvb" xml:id="A32365-001-a-1430">be</w>
     <w lemma="offer" pos="vvn" xml:id="A32365-001-a-1440">offered</w>
     <w lemma="up" pos="acp" xml:id="A32365-001-a-1450">up</w>
     <w lemma="unto" pos="acp" xml:id="A32365-001-a-1460">unto</w>
     <w lemma="almighty" pos="j" xml:id="A32365-001-a-1470">Almighty</w>
     <w lemma="God" pos="nn1" xml:id="A32365-001-a-1480">God</w>
     <w lemma="for" pos="acp" xml:id="A32365-001-a-1490">for</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-1500">the</w>
     <w lemma="removal" pos="n1" xml:id="A32365-001-a-1510">Removal</w>
     <w lemma="of" pos="acp" xml:id="A32365-001-a-1520">of</w>
     <w lemma="this" pos="d" xml:id="A32365-001-a-1530">this</w>
     <w lemma="heavy" pos="j" xml:id="A32365-001-a-1540">heavy</w>
     <w lemma="judgement" pos="n1" reg="judgement" xml:id="A32365-001-a-1550">Iudgment</w>
     <pc xml:id="A32365-001-a-1560">:</pc>
     <w lemma="and" pos="cc" xml:id="A32365-001-a-1570">And</w>
     <w lemma="whereas" pos="cs" xml:id="A32365-001-a-1580">whereas</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-1590">the</w>
     <w lemma="first" pos="ord" xml:id="A32365-001-a-1600">First</w>
     <w lemma="Wednesday" pos="nn1" xml:id="A32365-001-a-1610">Wednesday</w>
     <w lemma="in" pos="acp" xml:id="A32365-001-a-1620">in</w>
     <w lemma="November" pos="nn1" rend="hi" xml:id="A32365-001-a-1630">November</w>
     <pc join="right" xml:id="A32365-001-a-1640">(</pc>
     <w lemma="which" pos="crq" xml:id="A32365-001-a-1650">which</w>
     <w lemma="according" pos="j" xml:id="A32365-001-a-1660">according</w>
     <w lemma="to" pos="acp" xml:id="A32365-001-a-1670">to</w>
     <w lemma="that" pos="d" xml:id="A32365-001-a-1680">that</w>
     <w lemma="order" pos="n1" xml:id="A32365-001-a-1690">Order</w>
     <w lemma="ought" pos="vmd" xml:id="A32365-001-a-1700">ought</w>
     <w lemma="to" pos="prt" xml:id="A32365-001-a-1710">to</w>
     <w lemma="be" pos="vvi" xml:id="A32365-001-a-1720">be</w>
     <w lemma="keep" pos="vvn" xml:id="A32365-001-a-1730">kept</w>
     <pc xml:id="A32365-001-a-1740">)</pc>
     <w lemma="fall" pos="vvz" xml:id="A32365-001-a-1750">falls</w>
     <w lemma="out" pos="av" xml:id="A32365-001-a-1760">out</w>
     <w lemma="to" pos="prt" xml:id="A32365-001-a-1770">to</w>
     <w lemma="be" pos="vvi" xml:id="A32365-001-a-1780">be</w>
     <w lemma="all-saint" pos="n2" rend="hi" xml:id="A32365-001-a-1790">All-Saints</w>
     <w lemma="day" pos="n1" xml:id="A32365-001-a-1800">day</w>
     <pc xml:id="A32365-001-a-1810">,</pc>
     <w lemma="which" pos="crq" xml:id="A32365-001-a-1820">which</w>
     <w lemma="be" pos="vvz" xml:id="A32365-001-a-1830">is</w>
     <w lemma="a" pos="d" xml:id="A32365-001-a-1840">a</w>
     <w lemma="great" pos="j" xml:id="A32365-001-a-1850">great</w>
     <w lemma="festival" pos="n1" xml:id="A32365-001-a-1860">Festival</w>
     <w lemma="in" pos="acp" xml:id="A32365-001-a-1870">in</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-1880">the</w>
     <w lemma="church" pos="n1" xml:id="A32365-001-a-1890">Church</w>
     <pc xml:id="A32365-001-a-1900">,</pc>
     <w lemma="and" pos="cc" xml:id="A32365-001-a-1910">and</w>
     <w lemma="so" pos="av" xml:id="A32365-001-a-1920">so</w>
     <w lemma="not" pos="xx" xml:id="A32365-001-a-1930">not</w>
     <w lemma="fit" pos="j" xml:id="A32365-001-a-1940">fit</w>
     <w lemma="to" pos="prt" xml:id="A32365-001-a-1950">to</w>
     <w lemma="be" pos="vvi" xml:id="A32365-001-a-1960">be</w>
     <w lemma="keep" pos="vvn" xml:id="A32365-001-a-1970">kept</w>
     <w lemma="as" pos="acp" xml:id="A32365-001-a-1980">as</w>
     <w lemma="a" pos="d" xml:id="A32365-001-a-1990">a</w>
     <w lemma="day" pos="n1" xml:id="A32365-001-a-2000">day</w>
     <w lemma="of" pos="acp" xml:id="A32365-001-a-2010">of</w>
     <w lemma="fast" pos="vvg" xml:id="A32365-001-a-2020">Fasting</w>
     <w lemma="and" pos="cc" xml:id="A32365-001-a-2030">and</w>
     <w lemma="humiliation" pos="n1" xml:id="A32365-001-a-2040">Humiliation</w>
     <pc xml:id="A32365-001-a-2050">,</pc>
     <w lemma="his" pos="po" xml:id="A32365-001-a-2060">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A32365-001-a-2070">Majesties</w>
     <w lemma="pleasure" pos="n1" xml:id="A32365-001-a-2080">Pleasure</w>
     <w lemma="be" pos="vvz" xml:id="A32365-001-a-2090">is</w>
     <pc xml:id="A32365-001-a-2100">,</pc>
     <w lemma="and" pos="cc" xml:id="A32365-001-a-2110">and</w>
     <w lemma="he" pos="pns" xml:id="A32365-001-a-2120">He</w>
     <w lemma="do" pos="vvz" xml:id="A32365-001-a-2130">doth</w>
     <w lemma="hereby" pos="av" xml:id="A32365-001-a-2140">hereby</w>
     <w lemma="declare" pos="vvi" xml:id="A32365-001-a-2150">Declare</w>
     <pc xml:id="A32365-001-a-2160">,</pc>
     <w lemma="that" pos="cs" xml:id="A32365-001-a-2170">That</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-2180">the</w>
     <w lemma="next" pos="ord" xml:id="A32365-001-a-2190">Next</w>
     <w lemma="Wednesday" pos="nn1" xml:id="A32365-001-a-2200">Wednesday</w>
     <w lemma="follow" pos="vvg" xml:id="A32365-001-a-2210">following</w>
     <w lemma="in" pos="acp" xml:id="A32365-001-a-2220">in</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-2230">the</w>
     <w lemma="say" pos="j-vn" xml:id="A32365-001-a-2240">said</w>
     <w lemma="month" pos="n1" reg="Month" xml:id="A32365-001-a-2250">Moneth</w>
     <pc xml:id="A32365-001-a-2260">,</pc>
     <w lemma="that" pos="cs" xml:id="A32365-001-a-2270">that</w>
     <w lemma="be" pos="vvz" xml:id="A32365-001-a-2280">is</w>
     <w lemma="to" pos="prt" xml:id="A32365-001-a-2290">to</w>
     <w lemma="say" pos="vvi" xml:id="A32365-001-a-2300">say</w>
     <pc xml:id="A32365-001-a-2310">,</pc>
     <w lemma="Wednesday" pos="nn1" xml:id="A32365-001-a-2320">Wednesday</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-2330">the</w>
     <w lemma="eight" pos="ord" xml:id="A32365-001-a-2340">eighth</w>
     <w lemma="of" pos="acp" xml:id="A32365-001-a-2350">of</w>
     <hi xml:id="A32365-e180">
      <w lemma="November" pos="nn1" xml:id="A32365-001-a-2360">November</w>
      <pc xml:id="A32365-001-a-2370">,</pc>
     </hi>
     <w lemma="shall" pos="vmb" xml:id="A32365-001-a-2380">shall</w>
     <w lemma="be" pos="vvi" xml:id="A32365-001-a-2390">be</w>
     <w lemma="keep" pos="vvn" xml:id="A32365-001-a-2400">kept</w>
     <w lemma="in" pos="acp" xml:id="A32365-001-a-2410">in</w>
     <w lemma="all" pos="d" xml:id="A32365-001-a-2420">all</w>
     <w lemma="part" pos="n2" xml:id="A32365-001-a-2430">parts</w>
     <w lemma="of" pos="acp" xml:id="A32365-001-a-2440">of</w>
     <w lemma="this" pos="d" xml:id="A32365-001-a-2450">this</w>
     <w lemma="realm" pos="n1" xml:id="A32365-001-a-2460">Realm</w>
     <w lemma="as" pos="acp" xml:id="A32365-001-a-2470">as</w>
     <w lemma="a" pos="d" xml:id="A32365-001-a-2480">a</w>
     <w lemma="day" pos="n1" xml:id="A32365-001-a-2490">day</w>
     <w lemma="of" pos="acp" xml:id="A32365-001-a-2500">of</w>
     <w lemma="fast" pos="vvg" xml:id="A32365-001-a-2510">Fasting</w>
     <w lemma="and" pos="cc" xml:id="A32365-001-a-2520">and</w>
     <w lemma="humiliation" pos="n1" xml:id="A32365-001-a-2530">Humiliation</w>
     <pc xml:id="A32365-001-a-2540">,</pc>
     <w lemma="instead" pos="av" xml:id="A32365-001-a-2550">instead</w>
     <w lemma="of" pos="acp" xml:id="A32365-001-a-2560">of</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-2570">the</w>
     <w lemma="first" pos="ord" xml:id="A32365-001-a-2580">First</w>
     <w lemma="Wednesday" pos="nn1" xml:id="A32365-001-a-2590">Wednesday</w>
     <w lemma="of" pos="acp" xml:id="A32365-001-a-2600">of</w>
     <w lemma="that" pos="d" xml:id="A32365-001-a-2610">that</w>
     <w lemma="month" pos="n1" reg="Month" xml:id="A32365-001-a-2620">Moneth</w>
     <pc unit="sentence" xml:id="A32365-001-a-2630">.</pc>
     <w lemma="and" pos="cc" xml:id="A32365-001-a-2640">And</w>
     <w lemma="for" pos="acp" xml:id="A32365-001-a-2650">for</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-2660">the</w>
     <w lemma="time" pos="n1" xml:id="A32365-001-a-2670">time</w>
     <w lemma="to" pos="prt" xml:id="A32365-001-a-2680">to</w>
     <w lemma="come" pos="vvi" xml:id="A32365-001-a-2690">come</w>
     <pc xml:id="A32365-001-a-2700">,</pc>
     <w lemma="the" pos="d" xml:id="A32365-001-a-2710">the</w>
     <w lemma="first" pos="ord" xml:id="A32365-001-a-2720">First</w>
     <w lemma="Wednesday" pos="nn1" xml:id="A32365-001-a-2730">Wednesday</w>
     <w lemma="in" pos="acp" xml:id="A32365-001-a-2740">in</w>
     <w lemma="every" pos="d" xml:id="A32365-001-a-2750">every</w>
     <w lemma="month" pos="n1" reg="Month" xml:id="A32365-001-a-2760">Moneth</w>
     <w lemma="shall" pos="vmb" xml:id="A32365-001-a-2770">shall</w>
     <w lemma="be" pos="vvi" xml:id="A32365-001-a-2780">be</w>
     <w lemma="so" pos="av" xml:id="A32365-001-a-2790">so</w>
     <w lemma="keep" pos="vvn" xml:id="A32365-001-a-2800">kept</w>
     <w lemma="as" pos="acp" xml:id="A32365-001-a-2810">as</w>
     <w lemma="be" pos="vvd" xml:id="A32365-001-a-2820">was</w>
     <w lemma="appoint" pos="vvn" xml:id="A32365-001-a-2830">appointed</w>
     <w lemma="by" pos="acp" xml:id="A32365-001-a-2840">by</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-2850">the</w>
     <w lemma="say" pos="vvd" xml:id="A32365-001-a-2860">said</w>
     <w lemma="former" pos="j" xml:id="A32365-001-a-2870">former</w>
     <w lemma="proclamation" pos="n1" xml:id="A32365-001-a-2880">Proclamation</w>
     <pc xml:id="A32365-001-a-2890">;</pc>
     <w lemma="except" pos="acp" xml:id="A32365-001-a-2900">Except</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-2910">the</w>
     <w lemma="same" pos="d" xml:id="A32365-001-a-2920">same</w>
     <w lemma="fall" pos="vvz" xml:id="A32365-001-a-2930">falls</w>
     <w lemma="out" pos="av" xml:id="A32365-001-a-2940">out</w>
     <w lemma="to" pos="prt" xml:id="A32365-001-a-2950">to</w>
     <w lemma="be" pos="vvi" xml:id="A32365-001-a-2960">be</w>
     <w lemma="on" pos="acp" xml:id="A32365-001-a-2970">on</w>
     <w lemma="some" pos="d" xml:id="A32365-001-a-2980">some</w>
     <w lemma="day" pos="n1" xml:id="A32365-001-a-2990">day</w>
     <w lemma="appoint" pos="vvn" xml:id="A32365-001-a-3000">appointed</w>
     <w lemma="to" pos="prt" xml:id="A32365-001-a-3010">to</w>
     <w lemma="be" pos="vvi" xml:id="A32365-001-a-3020">be</w>
     <w lemma="keep" pos="vvn" xml:id="A32365-001-a-3030">kept</w>
     <w lemma="holy" pos="j" xml:id="A32365-001-a-3040">Holy</w>
     <pc xml:id="A32365-001-a-3050">,</pc>
     <w lemma="and" pos="cc" xml:id="A32365-001-a-3060">and</w>
     <w lemma="in" pos="acp" xml:id="A32365-001-a-3070">in</w>
     <w lemma="that" pos="d" xml:id="A32365-001-a-3080">that</w>
     <w lemma="case" pos="n1" xml:id="A32365-001-a-3090">case</w>
     <w lemma="it" pos="pn" xml:id="A32365-001-a-3100">it</w>
     <w lemma="shall" pos="vmb" xml:id="A32365-001-a-3110">shall</w>
     <w lemma="be" pos="vvi" xml:id="A32365-001-a-3120">be</w>
     <w lemma="keep" pos="vvn" xml:id="A32365-001-a-3130">kept</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-3140">the</w>
     <w lemma="Wednesday" pos="nn1" xml:id="A32365-001-a-3150">Wednesday</w>
     <w lemma="follow" pos="vvg" xml:id="A32365-001-a-3160">following</w>
     <pc xml:id="A32365-001-a-3170">,</pc>
     <w lemma="as" pos="acp" xml:id="A32365-001-a-3180">as</w>
     <w lemma="be" pos="vvz" xml:id="A32365-001-a-3190">is</w>
     <w lemma="hereby" pos="av" xml:id="A32365-001-a-3200">hereby</w>
     <w lemma="direct" pos="vvn" xml:id="A32365-001-a-3210">directed</w>
     <pc unit="sentence" xml:id="A32365-001-a-3220">.</pc>
     <w lemma="and" pos="cc" xml:id="A32365-001-a-3230">And</w>
     <w lemma="his" pos="po" xml:id="A32365-001-a-3240">His</w>
     <w lemma="majesty" pos="n1" xml:id="A32365-001-a-3250">Majesty</w>
     <w lemma="do" pos="vvz" xml:id="A32365-001-a-3260">doth</w>
     <w lemma="hereby" pos="av" xml:id="A32365-001-a-3270">hereby</w>
     <w lemma="again" pos="av" xml:id="A32365-001-a-3280">again</w>
     <w lemma="call" pos="vvi" xml:id="A32365-001-a-3290">call</w>
     <w lemma="upon" pos="acp" xml:id="A32365-001-a-3300">upon</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-3310">the</w>
     <w lemma="respective" pos="j" xml:id="A32365-001-a-3320">Respective</w>
     <w lemma="preacher" pos="n2" xml:id="A32365-001-a-3330">Preachers</w>
     <w lemma="on" pos="acp" xml:id="A32365-001-a-3340">on</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-3350">the</w>
     <w lemma="say" pos="j-vn" xml:id="A32365-001-a-3360">said</w>
     <w lemma="fast-day" pos="n2" xml:id="A32365-001-a-3370">Fast-days</w>
     <pc xml:id="A32365-001-a-3380">,</pc>
     <w lemma="that" pos="cs" xml:id="A32365-001-a-3390">that</w>
     <w lemma="they" pos="pns" xml:id="A32365-001-a-3400">they</w>
     <w lemma="do" pos="vvb" xml:id="A32365-001-a-3410">do</w>
     <w lemma="earnest" pos="av-j" xml:id="A32365-001-a-3420">Earnestly</w>
     <w lemma="exhort" pos="vvi" xml:id="A32365-001-a-3430">Exhort</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-3440">the</w>
     <w lemma="people" pos="n1" xml:id="A32365-001-a-3450">People</w>
     <pc xml:id="A32365-001-a-3460">,</pc>
     <w lemma="in" pos="acp" xml:id="A32365-001-a-3470">in</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-3480">the</w>
     <w lemma="several" pos="j" xml:id="A32365-001-a-3490">several</w>
     <w lemma="church" pos="n2" xml:id="A32365-001-a-3500">Churches</w>
     <pc xml:id="A32365-001-a-3510">,</pc>
     <w lemma="to" pos="prt" xml:id="A32365-001-a-3520">to</w>
     <w lemma="a" pos="d" xml:id="A32365-001-a-3530">a</w>
     <w lemma="free" pos="j" xml:id="A32365-001-a-3540">Free</w>
     <w lemma="and" pos="cc" xml:id="A32365-001-a-3550">and</w>
     <w lemma="cheerful" pos="j" reg="Cheerful" xml:id="A32365-001-a-3560">Chearful</w>
     <w lemma="contribution" pos="n1" xml:id="A32365-001-a-3570">Contribution</w>
     <pc xml:id="A32365-001-a-3580">,</pc>
     <w lemma="towards" pos="acp" xml:id="A32365-001-a-3590">towards</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-3600">the</w>
     <w lemma="relief" pos="n1" xml:id="A32365-001-a-3610">Relief</w>
     <w lemma="of" pos="acp" xml:id="A32365-001-a-3620">of</w>
     <w lemma="their" pos="po" xml:id="A32365-001-a-3630">their</w>
     <w lemma="christian" pos="jnn" xml:id="A32365-001-a-3640">Christian</w>
     <w lemma="brethren" pos="n2" xml:id="A32365-001-a-3650">Brethren</w>
     <pc xml:id="A32365-001-a-3660">,</pc>
     <w lemma="who" pos="crq" xml:id="A32365-001-a-3670">whom</w>
     <w lemma="it" pos="pn" xml:id="A32365-001-a-3680">it</w>
     <w lemma="have" pos="vvz" xml:id="A32365-001-a-3690">hath</w>
     <w lemma="please" pos="vvn" xml:id="A32365-001-a-3700">pleased</w>
     <w lemma="God" pos="nn1" xml:id="A32365-001-a-3710">God</w>
     <w lemma="to" pos="prt" xml:id="A32365-001-a-3720">to</w>
     <w lemma="visit" pos="vvi" xml:id="A32365-001-a-3730">visit</w>
     <w lemma="with" pos="acp" xml:id="A32365-001-a-3740">with</w>
     <w lemma="sickness" pos="n1" xml:id="A32365-001-a-3750">Sickness</w>
     <pc xml:id="A32365-001-a-3760">;</pc>
     <w lemma="and" pos="cc" xml:id="A32365-001-a-3770">And</w>
     <w lemma="that" pos="cs" xml:id="A32365-001-a-3780">that</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-3790">the</w>
     <w lemma="money" pos="n2" xml:id="A32365-001-a-3800">Moneys</w>
     <w lemma="so" pos="av" xml:id="A32365-001-a-3810">so</w>
     <w lemma="gather" pos="vvn" xml:id="A32365-001-a-3820">gathered</w>
     <w lemma="be" pos="vvb" xml:id="A32365-001-a-3830">be</w>
     <w lemma="dispose" pos="vvn" xml:id="A32365-001-a-3840">disposed</w>
     <w lemma="according" pos="j" xml:id="A32365-001-a-3850">according</w>
     <w lemma="as" pos="acp" xml:id="A32365-001-a-3860">as</w>
     <w lemma="his" pos="po" xml:id="A32365-001-a-3870">His</w>
     <w lemma="majesty" pos="n1" xml:id="A32365-001-a-3880">Majesty</w>
     <w lemma="have" pos="vvz" xml:id="A32365-001-a-3890">hath</w>
     <w lemma="direct" pos="vvn" xml:id="A32365-001-a-3900">directed</w>
     <w lemma="by" pos="acp" xml:id="A32365-001-a-3910">by</w>
     <w lemma="his" pos="po" xml:id="A32365-001-a-3920">His</w>
     <w lemma="say" pos="j-vn" xml:id="A32365-001-a-3930">said</w>
     <w lemma="former" pos="j" xml:id="A32365-001-a-3940">Former</w>
     <w lemma="proclamation" pos="n1" xml:id="A32365-001-a-3950">Proclamation</w>
     <pc unit="sentence" xml:id="A32365-001-a-3960">.</pc>
    </p>
    <closer xml:id="A32365-e190">
     <dateline xml:id="A32365-e200">
      <w lemma="give" pos="vvn" xml:id="A32365-001-a-3970">Given</w>
      <w lemma="at" pos="acp" xml:id="A32365-001-a-3980">at</w>
      <w lemma="the" pos="d" xml:id="A32365-001-a-3990">the</w>
      <w lemma="court" pos="n1" xml:id="A32365-001-a-4000">Court</w>
      <w lemma="at" pos="acp" xml:id="A32365-001-a-4010">at</w>
      <w lemma="Oxford" pos="nn1" rend="hi" xml:id="A32365-001-a-4020">Oxford</w>
      <date xml:id="A32365-e220">
       <w lemma="the" pos="d" xml:id="A32365-001-a-4030">the</w>
       <w lemma="twenty" pos="crd" xml:id="A32365-001-a-4040">Twenty</w>
       <w lemma="six" pos="ord" xml:id="A32365-001-a-4050">Sixth</w>
       <w lemma="day" pos="n1" xml:id="A32365-001-a-4060">day</w>
       <w lemma="of" pos="acp" xml:id="A32365-001-a-4070">of</w>
       <hi xml:id="A32365-e230">
        <w lemma="September" pos="nn1" xml:id="A32365-001-a-4080">September</w>
        <pc xml:id="A32365-001-a-4090">,</pc>
       </hi>
       <w lemma="in" pos="acp" xml:id="A32365-001-a-4100">in</w>
       <w lemma="the" pos="d" xml:id="A32365-001-a-4110">the</w>
       <w lemma="seventeen" pos="ord" xml:id="A32365-001-a-4120">Seventeenth</w>
       <w lemma="year" pos="n1" xml:id="A32365-001-a-4130">year</w>
       <w lemma="of" pos="acp" xml:id="A32365-001-a-4140">of</w>
       <w lemma="his" pos="po" xml:id="A32365-001-a-4150">his</w>
       <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A32365-001-a-4160">Majesties</w>
       <w lemma="reign" pos="n1" xml:id="A32365-001-a-4170">Reign</w>
       <pc unit="sentence" xml:id="A32365-001-a-4180">.</pc>
       <w lemma="1665." pos="crd" xml:id="A32365-001-a-4190">1665.</w>
       <pc unit="sentence" xml:id="A32365-001-a-4200"/>
      </date>
     </dateline>
     <lb xml:id="A32365-e240"/>
     <w lemma="God" pos="nn1" xml:id="A32365-001-a-4210">God</w>
     <w lemma="save" pos="vvb" xml:id="A32365-001-a-4220">save</w>
     <w lemma="the" pos="d" xml:id="A32365-001-a-4230">the</w>
     <w lemma="King" pos="n1" xml:id="A32365-001-a-4240">King</w>
     <pc unit="sentence" xml:id="A32365-001-a-4241">.</pc>
    </closer>
   </div>
  </body>
  <back xml:id="A32365-e250">
   <div type="colophon" xml:id="A32365-e260">
    <p xml:id="A32365-e270">
     <hi xml:id="A32365-e280">
      <w lemma="LONDON" pos="nn1" xml:id="A32365-001-a-4260">LONDON</w>
      <pc xml:id="A32365-001-a-4270">,</pc>
      <hi xml:id="A32365-e290">
       <w lemma="print" pos="vvn" xml:id="A32365-001-a-4280">Printed</w>
       <w lemma="by" pos="acp" xml:id="A32365-001-a-4290">by</w>
      </hi>
      <w lemma="John" pos="nn1" xml:id="A32365-001-a-4300">John</w>
      <w lemma="bill" pos="n1" xml:id="A32365-001-a-4310">Bill</w>
      <w lemma="and" pos="cc" rend="hi" xml:id="A32365-001-a-4320">and</w>
      <w lemma="Christopher" pos="nn1" xml:id="A32365-001-a-4330">Christopher</w>
      <w lemma="Barker" pos="nn1" xml:id="A32365-001-a-4340">Barker</w>
      <pc xml:id="A32365-001-a-4350">,</pc>
      <hi xml:id="A32365-e310">
       <w lemma="printer" pos="n2" xml:id="A32365-001-a-4360">Printers</w>
       <w lemma="to" pos="acp" xml:id="A32365-001-a-4370">to</w>
       <w lemma="the" pos="d" xml:id="A32365-001-a-4380">the</w>
       <w lemma="king" pos="n2" xml:id="A32365-001-a-4390">Kings</w>
       <w lemma="most" pos="avs-d" xml:id="A32365-001-a-4400">most</w>
       <w lemma="excellent" pos="j" xml:id="A32365-001-a-4410">Excellent</w>
       <w lemma="majesty" pos="n1" xml:id="A32365-001-a-4420">Majesty</w>
       <pc xml:id="A32365-001-a-4430">,</pc>
       <w lemma="1665." pos="crd" xml:id="A32365-001-a-4440">1665.</w>
       <pc unit="sentence" xml:id="A32365-001-a-4450"/>
      </hi>
     </hi>
    </p>
   </div>
  </back>
 </text>
</TEI>
