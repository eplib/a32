<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A32314">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>His Majestie's most gracious speech to both Houses of Parliament, the eighteenth day of January, 1666</title>
    <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A32314 of text R19 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing C3140). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 3 1-bit group-IV TIFF page images.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A32314</idno>
    <idno type="STC">Wing C3140</idno>
    <idno type="STC">ESTC R19</idno>
    <idno type="EEBO-CITATION">12125974</idno>
    <idno type="OCLC">ocm 12125974</idno>
    <idno type="VID">54601</idno>
    <idno type="PROQUESTGOID">2240951867</idno>
    <availability>
     <p>This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. Searching, reading, printing, or downloading EEBO-TCP texts is reserved for the authorized users of these project partner institutions. Permission must be granted for subsequent distribution, in print or electronically, of this EEBO-TCP Phase II text, in whole or in part.</p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 2, no. A32314)</note>
    <note>Transcribed from: (Early English Books Online ; image set 54601)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 760:22)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>His Majestie's most gracious speech to both Houses of Parliament, the eighteenth day of January, 1666</title>
      <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
      <author>Charles II, King of England, 1630-1685.</author>
     </titleStmt>
     <extent>4 p.</extent>
     <publicationStmt>
      <publisher>Printed by the assigns of John Bill and Christopher Barker ...,</publisher>
      <pubPlace>[London] In the Savoy :</pubPlace>
      <date>1666.</date>
     </publicationStmt>
     <notesStmt>
      <note>Reproduction of original in Cambridge University Library.</note>
      <note>Concerns the raising of moneys for the Anglo-Dutch War.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Anglo-Dutch War, 1664-1667.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>His Majestie's most gracious speech to both Houses of Parliament, the eighteenth day of January, 1666.</ep:title>
    <ep:author>England and Wales. Sovereign </ep:author>
    <ep:publicationYear>1667</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>6</ep:pageCount>
    <ep:wordCount>488</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2009-04</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2009-05</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2009-06</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2009-06</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-09</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A32314-e10010">
  <front xml:id="A32314-e10020">
   <div type="title_page" xml:id="A32314-e10030">
    <pb facs="tcp:54601:1" xml:id="A32314-001-a"/>
    <pb facs="tcp:54601:1" rend="simple:additions" xml:id="A32314-001-b"/>
    <p xml:id="A32314-e10040">
     <w lemma="his" pos="po" xml:id="A32314-001-b-0010">His</w>
     <w join="right" lemma="majesty" pos="n1" reg="majesty" xml:id="A32314-001-b-0020">Majestie</w>
     <w join="left" lemma="be" pos="vvz" xml:id="A32314-001-b-0021">'s</w>
     <w lemma="most" pos="avs-d" xml:id="A32314-001-b-0030">Most</w>
     <w lemma="gracious" pos="j" xml:id="A32314-001-b-0040">Gracious</w>
     <w lemma="speech" pos="n1" xml:id="A32314-001-b-0050">SPEECH</w>
     <w lemma="to" pos="acp" xml:id="A32314-001-b-0060">TO</w>
     <w lemma="both" pos="d" xml:id="A32314-001-b-0070">BOTH</w>
     <w lemma="house" pos="n2" xml:id="A32314-001-b-0080">HOUSES</w>
     <w lemma="of" pos="acp" xml:id="A32314-001-b-0090">OF</w>
     <w lemma="parliament" pos="n1" xml:id="A32314-001-b-0100">PARLIAMENT</w>
     <pc xml:id="A32314-001-b-0110">,</pc>
     <w lemma="the" pos="d" xml:id="A32314-001-b-0120">The</w>
     <w lemma="eighteen" pos="ord" xml:id="A32314-001-b-0130">Eighteenth</w>
     <w lemma="day" pos="n1" xml:id="A32314-001-b-0140">Day</w>
     <w lemma="of" pos="acp" xml:id="A32314-001-b-0150">of</w>
     <w lemma="January" pos="nn1" rend="hi" xml:id="A32314-001-b-0160">January</w>
     <pc xml:id="A32314-001-b-0170">,</pc>
     <w lemma="1666." pos="crd" xml:id="A32314-001-b-0180">1666.</w>
     <pc unit="sentence" xml:id="A32314-001-b-0190"/>
    </p>
    <figure xml:id="A32314-e10060">
     <head xml:id="A32314-e10070">
      <w lemma="c" pos="sy" xml:id="A32314-001-b-0200">C</w>
      <w lemma="r" pos="sy" xml:id="A32314-001-b-0210">R</w>
     </head>
     <q xml:id="A32314-e10080">
      <w lemma="n/a" pos="ffr" xml:id="A32314-001-b-0220">DIEV</w>
      <w lemma="n/a" pos="ffr" xml:id="A32314-001-b-0230">ET</w>
      <w lemma="n/a" pos="ffr" xml:id="A32314-001-b-0240">MON</w>
      <w lemma="n/a" pos="ffr" xml:id="A32314-001-b-0250">DROIT</w>
     </q>
     <q xml:id="A32314-e10090">
      <w lemma="n/a" pos="ffr" xml:id="A32314-001-b-0260">HONI</w>
      <w lemma="n/a" pos="ffr" xml:id="A32314-001-b-0270">SOIT</w>
      <w lemma="n/a" pos="ffr" xml:id="A32314-001-b-0280">QVI</w>
      <w lemma="n/a" pos="ffr" xml:id="A32314-001-b-0290">MAL</w>
      <w lemma="n/a" pos="ffr" xml:id="A32314-001-b-0300">Y</w>
      <w lemma="n/a" pos="ffr" xml:id="A32314-001-b-0310">PENSE</w>
     </q>
     <figDesc xml:id="A32314-e10100">royal blazon or coat of arms</figDesc>
    </figure>
    <p xml:id="A32314-e10110">
     <w lemma="in" pos="acp" xml:id="A32314-001-b-0320">In</w>
     <w lemma="the" pos="d" xml:id="A32314-001-b-0330">the</w>
     <w lemma="Savoy" pos="nn1" rend="hi" xml:id="A32314-001-b-0340">SAVOY</w>
     <pc xml:id="A32314-001-b-0350">,</pc>
     <w lemma="print" pos="vvn" xml:id="A32314-001-b-0360">Printed</w>
     <w lemma="by" pos="acp" xml:id="A32314-001-b-0370">by</w>
     <w lemma="the" pos="d" xml:id="A32314-001-b-0380">the</w>
     <w lemma="assign" pos="vvz" xml:id="A32314-001-b-0390">Assigns</w>
     <w lemma="of" pos="acp" xml:id="A32314-001-b-0400">of</w>
     <hi xml:id="A32314-e10130">
      <w lemma="John" pos="nn1" xml:id="A32314-001-b-0410">John</w>
      <w lemma="bill" pos="n1" xml:id="A32314-001-b-0420">Bill</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="A32314-001-b-0430">and</w>
     <hi xml:id="A32314-e10140">
      <w lemma="Christopher" pos="nn1" xml:id="A32314-001-b-0440">Christopher</w>
      <w lemma="Barker" pos="nn1" xml:id="A32314-001-b-0450">Barker</w>
     </hi>
     <pc rend="follows-hi" xml:id="A32314-001-b-0460">,</pc>
     <w lemma="his" pos="po" xml:id="A32314-001-b-0470">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A32314-001-b-0480">Majesties</w>
     <w lemma="printer" pos="n2" xml:id="A32314-001-b-0490">Printers</w>
     <pc unit="sentence" xml:id="A32314-001-b-0500">.</pc>
     <w lemma="1666." pos="crd" xml:id="A32314-001-b-0510">1666.</w>
     <pc unit="sentence" xml:id="A32314-001-b-0520"/>
    </p>
    <p xml:id="A32314-e10150">
     <w lemma="cum" pos="crd" reg="CUM" xml:id="A32314-001-b-0530">CVM</w>
     <w lemma="n/a" pos="fla" xml:id="A32314-001-b-0540">PRIVILEGIO</w>
     <pc unit="sentence" xml:id="A32314-001-b-0550">.</pc>
    </p>
   </div>
  </front>
  <body xml:id="A32314-e10160">
   <div type="speech" xml:id="A32314-e10170">
    <pb facs="tcp:54601:2" rend="simple:additions" xml:id="A32314-002-a"/>
    <pb facs="tcp:54601:2" n="3" xml:id="A32314-002-b"/>
    <head xml:id="A32314-e10180">
     <w lemma="his" pos="po" xml:id="A32314-002-b-0010">His</w>
     <w lemma="Majesty" pos="ng1" xml:id="A32314-002-b-0020">MAJESTIE'S</w>
     <w lemma="most" pos="avs-d" xml:id="A32314-002-b-0030">most</w>
     <w lemma="gracious" pos="j" xml:id="A32314-002-b-0040">Gracious</w>
     <w lemma="speech" pos="n1" xml:id="A32314-002-b-0050">Speech</w>
     <w lemma="to" pos="acp" xml:id="A32314-002-b-0060">to</w>
     <w lemma="both" pos="d" xml:id="A32314-002-b-0070">both</w>
     <w lemma="house" pos="n2" xml:id="A32314-002-b-0080">Houses</w>
     <w lemma="of" pos="acp" xml:id="A32314-002-b-0090">of</w>
     <w lemma="parliament" pos="n1" xml:id="A32314-002-b-0100">Parliament</w>
     <pc unit="sentence" xml:id="A32314-002-b-0110">.</pc>
    </head>
    <opener xml:id="A32314-e10190">
     <salute xml:id="A32314-e10200">
      <w lemma="my" pos="po" xml:id="A32314-002-b-0120">My</w>
      <w lemma="lord" pos="n2" xml:id="A32314-002-b-0130">Lords</w>
      <w lemma="and" pos="cc" xml:id="A32314-002-b-0140">and</w>
      <w lemma="gentleman" pos="n2" xml:id="A32314-002-b-0150">Gentlemen</w>
      <pc xml:id="A32314-002-b-0160">,</pc>
     </salute>
    </opener>
    <p xml:id="A32314-e10210">
     <w lemma="I" pos="pns" rend="decorinit" xml:id="A32314-002-b-0170">I</w>
     <w lemma="have" pos="vvb" xml:id="A32314-002-b-0180">Have</w>
     <w lemma="now" pos="av" xml:id="A32314-002-b-0190">now</w>
     <w lemma="pass" pos="vvn" xml:id="A32314-002-b-0200">Passed</w>
     <w lemma="your" pos="po" xml:id="A32314-002-b-0210">your</w>
     <w lemma="bill" pos="n2" xml:id="A32314-002-b-0220">Bills</w>
     <pc xml:id="A32314-002-b-0230">,</pc>
     <w lemma="and" pos="cc" xml:id="A32314-002-b-0240">and</w>
     <w lemma="I" pos="pns" xml:id="A32314-002-b-0250">I</w>
     <w lemma="be" pos="vvd" xml:id="A32314-002-b-0260">was</w>
     <w lemma="in" pos="acp" xml:id="A32314-002-b-0270">in</w>
     <w lemma="good" pos="j" xml:id="A32314-002-b-0280">good</w>
     <w lemma="hope" pos="n1" xml:id="A32314-002-b-0290">hope</w>
     <w lemma="to" pos="prt" xml:id="A32314-002-b-0300">to</w>
     <w lemma="have" pos="vvi" xml:id="A32314-002-b-0310">have</w>
     <w lemma="have" pos="vvn" xml:id="A32314-002-b-0320">had</w>
     <w lemma="other" pos="d" xml:id="A32314-002-b-0330">other</w>
     <w lemma="bill" pos="n2" xml:id="A32314-002-b-0340">Bills</w>
     <w lemma="ready" pos="j" xml:id="A32314-002-b-0350">ready</w>
     <w lemma="to" pos="prt" xml:id="A32314-002-b-0360">to</w>
     <w lemma="pass" pos="vvi" xml:id="A32314-002-b-0370">Pass</w>
     <w lemma="too" pos="av" xml:id="A32314-002-b-0380">too</w>
     <pc unit="sentence" xml:id="A32314-002-b-0390">.</pc>
     <w lemma="I" pos="pns" xml:id="A32314-002-b-0400">I</w>
     <w lemma="can" pos="vmbx" xml:id="A32314-002-b-0410">cannot</w>
     <w lemma="forget" pos="vvi" xml:id="A32314-002-b-0420">forget</w>
     <pc xml:id="A32314-002-b-0430">,</pc>
     <w lemma="that" pos="cs" xml:id="A32314-002-b-0440">That</w>
     <w lemma="within" pos="acp" xml:id="A32314-002-b-0450">within</w>
     <w lemma="few" pos="d" xml:id="A32314-002-b-0460">few</w>
     <w lemma="day" pos="n2" xml:id="A32314-002-b-0470">days</w>
     <w lemma="after" pos="acp" xml:id="A32314-002-b-0480">after</w>
     <w lemma="your" pos="po" xml:id="A32314-002-b-0490">your</w>
     <w lemma="come" pos="n1-vg" xml:id="A32314-002-b-0500">coming</w>
     <w lemma="together" pos="av" xml:id="A32314-002-b-0510">together</w>
     <w lemma="in" pos="acp" xml:id="A32314-002-b-0520">in</w>
     <w lemma="September" pos="nn1" rend="hi" xml:id="A32314-002-b-0530">September</w>
     <pc xml:id="A32314-002-b-0540">,</pc>
     <w lemma="both" pos="d" xml:id="A32314-002-b-0550">both</w>
     <w lemma="house" pos="n2" xml:id="A32314-002-b-0560">Houses</w>
     <w lemma="present" pos="vvd" xml:id="A32314-002-b-0570">presented</w>
     <w lemma="i" pos="pno" xml:id="A32314-002-b-0580">Me</w>
     <w lemma="with" pos="acp" xml:id="A32314-002-b-0590">with</w>
     <w lemma="their" pos="po" xml:id="A32314-002-b-0600">their</w>
     <w lemma="vote" pos="n1" xml:id="A32314-002-b-0610">Vote</w>
     <w lemma="and" pos="cc" xml:id="A32314-002-b-0620">and</w>
     <w lemma="declaration" pos="n1" xml:id="A32314-002-b-0630">Declaration</w>
     <pc xml:id="A32314-002-b-0640">,</pc>
     <w lemma="that" pos="cs" xml:id="A32314-002-b-0650">That</w>
     <w lemma="they" pos="pns" xml:id="A32314-002-b-0660">they</w>
     <w lemma="will" pos="vmd" xml:id="A32314-002-b-0670">would</w>
     <w lemma="give" pos="vvi" xml:id="A32314-002-b-0680">give</w>
     <w lemma="i" pos="pno" xml:id="A32314-002-b-0690">Me</w>
     <w lemma="a" pos="d" xml:id="A32314-002-b-0700">a</w>
     <w lemma="supply" pos="n1" xml:id="A32314-002-b-0710">Supply</w>
     <w lemma="proportionable" pos="j" xml:id="A32314-002-b-0720">proportionable</w>
     <w lemma="to" pos="acp" xml:id="A32314-002-b-0730">to</w>
     <w lemma="my" pos="po" xml:id="A32314-002-b-0740">My</w>
     <w lemma="occasion" pos="n2" xml:id="A32314-002-b-0750">Occasions</w>
     <pc xml:id="A32314-002-b-0760">;</pc>
     <w lemma="and" pos="cc" xml:id="A32314-002-b-0770">and</w>
     <w lemma="the" pos="d" xml:id="A32314-002-b-0780">the</w>
     <w lemma="confidence" pos="n1" xml:id="A32314-002-b-0790">Confidence</w>
     <w lemma="of" pos="acp" xml:id="A32314-002-b-0800">of</w>
     <w lemma="this" pos="d" xml:id="A32314-002-b-0810">this</w>
     <w lemma="make" pos="vvn" xml:id="A32314-002-b-0820">made</w>
     <w lemma="i" pos="pno" xml:id="A32314-002-b-0830">Me</w>
     <w lemma="anticipate" pos="vvi" xml:id="A32314-002-b-0840">anticipate</w>
     <w lemma="that" pos="d" xml:id="A32314-002-b-0850">that</w>
     <w lemma="small" pos="j" xml:id="A32314-002-b-0860">small</w>
     <w lemma="part" pos="n1" xml:id="A32314-002-b-0870">part</w>
     <w lemma="of" pos="acp" xml:id="A32314-002-b-0880">of</w>
     <w lemma="my" pos="po" xml:id="A32314-002-b-0890">My</w>
     <w lemma="revenue" pos="n1" xml:id="A32314-002-b-0900">Revenue</w>
     <pc xml:id="A32314-002-b-0910">,</pc>
     <w lemma="which" pos="crq" xml:id="A32314-002-b-0920">which</w>
     <w lemma="be" pos="vvd" xml:id="A32314-002-b-0930">was</w>
     <w lemma="unanticipate" pos="vvn" xml:id="A32314-002-b-0940">unanticipated</w>
     <pc xml:id="A32314-002-b-0950">,</pc>
     <w lemma="for" pos="acp" xml:id="A32314-002-b-0960">for</w>
     <w lemma="the" pos="d" xml:id="A32314-002-b-0970">the</w>
     <w lemma="payment" pos="n1" xml:id="A32314-002-b-0980">Payment</w>
     <w lemma="of" pos="acp" xml:id="A32314-002-b-0990">of</w>
     <w lemma="the" pos="d" xml:id="A32314-002-b-1000">the</w>
     <w lemma="seaman" pos="n2" xml:id="A32314-002-b-1010">Seamen</w>
     <pc xml:id="A32314-002-b-1020">;</pc>
     <w lemma="and" pos="cc" xml:id="A32314-002-b-1030">and</w>
     <w lemma="my" pos="po" xml:id="A32314-002-b-1040">My</w>
     <w lemma="credit" pos="n1" xml:id="A32314-002-b-1050">Credit</w>
     <w lemma="have" pos="vvz" xml:id="A32314-002-b-1060">hath</w>
     <w lemma="go" pos="vvn" xml:id="A32314-002-b-1070">gone</w>
     <w lemma="far" pos="avc-j" xml:id="A32314-002-b-1080">farther</w>
     <w lemma="than" pos="cs" reg="than" xml:id="A32314-002-b-1090">then</w>
     <w lemma="I" pos="pns" xml:id="A32314-002-b-1100">I</w>
     <w lemma="have" pos="vvd" xml:id="A32314-002-b-1110">had</w>
     <w lemma="reason" pos="n1" xml:id="A32314-002-b-1120">reason</w>
     <w lemma="to" pos="prt" xml:id="A32314-002-b-1130">to</w>
     <w lemma="think" pos="vvi" xml:id="A32314-002-b-1140">think</w>
     <w lemma="it" pos="pn" xml:id="A32314-002-b-1150">it</w>
     <w lemma="will" pos="vmd" xml:id="A32314-002-b-1160">would</w>
     <pc xml:id="A32314-002-b-1170">,</pc>
     <w lemma="but" pos="acp" xml:id="A32314-002-b-1180">but</w>
     <w join="right" lemma="it" pos="pn" xml:id="A32314-002-b-1190">'t</w>
     <w join="left" lemma="be" pos="vvz" xml:id="A32314-002-b-1191">is</w>
     <w lemma="now" pos="av" xml:id="A32314-002-b-1200">now</w>
     <w lemma="at" pos="acp" xml:id="A32314-002-b-1210">at</w>
     <w lemma="a" pos="d" xml:id="A32314-002-b-1220">an</w>
     <w lemma="end" pos="n1" xml:id="A32314-002-b-1230">end</w>
     <pc unit="sentence" xml:id="A32314-002-b-1240">.</pc>
    </p>
    <p xml:id="A32314-e10230">
     <w lemma="this" pos="d" xml:id="A32314-002-b-1250">This</w>
     <w lemma="be" pos="vvz" xml:id="A32314-002-b-1260">is</w>
     <w lemma="the" pos="d" xml:id="A32314-002-b-1270">the</w>
     <w lemma="first" pos="ord" xml:id="A32314-002-b-1280">first</w>
     <w lemma="day" pos="n1" xml:id="A32314-002-b-1290">day</w>
     <w lemma="I" pos="pns" xml:id="A32314-002-b-1300">I</w>
     <w lemma="have" pos="vvb" xml:id="A32314-002-b-1310">have</w>
     <w lemma="hear" pos="vvn" xml:id="A32314-002-b-1320">heard</w>
     <w lemma="of" pos="acp" xml:id="A32314-002-b-1330">of</w>
     <w lemma="any" pos="d" xml:id="A32314-002-b-1340">any</w>
     <w lemma="money" pos="n1" xml:id="A32314-002-b-1350">Money</w>
     <w lemma="towards" pos="acp" xml:id="A32314-002-b-1360">towards</w>
     <w lemma="a" pos="d" xml:id="A32314-002-b-1370">a</w>
     <w lemma="supply" pos="n1" xml:id="A32314-002-b-1380">Supply</w>
     <pc xml:id="A32314-002-b-1390">,</pc>
     <w lemma="be" pos="vvg" xml:id="A32314-002-b-1400">being</w>
     <w lemma="the" pos="d" xml:id="A32314-002-b-1410">the</w>
     <w lemma="18." pos="crd" xml:id="A32314-002-b-1420">18.</w>
     <w lemma="of" pos="acp" xml:id="A32314-002-b-1430">of</w>
     <w lemma="January" pos="nn1" rend="hi" xml:id="A32314-002-b-1440">January</w>
     <pc xml:id="A32314-002-b-1450">,</pc>
     <w lemma="and" pos="cc" xml:id="A32314-002-b-1460">and</w>
     <w lemma="what" pos="crq" xml:id="A32314-002-b-1470">what</w>
     <w lemma="this" pos="d" xml:id="A32314-002-b-1480">this</w>
     <w lemma="will" pos="vmb" xml:id="A32314-002-b-1490">will</w>
     <w lemma="amount" pos="vvi" xml:id="A32314-002-b-1500">amount</w>
     <w lemma="to" pos="acp" xml:id="A32314-002-b-1510">to</w>
     <pc xml:id="A32314-002-b-1520">,</pc>
     <w lemma="God" pos="nn1" xml:id="A32314-002-b-1530">God</w>
     <w lemma="know" pos="vvz" xml:id="A32314-002-b-1540">knows</w>
     <pc xml:id="A32314-002-b-1550">;</pc>
     <w lemma="and" pos="cc" xml:id="A32314-002-b-1560">And</w>
     <w lemma="what" pos="crq" xml:id="A32314-002-b-1570">what</w>
     <w lemma="time" pos="n1" xml:id="A32314-002-b-1580">time</w>
     <w lemma="I" pos="pns" xml:id="A32314-002-b-1590">I</w>
     <w lemma="have" pos="vvb" xml:id="A32314-002-b-1600">have</w>
     <w lemma="to" pos="prt" xml:id="A32314-002-b-1610">to</w>
     <w lemma="make" pos="vvi" xml:id="A32314-002-b-1620">make</w>
     <w lemma="such" pos="d" xml:id="A32314-002-b-1630">such</w>
     <w lemma="preparation" pos="n2" xml:id="A32314-002-b-1640">Preparations</w>
     <w lemma="as" pos="acp" xml:id="A32314-002-b-1650">as</w>
     <w lemma="be" pos="vvb" xml:id="A32314-002-b-1660">are</w>
     <w lemma="necessary" pos="j" xml:id="A32314-002-b-1670">necessary</w>
     <w lemma="to" pos="prt" xml:id="A32314-002-b-1680">to</w>
     <w lemma="meet" pos="vvi" xml:id="A32314-002-b-1690">meet</w>
     <w lemma="three" pos="crd" xml:id="A32314-002-b-1700">three</w>
     <w lemma="such" pos="d" xml:id="A32314-002-b-1710">such</w>
     <w lemma="enemy" pos="n2" xml:id="A32314-002-b-1720">Enemies</w>
     <w lemma="as" pos="acp" xml:id="A32314-002-b-1730">as</w>
     <w lemma="I" pos="pns" xml:id="A32314-002-b-1740">I</w>
     <w lemma="have" pos="vvb" xml:id="A32314-002-b-1750">have</w>
     <pc xml:id="A32314-002-b-1760">,</pc>
     <w lemma="you" pos="pn" xml:id="A32314-002-b-1770">you</w>
     <w lemma="can" pos="vmb" xml:id="A32314-002-b-1780">can</w>
     <w lemma="well" pos="av" xml:id="A32314-002-b-1790">well</w>
     <w lemma="enough" pos="av-d" xml:id="A32314-002-b-1800">enough</w>
     <w lemma="judge" pos="vvi" xml:id="A32314-002-b-1810">judge</w>
     <pc xml:id="A32314-002-b-1820">:</pc>
     <w lemma="and" pos="cc" xml:id="A32314-002-b-1830">And</w>
     <w lemma="I" pos="pns" xml:id="A32314-002-b-1840">I</w>
     <w lemma="must" pos="vmb" xml:id="A32314-002-b-1850">must</w>
     <w lemma="tell" pos="vvi" xml:id="A32314-002-b-1860">tell</w>
     <pb facs="tcp:54601:3" n="4" xml:id="A32314-003-a"/>
     <w lemma="you" pos="pn" xml:id="A32314-003-a-0010">you</w>
     <pc xml:id="A32314-003-a-0020">,</pc>
     <w lemma="what" pos="crq" xml:id="A32314-003-a-0030">what</w>
     <w lemma="discourse" pos="vvz" xml:id="A32314-003-a-0040">Discourses</w>
     <w lemma="soever" pos="av" xml:id="A32314-003-a-0050">soever</w>
     <w lemma="be" pos="vvb" xml:id="A32314-003-a-0060">are</w>
     <w lemma="abroad" pos="av" xml:id="A32314-003-a-0070">abroad</w>
     <pc xml:id="A32314-003-a-0080">,</pc>
     <w lemma="I" pos="pns" xml:id="A32314-003-a-0090">I</w>
     <w lemma="be" pos="vvm" xml:id="A32314-003-a-0100">am</w>
     <w lemma="not" pos="xx" xml:id="A32314-003-a-0110">not</w>
     <w lemma="in" pos="acp" xml:id="A32314-003-a-0120">in</w>
     <w lemma="any" pos="d" xml:id="A32314-003-a-0130">any</w>
     <w lemma="treaty" pos="n1" xml:id="A32314-003-a-0140">Treaty</w>
     <pc xml:id="A32314-003-a-0150">;</pc>
     <w lemma="but" pos="acp" xml:id="A32314-003-a-0160">but</w>
     <w lemma="by" pos="acp" xml:id="A32314-003-a-0170">by</w>
     <w lemma="the" pos="d" xml:id="A32314-003-a-0180">the</w>
     <w lemma="grace" pos="n1" xml:id="A32314-003-a-0190">grace</w>
     <w lemma="of" pos="acp" xml:id="A32314-003-a-0200">of</w>
     <w lemma="God" pos="nn1" xml:id="A32314-003-a-0210">God</w>
     <w lemma="I" pos="pns" xml:id="A32314-003-a-0220">I</w>
     <w lemma="will" pos="vmb" xml:id="A32314-003-a-0230">will</w>
     <w lemma="not" pos="xx" xml:id="A32314-003-a-0240">not</w>
     <w lemma="give" pos="vvi" xml:id="A32314-003-a-0250">give</w>
     <w lemma="over" pos="acp" xml:id="A32314-003-a-0260">over</w>
     <w lemma="myself" pos="pr" reg="Myself" xml:id="A32314-003-a-0270">My self</w>
     <w lemma="and" pos="cc" xml:id="A32314-003-a-0290">and</w>
     <w lemma="you" pos="pn" xml:id="A32314-003-a-0300">You</w>
     <pc xml:id="A32314-003-a-0310">,</pc>
     <w lemma="but" pos="acp" xml:id="A32314-003-a-0320">but</w>
     <w lemma="will" pos="vmb" xml:id="A32314-003-a-0330">will</w>
     <w lemma="do" pos="vvi" xml:id="A32314-003-a-0340">do</w>
     <w lemma="what" pos="crq" xml:id="A32314-003-a-0350">what</w>
     <w lemma="be" pos="vvz" xml:id="A32314-003-a-0360">is</w>
     <w lemma="in" pos="acp" xml:id="A32314-003-a-0370">in</w>
     <w lemma="my" pos="po" xml:id="A32314-003-a-0380">My</w>
     <w lemma="power" pos="n1" xml:id="A32314-003-a-0390">power</w>
     <w lemma="for" pos="acp" xml:id="A32314-003-a-0400">for</w>
     <w lemma="the" pos="d" xml:id="A32314-003-a-0410">the</w>
     <w lemma="defence" pos="n1" xml:id="A32314-003-a-0420">Defence</w>
     <w lemma="of" pos="acp" xml:id="A32314-003-a-0430">of</w>
     <w lemma="myself" pos="pr" reg="Myself" xml:id="A32314-003-a-0440">My self</w>
     <w lemma="and" pos="cc" xml:id="A32314-003-a-0460">and</w>
     <w lemma="you" pos="pn" xml:id="A32314-003-a-0470">You</w>
     <pc unit="sentence" xml:id="A32314-003-a-0471">.</pc>
     <w join="right" lemma="it" pos="pn" xml:id="A32314-003-a-0480">'T</w>
     <w join="left" lemma="be" pos="vvz" xml:id="A32314-003-a-0481">is</w>
     <w lemma="high" pos="j" xml:id="A32314-003-a-0490">high</w>
     <w lemma="time" pos="n1" xml:id="A32314-003-a-0500">time</w>
     <w lemma="for" pos="acp" xml:id="A32314-003-a-0510">for</w>
     <w lemma="you" pos="pn" xml:id="A32314-003-a-0520">you</w>
     <w lemma="to" pos="prt" xml:id="A32314-003-a-0530">to</w>
     <w lemma="make" pos="vvi" xml:id="A32314-003-a-0540">make</w>
     <w lemma="good" pos="j" xml:id="A32314-003-a-0550">good</w>
     <w lemma="your" pos="po" xml:id="A32314-003-a-0560">your</w>
     <w lemma="promise" pos="n1" xml:id="A32314-003-a-0570">Promise</w>
     <pc xml:id="A32314-003-a-0580">;</pc>
     <w lemma="and" pos="cc" xml:id="A32314-003-a-0590">and</w>
     <w join="right" lemma="it" pos="pn" xml:id="A32314-003-a-0600">'t</w>
     <w join="left" lemma="be" pos="vvz" xml:id="A32314-003-a-0601">is</w>
     <w lemma="high" pos="j" xml:id="A32314-003-a-0610">high</w>
     <w lemma="time" pos="n1" xml:id="A32314-003-a-0620">time</w>
     <w lemma="for" pos="acp" xml:id="A32314-003-a-0630">for</w>
     <w lemma="you" pos="pn" xml:id="A32314-003-a-0640">you</w>
     <w lemma="to" pos="prt" xml:id="A32314-003-a-0650">to</w>
     <w lemma="be" pos="vvi" xml:id="A32314-003-a-0660">be</w>
     <w lemma="in" pos="acp" xml:id="A32314-003-a-0670">in</w>
     <w lemma="the" pos="d" xml:id="A32314-003-a-0680">the</w>
     <w lemma="country" pos="n1" reg="Country" xml:id="A32314-003-a-0690">Countrey</w>
     <pc xml:id="A32314-003-a-0700">;</pc>
     <w lemma="as" pos="acp" xml:id="A32314-003-a-0710">as</w>
     <w lemma="well" pos="av" xml:id="A32314-003-a-0720">well</w>
     <w lemma="for" pos="acp" xml:id="A32314-003-a-0730">for</w>
     <w lemma="the" pos="d" xml:id="A32314-003-a-0740">the</w>
     <w lemma="raise" pos="n1-vg" xml:id="A32314-003-a-0750">raising</w>
     <w lemma="of" pos="acp" xml:id="A32314-003-a-0760">of</w>
     <w lemma="money" pos="n1" xml:id="A32314-003-a-0770">Money</w>
     <pc xml:id="A32314-003-a-0780">,</pc>
     <w lemma="as" pos="acp" xml:id="A32314-003-a-0790">as</w>
     <w lemma="that" pos="cs" xml:id="A32314-003-a-0800">that</w>
     <w lemma="the" pos="d" xml:id="A32314-003-a-0810">the</w>
     <w lemma="lord" pos="n2" xml:id="A32314-003-a-0820">Lords</w>
     <w lemma="lieutenant" pos="n2" xml:id="A32314-003-a-0830">Lieutenants</w>
     <w lemma="and" pos="cc" xml:id="A32314-003-a-0840">and</w>
     <w lemma="deputy-lieutenant" pos="n2" xml:id="A32314-003-a-0850">Deputy-Lieutenants</w>
     <w lemma="may" pos="vmb" xml:id="A32314-003-a-0860">may</w>
     <w lemma="watch" pos="vvi" xml:id="A32314-003-a-0870">watch</w>
     <w lemma="those" pos="d" xml:id="A32314-003-a-0880">those</w>
     <w lemma="seditious" pos="j" xml:id="A32314-003-a-0890">seditious</w>
     <w lemma="spirit" pos="n2" xml:id="A32314-003-a-0900">Spirits</w>
     <w lemma="which" pos="crq" xml:id="A32314-003-a-0910">which</w>
     <w lemma="be" pos="vvb" xml:id="A32314-003-a-0920">are</w>
     <w lemma="at" pos="acp" xml:id="A32314-003-a-0930">at</w>
     <w lemma="work" pos="n1" xml:id="A32314-003-a-0940">work</w>
     <w lemma="to" pos="prt" xml:id="A32314-003-a-0950">to</w>
     <w lemma="disturb" pos="vvi" xml:id="A32314-003-a-0960">disturb</w>
     <w lemma="the" pos="d" xml:id="A32314-003-a-0970">the</w>
     <w lemma="public" pos="j" reg="Public" xml:id="A32314-003-a-0980">Publick</w>
     <w lemma="peace" pos="n1" xml:id="A32314-003-a-0990">peace</w>
     <pc unit="sentence" xml:id="A32314-003-a-1000">.</pc>
     <w lemma="and" pos="cc" xml:id="A32314-003-a-1010">And</w>
     <w lemma="therefore" pos="av" xml:id="A32314-003-a-1020">therefore</w>
     <w lemma="I" pos="pns" xml:id="A32314-003-a-1030">I</w>
     <w lemma="be" pos="vvm" xml:id="A32314-003-a-1040">am</w>
     <w lemma="resolve" pos="vvn" xml:id="A32314-003-a-1050">resolved</w>
     <w lemma="to" pos="prt" xml:id="A32314-003-a-1060">to</w>
     <w lemma="put" pos="vvi" xml:id="A32314-003-a-1070">put</w>
     <w lemma="a" pos="d" xml:id="A32314-003-a-1080">an</w>
     <w lemma="end" pos="n1" xml:id="A32314-003-a-1090">end</w>
     <w lemma="to" pos="acp" xml:id="A32314-003-a-1100">to</w>
     <w lemma="this" pos="d" xml:id="A32314-003-a-1110">this</w>
     <w lemma="session" pos="n1" xml:id="A32314-003-a-1120">Session</w>
     <w lemma="on" pos="acp" xml:id="A32314-003-a-1130">on</w>
     <w lemma="Monday" pos="nn1" xml:id="A32314-003-a-1140">Monday</w>
     <w lemma="next" pos="ord" xml:id="A32314-003-a-1150">next</w>
     <w lemma="come" pos="vvb" xml:id="A32314-003-a-1160">come</w>
     <w lemma="seven-night" pos="n1" reg="seven-night" xml:id="A32314-003-a-1170">sevennight</w>
     <pc xml:id="A32314-003-a-1180">,</pc>
     <w lemma="before" pos="acp" xml:id="A32314-003-a-1190">before</w>
     <w lemma="which" pos="crq" xml:id="A32314-003-a-1200">which</w>
     <w lemma="time" pos="n1" xml:id="A32314-003-a-1210">time</w>
     <w lemma="I" pos="pns" xml:id="A32314-003-a-1220">I</w>
     <w lemma="pray" pos="vvb" xml:id="A32314-003-a-1230">pray</w>
     <w lemma="let" pos="vvb" xml:id="A32314-003-a-1240">let</w>
     <w lemma="all" pos="d" xml:id="A32314-003-a-1250">all</w>
     <w lemma="thing" pos="n2" xml:id="A32314-003-a-1260">things</w>
     <w lemma="be" pos="vvb" xml:id="A32314-003-a-1270">be</w>
     <w lemma="make" pos="vvn" xml:id="A32314-003-a-1280">made</w>
     <w lemma="ready" pos="j" xml:id="A32314-003-a-1290">ready</w>
     <w lemma="that" pos="cs" xml:id="A32314-003-a-1300">that</w>
     <w lemma="I" pos="pns" xml:id="A32314-003-a-1310">I</w>
     <w lemma="be" pos="vvm" xml:id="A32314-003-a-1320">am</w>
     <w lemma="to" pos="prt" xml:id="A32314-003-a-1330">to</w>
     <w lemma="dispatch" pos="vvi" xml:id="A32314-003-a-1340">dispatch</w>
     <pc unit="sentence" xml:id="A32314-003-a-1350">.</pc>
     <w lemma="I" pos="pns" xml:id="A32314-003-a-1360">I</w>
     <w lemma="be" pos="vvm" xml:id="A32314-003-a-1370">am</w>
     <w lemma="not" pos="xx" xml:id="A32314-003-a-1380">not</w>
     <w lemma="willing" pos="j" xml:id="A32314-003-a-1390">willing</w>
     <w lemma="to" pos="prt" xml:id="A32314-003-a-1400">to</w>
     <w lemma="complain" pos="vvi" xml:id="A32314-003-a-1410">complain</w>
     <w lemma="you" pos="pn" xml:id="A32314-003-a-1420">you</w>
     <w lemma="have" pos="vvb" xml:id="A32314-003-a-1430">have</w>
     <w lemma="deal" pos="vvn" xml:id="A32314-003-a-1440">dealt</w>
     <w lemma="unkind" pos="av-j" xml:id="A32314-003-a-1450">unkindly</w>
     <w lemma="with" pos="acp" xml:id="A32314-003-a-1460">with</w>
     <w lemma="i" pos="pno" xml:id="A32314-003-a-1470">Me</w>
     <w lemma="in" pos="acp" xml:id="A32314-003-a-1480">in</w>
     <w lemma="a" pos="d" xml:id="A32314-003-a-1490">a</w>
     <w lemma="bill" pos="n1" xml:id="A32314-003-a-1500">Bill</w>
     <w lemma="I" pos="pns" xml:id="A32314-003-a-1510">I</w>
     <w lemma="have" pos="vvb" xml:id="A32314-003-a-1520">have</w>
     <w lemma="now" pos="av" xml:id="A32314-003-a-1530">now</w>
     <w lemma="pass" pos="vvn" xml:id="A32314-003-a-1540">passed</w>
     <pc xml:id="A32314-003-a-1550">,</pc>
     <w lemma="in" pos="acp" xml:id="A32314-003-a-1560">in</w>
     <w lemma="which" pos="crq" xml:id="A32314-003-a-1570">which</w>
     <w lemma="you" pos="pn" xml:id="A32314-003-a-1580">you</w>
     <w lemma="have" pos="vvb" xml:id="A32314-003-a-1590">have</w>
     <w lemma="manifest" pos="vvn" xml:id="A32314-003-a-1600">manifested</w>
     <w lemma="a" pos="d" xml:id="A32314-003-a-1610">a</w>
     <w lemma="great" pos="jc" xml:id="A32314-003-a-1620">greater</w>
     <w lemma="distrust" pos="n1" xml:id="A32314-003-a-1630">distrust</w>
     <w lemma="of" pos="acp" xml:id="A32314-003-a-1640">of</w>
     <w lemma="i" pos="pno" xml:id="A32314-003-a-1650">Me</w>
     <w lemma="than" pos="cs" reg="than" xml:id="A32314-003-a-1660">then</w>
     <w lemma="I" pos="pns" xml:id="A32314-003-a-1670">I</w>
     <w lemma="have" pos="vvb" xml:id="A32314-003-a-1680">have</w>
     <w lemma="deserve" pos="vvn" xml:id="A32314-003-a-1690">deserved</w>
     <pc unit="sentence" xml:id="A32314-003-a-1700">.</pc>
     <w lemma="I" pos="pns" xml:id="A32314-003-a-1710">I</w>
     <w lemma="do" pos="vvb" xml:id="A32314-003-a-1720">do</w>
     <w lemma="not" pos="xx" xml:id="A32314-003-a-1730">not</w>
     <w lemma="pretend" pos="vvi" xml:id="A32314-003-a-1740">pretend</w>
     <w lemma="to" pos="prt" xml:id="A32314-003-a-1750">to</w>
     <w lemma="be" pos="vvi" xml:id="A32314-003-a-1760">be</w>
     <w lemma="without" pos="acp" xml:id="A32314-003-a-1770">without</w>
     <w lemma="infirmity" pos="n2" xml:id="A32314-003-a-1780">infirmities</w>
     <pc xml:id="A32314-003-a-1790">,</pc>
     <w lemma="but" pos="acp" xml:id="A32314-003-a-1800">but</w>
     <w lemma="I" pos="pns" xml:id="A32314-003-a-1810">I</w>
     <w lemma="have" pos="vvb" xml:id="A32314-003-a-1820">have</w>
     <w lemma="never" pos="avx" xml:id="A32314-003-a-1830">never</w>
     <w lemma="break" pos="vvn" xml:id="A32314-003-a-1840">broken</w>
     <w lemma="my" pos="po" xml:id="A32314-003-a-1850">My</w>
     <w lemma="word" pos="n1" xml:id="A32314-003-a-1860">word</w>
     <w lemma="with" pos="acp" xml:id="A32314-003-a-1870">with</w>
     <w lemma="you" pos="pn" xml:id="A32314-003-a-1880">you</w>
     <pc xml:id="A32314-003-a-1890">;</pc>
     <w lemma="and" pos="cc" xml:id="A32314-003-a-1900">and</w>
     <w lemma="if" pos="cs" xml:id="A32314-003-a-1910">if</w>
     <w lemma="I" pos="pns" xml:id="A32314-003-a-1920">I</w>
     <w lemma="do" pos="vvb" xml:id="A32314-003-a-1930">do</w>
     <w lemma="not" pos="xx" xml:id="A32314-003-a-1940">not</w>
     <w lemma="flatter" pos="vvi" xml:id="A32314-003-a-1950">flatter</w>
     <w lemma="myself" pos="pr" reg="Myself" xml:id="A32314-003-a-1960">My self</w>
     <pc xml:id="A32314-003-a-1980">,</pc>
     <w lemma="the" pos="d" xml:id="A32314-003-a-1990">the</w>
     <w lemma="nation" pos="n1" xml:id="A32314-003-a-2000">Nation</w>
     <w lemma="never" pos="avx" xml:id="A32314-003-a-2010">never</w>
     <w lemma="have" pos="vvd" xml:id="A32314-003-a-2020">had</w>
     <w lemma="less" pos="dc" xml:id="A32314-003-a-2030">less</w>
     <w lemma="cause" pos="n1" xml:id="A32314-003-a-2040">cause</w>
     <w lemma="to" pos="prt" xml:id="A32314-003-a-2050">to</w>
     <w lemma="complain" pos="vvi" xml:id="A32314-003-a-2060">complain</w>
     <w lemma="of" pos="acp" xml:id="A32314-003-a-2070">of</w>
     <w lemma="grievance" pos="n2" xml:id="A32314-003-a-2080">Grievances</w>
     <pc xml:id="A32314-003-a-2090">,</pc>
     <w lemma="or" pos="cc" xml:id="A32314-003-a-2100">or</w>
     <w lemma="the" pos="d" xml:id="A32314-003-a-2110">the</w>
     <w lemma="least" pos="ds" xml:id="A32314-003-a-2120">least</w>
     <w lemma="injustice" pos="n1" xml:id="A32314-003-a-2130">Injustice</w>
     <w lemma="or" pos="cc" xml:id="A32314-003-a-2140">or</w>
     <w lemma="oppression" pos="n1" xml:id="A32314-003-a-2150">Oppression</w>
     <pc xml:id="A32314-003-a-2160">,</pc>
     <w lemma="than" pos="cs" reg="than" xml:id="A32314-003-a-2170">then</w>
     <w lemma="it" pos="pn" xml:id="A32314-003-a-2180">it</w>
     <w lemma="have" pos="vvz" xml:id="A32314-003-a-2190">hath</w>
     <w lemma="have" pos="vvn" xml:id="A32314-003-a-2200">had</w>
     <w lemma="in" pos="acp" xml:id="A32314-003-a-2210">in</w>
     <w lemma="these" pos="d" xml:id="A32314-003-a-2220">these</w>
     <w lemma="seven" pos="crd" xml:id="A32314-003-a-2230">Seven</w>
     <w lemma="year" pos="n2" xml:id="A32314-003-a-2240">years</w>
     <w lemma="it" pos="pn" xml:id="A32314-003-a-2250">it</w>
     <w lemma="have" pos="vvz" xml:id="A32314-003-a-2260">hath</w>
     <w lemma="please" pos="vvn" xml:id="A32314-003-a-2270">pleased</w>
     <w lemma="God" pos="nn1" xml:id="A32314-003-a-2280">God</w>
     <w lemma="to" pos="prt" xml:id="A32314-003-a-2290">to</w>
     <w lemma="restore" pos="vvi" xml:id="A32314-003-a-2300">restore</w>
     <w lemma="i" pos="pno" xml:id="A32314-003-a-2310">Me</w>
     <w lemma="to" pos="acp" xml:id="A32314-003-a-2320">to</w>
     <w lemma="you" pos="pn" xml:id="A32314-003-a-2330">you</w>
     <pc xml:id="A32314-003-a-2340">:</pc>
     <w lemma="I" pos="pns" xml:id="A32314-003-a-2350">I</w>
     <w lemma="will" pos="vmd" xml:id="A32314-003-a-2360">would</w>
     <w lemma="be" pos="vvi" xml:id="A32314-003-a-2370">be</w>
     <w lemma="glad" pos="j" xml:id="A32314-003-a-2380">glad</w>
     <w lemma="to" pos="prt" xml:id="A32314-003-a-2390">to</w>
     <w lemma="be" pos="vvi" xml:id="A32314-003-a-2400">be</w>
     <w lemma="use" pos="vvn" xml:id="A32314-003-a-2410">used</w>
     <w lemma="according" pos="av-j" xml:id="A32314-003-a-2420">accordingly</w>
     <pc unit="sentence" xml:id="A32314-003-a-2430">.</pc>
    </p>
    <trailer xml:id="A32314-e10250">
     <w lemma="n/a" pos="fla" xml:id="A32314-003-a-2440">FINIS</w>
     <pc unit="sentence" xml:id="A32314-003-a-2450">.</pc>
    </trailer>
    <pb facs="tcp:54601:3" rend="simple:additions" xml:id="A32314-003-b"/>
   </div>
  </body>
  <back xml:id="A32314-e10010-b"/>
 </text>
</TEI>
