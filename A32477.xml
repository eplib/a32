<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A32477">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>By the King, a proclamation for proroguing the Parliament until the nineteenth day of October next</title>
    <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A32477 of text R39177 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing C3382_VARIANT). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A32477</idno>
    <idno type="STC">Wing C3382_VARIANT</idno>
    <idno type="STC">ESTC R39177</idno>
    <idno type="EEBO-CITATION">18241253</idno>
    <idno type="OCLC">ocm 18241253</idno>
    <idno type="VID">107245</idno>
    <idno type="PROQUESTGOID">2248541726</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A32477)</note>
    <note>Transcribed from: (Early English Books Online ; image set 107245)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1629:87)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>By the King, a proclamation for proroguing the Parliament until the nineteenth day of October next</title>
      <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
      <author>Charles II, King of England, 1630-1685.</author>
     </titleStmt>
     <extent>1 broadside.</extent>
     <publicationStmt>
      <publisher>Printed by the assigns of John Bill and Christopher Barker ...,</publisher>
      <pubPlace>[S.l.] :</pubPlace>
      <date>1668.</date>
     </publicationStmt>
     <notesStmt>
      <note>"Given at our court at Whitehall the eighteenth day of December, in the twentieth year of our reign. 1668."</note>
      <note>Despite the information in the reel guide, this item is a variant of Wing number C3382 because it lacks the "In the Savoy" place of publication.</note>
      <note>Reproduction of original in the Huntington Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>England and Wales. -- Parliament.</term>
     <term>Great Britain -- History -- Charles II, 1660-1685.</term>
     <term>Great Britain -- Politics and government -- 1660-1688.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>By the King. A proclamation for proroguing the Parliament until the nineteenth day of October next.</ep:title>
    <ep:author>England and Wales. Sovereign </ep:author>
    <ep:publicationYear>1668</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>258</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-03</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-08</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-09</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-09</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A32477-t">
  <body xml:id="A32477-e0">
   <div type="royal_proclamation" xml:id="A32477-e10">
    <pb facs="tcp:107245:1" rend="simple:additions" xml:id="A32477-001-a"/>
    <head xml:id="A32477-e20">
     <figure xml:id="A32477-e30">
      <p xml:id="A32477-e40">
       <w lemma="n/a" pos="ffr" xml:id="A32477-001-a-0010">DIEV</w>
       <w lemma="n/a" pos="ffr" xml:id="A32477-001-a-0020">ET</w>
       <w lemma="n/a" pos="ffr" xml:id="A32477-001-a-0030">MON</w>
       <w lemma="n/a" pos="ffr" xml:id="A32477-001-a-0040">DROIT</w>
      </p>
      <p xml:id="A32477-e50">
       <w lemma="n/a" pos="ffr" xml:id="A32477-001-a-0050">HONI</w>
       <w lemma="n/a" pos="ffr" xml:id="A32477-001-a-0060">SOIT</w>
       <w lemma="n/a" pos="ffr" xml:id="A32477-001-a-0070">QVI</w>
       <w lemma="n/a" pos="ffr" xml:id="A32477-001-a-0080">MAL</w>
       <w lemma="n/a" pos="ffr" xml:id="A32477-001-a-0090">Y</w>
       <w lemma="n/a" pos="ffr" xml:id="A32477-001-a-0100">PENSE</w>
      </p>
      <figDesc xml:id="A32477-e60">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <byline xml:id="A32477-e70">
     <w lemma="by" pos="acp" xml:id="A32477-001-a-0110">By</w>
     <w lemma="the" pos="d" xml:id="A32477-001-a-0120">the</w>
     <w lemma="King" pos="n1" xml:id="A32477-001-a-0130">King</w>
     <pc unit="sentence" xml:id="A32477-001-a-0131">.</pc>
    </byline>
    <head xml:id="A32477-e80">
     <w lemma="a" pos="d" xml:id="A32477-001-a-0150">A</w>
     <w lemma="proclamation" pos="n1" xml:id="A32477-001-a-0160">PROCLAMATION</w>
     <w lemma="for" pos="acp" xml:id="A32477-001-a-0170">For</w>
     <w lemma="prorogue" pos="vvg" xml:id="A32477-001-a-0180">Proroguing</w>
     <w lemma="the" pos="d" xml:id="A32477-001-a-0190">the</w>
     <w lemma="parliament" pos="n1" xml:id="A32477-001-a-0200">Parliament</w>
     <w lemma="until" pos="acp" xml:id="A32477-001-a-0210">until</w>
     <w lemma="the" pos="d" xml:id="A32477-001-a-0220">the</w>
     <w lemma="nineteen" pos="ord" xml:id="A32477-001-a-0230">Nineteenth</w>
     <w lemma="day" pos="n1" xml:id="A32477-001-a-0240">day</w>
     <w lemma="of" pos="acp" xml:id="A32477-001-a-0250">of</w>
     <w lemma="October" pos="nn1" rend="hi" xml:id="A32477-001-a-0260">October</w>
     <w lemma="next" pos="ord" xml:id="A32477-001-a-0270">next</w>
     <pc unit="sentence" xml:id="A32477-001-a-0280">.</pc>
    </head>
    <opener xml:id="A32477-e100">
     <signed xml:id="A32477-e110">
      <w lemma="CHARLES" pos="nn1" xml:id="A32477-001-a-0290">CHARLES</w>
      <w lemma="r." pos="ab" xml:id="A32477-001-a-0300">R.</w>
      <pc unit="sentence" xml:id="A32477-001-a-0310"/>
     </signed>
    </opener>
    <p xml:id="A32477-e120">
     <w lemma="whereas" pos="cs" rend="decorinit" xml:id="A32477-001-a-0320">WHereas</w>
     <w lemma="the" pos="d" xml:id="A32477-001-a-0330">the</w>
     <w lemma="two" pos="crd" xml:id="A32477-001-a-0340">Two</w>
     <w lemma="house" pos="n2" xml:id="A32477-001-a-0350">Houses</w>
     <w lemma="of" pos="acp" xml:id="A32477-001-a-0360">of</w>
     <w lemma="parliament" pos="n1" xml:id="A32477-001-a-0370">Parliament</w>
     <w lemma="have" pos="vvb" xml:id="A32477-001-a-0380">have</w>
     <w lemma="by" pos="acp" xml:id="A32477-001-a-0390">by</w>
     <w lemma="our" pos="po" xml:id="A32477-001-a-0400">Our</w>
     <w lemma="direction" pos="n1" xml:id="A32477-001-a-0410">direction</w>
     <w lemma="adjourn" pos="vvn" xml:id="A32477-001-a-0420">Adjourned</w>
     <w lemma="themselves" pos="pr" xml:id="A32477-001-a-0430">themselves</w>
     <w lemma="until" pos="acp" xml:id="A32477-001-a-0440">until</w>
     <w lemma="the" pos="d" xml:id="A32477-001-a-0450">the</w>
     <w lemma="first" pos="ord" xml:id="A32477-001-a-0460">First</w>
     <w lemma="day" pos="n1" xml:id="A32477-001-a-0470">day</w>
     <w lemma="of" pos="acp" xml:id="A32477-001-a-0480">of</w>
     <w lemma="March" pos="nn1" rend="hi" xml:id="A32477-001-a-0490">March</w>
     <w lemma="next" pos="ord" xml:id="A32477-001-a-0500">next</w>
     <pc xml:id="A32477-001-a-0510">,</pc>
     <w lemma="we" pos="pns" xml:id="A32477-001-a-0520">We</w>
     <w lemma="for" pos="acp" xml:id="A32477-001-a-0530">for</w>
     <w lemma="many" pos="d" xml:id="A32477-001-a-0540">many</w>
     <w lemma="weighty" pos="j" xml:id="A32477-001-a-0550">weighty</w>
     <w lemma="reason" pos="n2" xml:id="A32477-001-a-0560">Reasons</w>
     <w lemma="have" pos="vvb" xml:id="A32477-001-a-0570">have</w>
     <w lemma="think" pos="vvn" xml:id="A32477-001-a-0580">thought</w>
     <w lemma="fit" pos="j" xml:id="A32477-001-a-0590">fit</w>
     <w lemma="and" pos="cc" xml:id="A32477-001-a-0600">and</w>
     <w lemma="resolve" pos="vvn" xml:id="A32477-001-a-0610">resolved</w>
     <w lemma="to" pos="prt" xml:id="A32477-001-a-0620">to</w>
     <w lemma="make" pos="vvi" xml:id="A32477-001-a-0630">make</w>
     <w lemma="a" pos="d" xml:id="A32477-001-a-0640">a</w>
     <w lemma="prorogation" pos="n1" xml:id="A32477-001-a-0650">Prorogation</w>
     <w lemma="of" pos="acp" xml:id="A32477-001-a-0660">of</w>
     <w lemma="the" pos="d" xml:id="A32477-001-a-0670">the</w>
     <w lemma="parliament" pos="n1" xml:id="A32477-001-a-0680">Parliament</w>
     <w lemma="until" pos="acp" xml:id="A32477-001-a-0690">until</w>
     <w lemma="the" pos="d" xml:id="A32477-001-a-0700">the</w>
     <w lemma="nineteen" pos="ord" xml:id="A32477-001-a-0710">Nineteenth</w>
     <w lemma="day" pos="n1" xml:id="A32477-001-a-0720">day</w>
     <w lemma="of" pos="acp" xml:id="A32477-001-a-0730">of</w>
     <w lemma="October" pos="nn1" rend="hi" xml:id="A32477-001-a-0740">October</w>
     <w lemma="next" pos="ord" xml:id="A32477-001-a-0750">next</w>
     <pc unit="sentence" xml:id="A32477-001-a-0760">.</pc>
     <w lemma="and" pos="cc" xml:id="A32477-001-a-0770">And</w>
     <w lemma="therefore" pos="av" xml:id="A32477-001-a-0780">therefore</w>
     <w lemma="do" pos="vvb" xml:id="A32477-001-a-0790">do</w>
     <w lemma="by" pos="acp" xml:id="A32477-001-a-0800">by</w>
     <w lemma="this" pos="d" xml:id="A32477-001-a-0810">this</w>
     <w lemma="our" pos="po" xml:id="A32477-001-a-0820">Our</w>
     <w lemma="royal" pos="j" xml:id="A32477-001-a-0830">Royal</w>
     <w lemma="proclamation" pos="n1" xml:id="A32477-001-a-0840">Proclamation</w>
     <w lemma="publish" pos="vvi" xml:id="A32477-001-a-0850">publish</w>
     <pc xml:id="A32477-001-a-0860">,</pc>
     <w lemma="notify" pos="vvi" reg="notify" xml:id="A32477-001-a-0870">notifie</w>
     <pc xml:id="A32477-001-a-0880">,</pc>
     <w lemma="and" pos="cc" xml:id="A32477-001-a-0890">and</w>
     <w lemma="declare" pos="vvi" xml:id="A32477-001-a-0900">declare</w>
     <pc xml:id="A32477-001-a-0910">,</pc>
     <w lemma="that" pos="cs" xml:id="A32477-001-a-0920">That</w>
     <w lemma="we" pos="pns" xml:id="A32477-001-a-0930">We</w>
     <w lemma="do" pos="vvb" xml:id="A32477-001-a-0940">do</w>
     <w lemma="intend" pos="vvi" xml:id="A32477-001-a-0950">intend</w>
     <w lemma="that" pos="cs" xml:id="A32477-001-a-0960">that</w>
     <w lemma="the" pos="d" xml:id="A32477-001-a-0970">the</w>
     <w lemma="parliament" pos="n1" xml:id="A32477-001-a-0980">Parliament</w>
     <w lemma="shall" pos="vmb" xml:id="A32477-001-a-0990">shall</w>
     <w lemma="be" pos="vvi" xml:id="A32477-001-a-1000">be</w>
     <w lemma="prorogue" pos="vvn" xml:id="A32477-001-a-1010">Prorogued</w>
     <w lemma="upon" pos="acp" xml:id="A32477-001-a-1020">upon</w>
     <w lemma="and" pos="cc" xml:id="A32477-001-a-1030">and</w>
     <w lemma="from" pos="acp" xml:id="A32477-001-a-1040">from</w>
     <w lemma="the" pos="d" xml:id="A32477-001-a-1050">the</w>
     <w lemma="say" pos="vvd" xml:id="A32477-001-a-1060">said</w>
     <w lemma="first" pos="ord" xml:id="A32477-001-a-1070">First</w>
     <w lemma="day" pos="n1" xml:id="A32477-001-a-1080">day</w>
     <w lemma="of" pos="acp" xml:id="A32477-001-a-1090">of</w>
     <hi xml:id="A32477-e150">
      <w lemma="march" pos="n1" xml:id="A32477-001-a-1100">March</w>
      <pc xml:id="A32477-001-a-1110">,</pc>
     </hi>
     <w lemma="until" pos="acp" xml:id="A32477-001-a-1120">until</w>
     <w lemma="the" pos="d" xml:id="A32477-001-a-1130">the</w>
     <w lemma="say" pos="vvd" xml:id="A32477-001-a-1140">said</w>
     <w lemma="nineteen" pos="ord" xml:id="A32477-001-a-1150">Nineteenth</w>
     <w lemma="day" pos="n1" xml:id="A32477-001-a-1160">day</w>
     <w lemma="of" pos="acp" xml:id="A32477-001-a-1170">of</w>
     <w lemma="October" pos="nn1" rend="hi" xml:id="A32477-001-a-1180">October</w>
     <w lemma="next" pos="ord" xml:id="A32477-001-a-1190">next</w>
     <pc xml:id="A32477-001-a-1200">:</pc>
     <w lemma="whereof" pos="crq" xml:id="A32477-001-a-1210">Whereof</w>
     <w lemma="the" pos="d" xml:id="A32477-001-a-1220">the</w>
     <w lemma="lord" pos="n2" xml:id="A32477-001-a-1230">Lords</w>
     <w lemma="spiritual" pos="j" xml:id="A32477-001-a-1240">Spiritual</w>
     <w lemma="and" pos="cc" xml:id="A32477-001-a-1250">and</w>
     <w lemma="temporal" pos="j" xml:id="A32477-001-a-1260">Temporal</w>
     <pc xml:id="A32477-001-a-1270">,</pc>
     <w lemma="knight" pos="n2" xml:id="A32477-001-a-1280">Knights</w>
     <pc xml:id="A32477-001-a-1290">,</pc>
     <w lemma="citizen" pos="n2" xml:id="A32477-001-a-1300">Citizens</w>
     <w lemma="and" pos="cc" xml:id="A32477-001-a-1310">and</w>
     <w lemma="burgess" pos="n2" xml:id="A32477-001-a-1320">Burgesses</w>
     <pc xml:id="A32477-001-a-1330">,</pc>
     <w lemma="and" pos="cc" xml:id="A32477-001-a-1340">and</w>
     <w lemma="all" pos="d" xml:id="A32477-001-a-1350">all</w>
     <w lemma="other" pos="pi2-d" xml:id="A32477-001-a-1360">others</w>
     <w lemma="who" pos="crq" xml:id="A32477-001-a-1370">whom</w>
     <w lemma="it" pos="pn" xml:id="A32477-001-a-1380">it</w>
     <w lemma="may" pos="vmb" xml:id="A32477-001-a-1390">may</w>
     <w lemma="concern" pos="vvi" xml:id="A32477-001-a-1400">concern</w>
     <pc xml:id="A32477-001-a-1410">,</pc>
     <w lemma="may" pos="vmb" xml:id="A32477-001-a-1420">may</w>
     <w lemma="hereby" pos="av" xml:id="A32477-001-a-1430">hereby</w>
     <w lemma="take" pos="vvi" xml:id="A32477-001-a-1440">take</w>
     <w lemma="notice" pos="n1" xml:id="A32477-001-a-1450">notice</w>
     <pc xml:id="A32477-001-a-1460">,</pc>
     <w lemma="and" pos="cc" xml:id="A32477-001-a-1470">and</w>
     <w lemma="order" pos="vvi" xml:id="A32477-001-a-1480">order</w>
     <w lemma="their" pos="po" xml:id="A32477-001-a-1490">their</w>
     <w lemma="affair" pos="n2" xml:id="A32477-001-a-1500">affairs</w>
     <w lemma="according" pos="av-j" xml:id="A32477-001-a-1510">accordingly</w>
     <pc xml:id="A32477-001-a-1520">;</pc>
     <w lemma="we" pos="pns" xml:id="A32477-001-a-1530">We</w>
     <w lemma="let" pos="vvg" xml:id="A32477-001-a-1540">letting</w>
     <w lemma="they" pos="pno" xml:id="A32477-001-a-1550">them</w>
     <w lemma="know" pos="vvi" xml:id="A32477-001-a-1560">know</w>
     <pc xml:id="A32477-001-a-1570">,</pc>
     <w lemma="that" pos="cs" xml:id="A32477-001-a-1580">That</w>
     <w lemma="we" pos="pns" xml:id="A32477-001-a-1590">We</w>
     <w lemma="will" pos="vmb" xml:id="A32477-001-a-1600">will</w>
     <w lemma="not" pos="xx" xml:id="A32477-001-a-1610">not</w>
     <w lemma="at" pos="acp" xml:id="A32477-001-a-1620">at</w>
     <w lemma="the" pos="d" xml:id="A32477-001-a-1630">the</w>
     <w lemma="say" pos="vvn" xml:id="A32477-001-a-1640">said</w>
     <w lemma="first" pos="ord" xml:id="A32477-001-a-1650">First</w>
     <w lemma="day" pos="n1" xml:id="A32477-001-a-1660">day</w>
     <w lemma="of" pos="acp" xml:id="A32477-001-a-1670">of</w>
     <hi xml:id="A32477-e170">
      <w lemma="march" pos="n1" xml:id="A32477-001-a-1680">March</w>
      <pc xml:id="A32477-001-a-1690">,</pc>
     </hi>
     <w lemma="expect" pos="vvb" xml:id="A32477-001-a-1700">expect</w>
     <w lemma="the" pos="d" xml:id="A32477-001-a-1710">the</w>
     <w lemma="attendance" pos="n1" xml:id="A32477-001-a-1720">attendance</w>
     <w lemma="of" pos="acp" xml:id="A32477-001-a-1730">of</w>
     <w lemma="any" pos="d" xml:id="A32477-001-a-1740">any</w>
     <pc xml:id="A32477-001-a-1750">,</pc>
     <w lemma="but" pos="acp" xml:id="A32477-001-a-1760">but</w>
     <w lemma="only" pos="av-j" reg="only" xml:id="A32477-001-a-1770">onely</w>
     <w lemma="such" pos="d" xml:id="A32477-001-a-1780">such</w>
     <w lemma="as" pos="acp" xml:id="A32477-001-a-1790">as</w>
     <w lemma="be" pos="vvg" xml:id="A32477-001-a-1800">being</w>
     <w lemma="in" pos="acp" xml:id="A32477-001-a-1810">in</w>
     <w lemma="or" pos="cc" xml:id="A32477-001-a-1820">or</w>
     <w lemma="about" pos="acp" xml:id="A32477-001-a-1830">about</w>
     <w lemma="the" pos="d" xml:id="A32477-001-a-1840">the</w>
     <w lemma="city" pos="n2" xml:id="A32477-001-a-1850">Cities</w>
     <w lemma="of" pos="acp" xml:id="A32477-001-a-1860">of</w>
     <w lemma="London" pos="nn1" rend="hi" xml:id="A32477-001-a-1870">London</w>
     <w lemma="or" pos="cc" xml:id="A32477-001-a-1880">or</w>
     <hi xml:id="A32477-e190">
      <w lemma="Westminster" pos="nn1" xml:id="A32477-001-a-1890">Westminster</w>
      <pc xml:id="A32477-001-a-1900">,</pc>
     </hi>
     <w lemma="may" pos="vmb" xml:id="A32477-001-a-1910">may</w>
     <w lemma="attend" pos="vvi" xml:id="A32477-001-a-1920">attend</w>
     <w lemma="the" pos="d" xml:id="A32477-001-a-1930">the</w>
     <w lemma="make" pos="n1-vg" xml:id="A32477-001-a-1940">making</w>
     <w lemma="of" pos="acp" xml:id="A32477-001-a-1950">of</w>
     <w lemma="the" pos="d" xml:id="A32477-001-a-1960">the</w>
     <w lemma="say" pos="j-vn" xml:id="A32477-001-a-1970">said</w>
     <w lemma="prorogation" pos="n1" xml:id="A32477-001-a-1980">Prorogation</w>
     <pc xml:id="A32477-001-a-1990">,</pc>
     <w lemma="as" pos="acp" xml:id="A32477-001-a-2000">as</w>
     <w lemma="heretofore" pos="av" xml:id="A32477-001-a-2010">heretofore</w>
     <w lemma="have" pos="vvz" xml:id="A32477-001-a-2020">hath</w>
     <w lemma="in" pos="acp" xml:id="A32477-001-a-2030">in</w>
     <w lemma="like" pos="j" xml:id="A32477-001-a-2040">like</w>
     <w lemma="case" pos="n2" xml:id="A32477-001-a-2050">cases</w>
     <w lemma="be" pos="vvn" reg="been" xml:id="A32477-001-a-2060">beén</w>
     <w lemma="accustom" pos="vvn" xml:id="A32477-001-a-2070">accustomed</w>
     <pc unit="sentence" xml:id="A32477-001-a-2080">.</pc>
    </p>
    <closer xml:id="A32477-e200">
     <dateline xml:id="A32477-e210">
      <w lemma="give" pos="vvn" xml:id="A32477-001-a-2090">Given</w>
      <w lemma="at" pos="acp" xml:id="A32477-001-a-2100">at</w>
      <w lemma="our" pos="po" xml:id="A32477-001-a-2110">Our</w>
      <w lemma="court" pos="n1" xml:id="A32477-001-a-2120">Court</w>
      <w lemma="at" pos="acp" xml:id="A32477-001-a-2130">at</w>
      <w lemma="Whitehall" pos="nn1" rend="hi" xml:id="A32477-001-a-2140">Whitehall</w>
      <date xml:id="A32477-e230">
       <w lemma="the" pos="d" xml:id="A32477-001-a-2150">the</w>
       <w lemma="eighteen" pos="ord" xml:id="A32477-001-a-2160">Eighteenth</w>
       <w lemma="day" pos="n1" xml:id="A32477-001-a-2170">day</w>
       <w lemma="of" pos="acp" xml:id="A32477-001-a-2180">of</w>
       <hi xml:id="A32477-e240">
        <w lemma="December" pos="nn1" xml:id="A32477-001-a-2190">December</w>
        <pc xml:id="A32477-001-a-2200">,</pc>
       </hi>
       <w lemma="in" pos="acp" xml:id="A32477-001-a-2210">in</w>
       <w lemma="the" pos="d" xml:id="A32477-001-a-2220">the</w>
       <w lemma="twenty" pos="ord" xml:id="A32477-001-a-2230">Twentieth</w>
       <w lemma="year" pos="n1" xml:id="A32477-001-a-2240">year</w>
       <w lemma="of" pos="acp" xml:id="A32477-001-a-2250">of</w>
       <w lemma="our" pos="po" xml:id="A32477-001-a-2260">Our</w>
       <w lemma="reign" pos="n1" xml:id="A32477-001-a-2270">Reign</w>
       <pc unit="sentence" xml:id="A32477-001-a-2280">.</pc>
       <w lemma="1668." pos="crd" xml:id="A32477-001-a-2290">1668.</w>
       <pc unit="sentence" xml:id="A32477-001-a-2300"/>
      </date>
     </dateline>
     <lb xml:id="A32477-e250"/>
     <w lemma="God" pos="nn1" xml:id="A32477-001-a-2310">God</w>
     <w lemma="save" pos="vvb" xml:id="A32477-001-a-2320">save</w>
     <w lemma="the" pos="d" xml:id="A32477-001-a-2330">the</w>
     <w lemma="King" pos="n1" xml:id="A32477-001-a-2340">King</w>
     <pc unit="sentence" xml:id="A32477-001-a-2341">.</pc>
    </closer>
   </div>
  </body>
  <back xml:id="A32477-e260">
   <div type="colophon" xml:id="A32477-e270">
    <p xml:id="A32477-e280">
     <w lemma="print" pos="j-vn" xml:id="A32477-001-a-2360">Printed</w>
     <w lemma="by" pos="acp" xml:id="A32477-001-a-2370">by</w>
     <w lemma="the" pos="d" xml:id="A32477-001-a-2380">the</w>
     <w lemma="assign" pos="vvz" xml:id="A32477-001-a-2390">Assigns</w>
     <w lemma="of" pos="acp" xml:id="A32477-001-a-2400">of</w>
     <hi xml:id="A32477-e290">
      <w lemma="John" pos="nn1" xml:id="A32477-001-a-2410">John</w>
      <w lemma="bill" pos="n1" xml:id="A32477-001-a-2420">Bill</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="A32477-001-a-2430">and</w>
     <hi xml:id="A32477-e300">
      <w lemma="Christopher" pos="nn1" xml:id="A32477-001-a-2440">Christopher</w>
      <w lemma="Barker" pos="nn1" xml:id="A32477-001-a-2450">Barker</w>
      <pc xml:id="A32477-001-a-2460">,</pc>
     </hi>
     <w lemma="printer" pos="n2" xml:id="A32477-001-a-2470">Printers</w>
     <w lemma="to" pos="acp" xml:id="A32477-001-a-2480">to</w>
     <w lemma="the" pos="d" xml:id="A32477-001-a-2490">the</w>
     <w lemma="king" pos="n2" xml:id="A32477-001-a-2500">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A32477-001-a-2510">most</w>
     <w lemma="excellent" pos="j" xml:id="A32477-001-a-2520">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A32477-001-a-2530">Majesty</w>
     <pc unit="sentence" xml:id="A32477-001-a-2540">.</pc>
     <w lemma="1668." pos="crd" xml:id="A32477-001-a-2550">1668.</w>
     <pc unit="sentence" xml:id="A32477-001-a-2560"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
