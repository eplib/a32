<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A32480">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>By the King, a proclamation for publishing the peace between His Majesty and the King of Denmark</title>
    <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A32480 of text R35856 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing C3386). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A32480</idno>
    <idno type="STC">Wing C3386</idno>
    <idno type="STC">ESTC R35856</idno>
    <idno type="EEBO-CITATION">15565363</idno>
    <idno type="OCLC">ocm 15565363</idno>
    <idno type="VID">103822</idno>
    <idno type="PROQUESTGOID">2240899096</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A32480)</note>
    <note>Transcribed from: (Early English Books Online ; image set 103822)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1588:77)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>By the King, a proclamation for publishing the peace between His Majesty and the King of Denmark</title>
      <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
      <author>Charles II, King of England, 1630-1685.</author>
     </titleStmt>
     <extent>1 broadside.</extent>
     <publicationStmt>
      <publisher>Printed by the assigns of John Bill and Christopher Barker ...,</publisher>
      <pubPlace>In the Savoy [i.e. London] :</pubPlace>
      <date>1667.</date>
     </publicationStmt>
     <notesStmt>
      <note>"Given at our court at Whitehall the four and twentieth day of August, 1667 in the nineteenth year of our reign."</note>
      <note>Reproduction of the original in the Huntington Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Great Britain -- Foreign relations -- Denmark.</term>
     <term>Denmark -- Foreign relations -- England.</term>
     <term>Great Britain -- History -- Charles II, 1660-1685.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>By the King. A proclamation for publishing the peace between His Majesty and the King of Denmark.</ep:title>
    <ep:author>England and Wales. Sovereign </ep:author>
    <ep:publicationYear>1667</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>343</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-03</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-08</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-09</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-09</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A32480-t">
  <body xml:id="A32480-e0">
   <div type="royal_proclamation" xml:id="A32480-e10">
    <pb facs="tcp:103822:1" rend="simple:additions" xml:id="A32480-001-a"/>
    <head xml:id="A32480-e20">
     <figure xml:id="A32480-e30">
      <p xml:id="A32480-e40">
       <w lemma="c" pos="sy" xml:id="A32480-001-a-0010">C</w>
       <w lemma="r" pos="sy" xml:id="A32480-001-a-0020">R</w>
      </p>
      <p xml:id="A32480-e50">
       <w lemma="n/a" pos="ffr" xml:id="A32480-001-a-0030">DIEV</w>
       <w lemma="n/a" pos="ffr" xml:id="A32480-001-a-0040">ET</w>
       <w lemma="n/a" pos="ffr" xml:id="A32480-001-a-0050">MON</w>
       <w lemma="n/a" pos="ffr" xml:id="A32480-001-a-0060">DROIT</w>
      </p>
      <p xml:id="A32480-e60">
       <w lemma="n/a" pos="ffr" xml:id="A32480-001-a-0070">HONI</w>
       <w lemma="n/a" pos="ffr" xml:id="A32480-001-a-0080">SOIT</w>
       <w lemma="n/a" pos="ffr" xml:id="A32480-001-a-0090">QVI</w>
       <w lemma="n/a" pos="ffr" xml:id="A32480-001-a-0100">MAL</w>
       <w lemma="n/a" pos="ffr" xml:id="A32480-001-a-0110">Y</w>
       <w lemma="n/a" pos="ffr" xml:id="A32480-001-a-0120">PENSE</w>
      </p>
      <figDesc xml:id="A32480-e70">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <byline xml:id="A32480-e80">
     <w lemma="by" pos="acp" xml:id="A32480-001-a-0130">By</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-0140">the</w>
     <w lemma="King" pos="n1" xml:id="A32480-001-a-0150">King</w>
     <pc unit="sentence" xml:id="A32480-001-a-0151">.</pc>
    </byline>
    <head xml:id="A32480-e90">
     <w lemma="a" pos="d" xml:id="A32480-001-a-0170">A</w>
     <w lemma="proclamation" pos="n1" xml:id="A32480-001-a-0180">PROCLAMATION</w>
     <w lemma="for" pos="acp" xml:id="A32480-001-a-0190">FOR</w>
     <w lemma="publish" pos="vvg" xml:id="A32480-001-a-0200">Publishing</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-0210">the</w>
     <w lemma="peace" pos="n1" xml:id="A32480-001-a-0220">Peace</w>
     <w lemma="between" pos="acp" xml:id="A32480-001-a-0230">between</w>
     <w lemma="his" pos="po" xml:id="A32480-001-a-0240">His</w>
     <w lemma="majesty" pos="n1" xml:id="A32480-001-a-0250">Majesty</w>
     <w lemma="and" pos="cc" xml:id="A32480-001-a-0260">and</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-0270">The</w>
     <w lemma="king" pos="n1" xml:id="A32480-001-a-0280">King</w>
     <w lemma="of" pos="acp" xml:id="A32480-001-a-0290">of</w>
     <w lemma="Denmark" pos="nn1" xml:id="A32480-001-a-0300">Denmark</w>
     <pc unit="sentence" xml:id="A32480-001-a-0310">.</pc>
    </head>
    <opener xml:id="A32480-e100">
     <signed xml:id="A32480-e110">
      <w lemma="CHARLES" pos="nn1" xml:id="A32480-001-a-0320">CHARLES</w>
      <w lemma="r." pos="ab" xml:id="A32480-001-a-0330">R.</w>
      <pc unit="sentence" xml:id="A32480-001-a-0340"/>
     </signed>
    </opener>
    <p xml:id="A32480-e120">
     <w lemma="whereas" pos="cs" rend="decorinit" xml:id="A32480-001-a-0350">WHereas</w>
     <w lemma="a" pos="d" xml:id="A32480-001-a-0360">a</w>
     <w lemma="peace" pos="n1" xml:id="A32480-001-a-0370">Peace</w>
     <w lemma="have" pos="vvz" xml:id="A32480-001-a-0380">hath</w>
     <w lemma="be" pos="vvn" xml:id="A32480-001-a-0390">been</w>
     <w lemma="treat" pos="vvd" xml:id="A32480-001-a-0400">Treated</w>
     <w lemma="and" pos="cc" xml:id="A32480-001-a-0410">and</w>
     <w lemma="conclude" pos="vvn" xml:id="A32480-001-a-0420">Concluded</w>
     <w lemma="at" pos="acp" xml:id="A32480-001-a-0430">at</w>
     <hi xml:id="A32480-e130">
      <w lemma="Breda" pos="nn1" xml:id="A32480-001-a-0440">Breda</w>
      <pc xml:id="A32480-001-a-0450">,</pc>
     </hi>
     <w lemma="betwixt" pos="acp" xml:id="A32480-001-a-0460">betwixt</w>
     <w lemma="his" pos="po" xml:id="A32480-001-a-0470">His</w>
     <w lemma="majesty" pos="n1" xml:id="A32480-001-a-0480">Majesty</w>
     <w lemma="and" pos="cc" xml:id="A32480-001-a-0490">and</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-0500">the</w>
     <w lemma="king" pos="n1" xml:id="A32480-001-a-0510">King</w>
     <w lemma="of" pos="acp" xml:id="A32480-001-a-0520">of</w>
     <hi xml:id="A32480-e140">
      <w lemma="Denmark" pos="nn1" xml:id="A32480-001-a-0530">Denmark</w>
      <pc xml:id="A32480-001-a-0540">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A32480-001-a-0550">and</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-0560">the</w>
     <w lemma="ratification" pos="n2" xml:id="A32480-001-a-0570">Ratifications</w>
     <w lemma="thereof" pos="av" xml:id="A32480-001-a-0580">thereof</w>
     <w lemma="exchange" pos="vvn" xml:id="A32480-001-a-0590">exchanged</w>
     <pc xml:id="A32480-001-a-0600">,</pc>
     <w lemma="and" pos="cc" xml:id="A32480-001-a-0610">and</w>
     <w lemma="publication" pos="n1" xml:id="A32480-001-a-0620">Publication</w>
     <w lemma="thereof" pos="av" xml:id="A32480-001-a-0630">thereof</w>
     <w lemma="there" pos="av" xml:id="A32480-001-a-0640">there</w>
     <w lemma="make" pos="vvd" xml:id="A32480-001-a-0650">made</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-0660">the</w>
     <w lemma="fourteen" pos="ord" xml:id="A32480-001-a-0670">Fourteenth</w>
     <w lemma="day" pos="n1" xml:id="A32480-001-a-0680">day</w>
     <w lemma="of" pos="acp" xml:id="A32480-001-a-0690">of</w>
     <w lemma="this" pos="d" xml:id="A32480-001-a-0700">this</w>
     <w lemma="instant" pos="j" xml:id="A32480-001-a-0710">instant</w>
     <hi xml:id="A32480-e150">
      <w lemma="August" pos="nn1" xml:id="A32480-001-a-0720">August</w>
      <pc xml:id="A32480-001-a-0730">;</pc>
     </hi>
     <w lemma="in" pos="acp" xml:id="A32480-001-a-0740">In</w>
     <w lemma="conformity" pos="n1" xml:id="A32480-001-a-0750">conformity</w>
     <w lemma="thereunto" pos="av" xml:id="A32480-001-a-0760">thereunto</w>
     <w lemma="his" pos="po" xml:id="A32480-001-a-0770">His</w>
     <w lemma="majesty" pos="n1" xml:id="A32480-001-a-0780">Majesty</w>
     <w lemma="have" pos="vvz" xml:id="A32480-001-a-0790">hath</w>
     <w lemma="think" pos="vvn" xml:id="A32480-001-a-0800">thought</w>
     <w lemma="fit" pos="j" xml:id="A32480-001-a-0810">fit</w>
     <w lemma="hereby" pos="av" xml:id="A32480-001-a-0820">hereby</w>
     <w lemma="to" pos="prt" xml:id="A32480-001-a-0830">to</w>
     <w lemma="command" pos="vvi" xml:id="A32480-001-a-0840">Command</w>
     <w lemma="that" pos="cs" xml:id="A32480-001-a-0850">that</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-0860">the</w>
     <w lemma="same" pos="d" xml:id="A32480-001-a-0870">same</w>
     <w lemma="be" pos="vvb" xml:id="A32480-001-a-0880">be</w>
     <w lemma="publish" pos="vvn" xml:id="A32480-001-a-0890">Published</w>
     <w lemma="throughout" pos="acp" xml:id="A32480-001-a-0900">throughout</w>
     <w lemma="all" pos="d" xml:id="A32480-001-a-0910">all</w>
     <w lemma="his" pos="po" xml:id="A32480-001-a-0920">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A32480-001-a-0930">Majesties</w>
     <w lemma="dominion" pos="n2" xml:id="A32480-001-a-0940">Dominions</w>
     <pc unit="sentence" xml:id="A32480-001-a-0950">.</pc>
     <w lemma="and" pos="cc" xml:id="A32480-001-a-0960">And</w>
     <w lemma="his" pos="po" xml:id="A32480-001-a-0970">His</w>
     <w lemma="majesty" pos="n1" xml:id="A32480-001-a-0980">Majesty</w>
     <w lemma="do" pos="vvz" xml:id="A32480-001-a-0990">doth</w>
     <w lemma="declare" pos="vvi" xml:id="A32480-001-a-1000">declare</w>
     <pc xml:id="A32480-001-a-1010">,</pc>
     <w lemma="that" pos="cs" xml:id="A32480-001-a-1020">That</w>
     <w lemma="all" pos="d" xml:id="A32480-001-a-1030">all</w>
     <w lemma="ship" pos="n2" xml:id="A32480-001-a-1040">Ships</w>
     <w lemma="or" pos="cc" xml:id="A32480-001-a-1050">or</w>
     <w lemma="other" pos="d" xml:id="A32480-001-a-1060">other</w>
     <w lemma="movable" pos="j" reg="Movable" xml:id="A32480-001-a-1070">Moveable</w>
     <w lemma="good" pos="n2-j" xml:id="A32480-001-a-1080">Goods</w>
     <w lemma="whatsoever" pos="crq" xml:id="A32480-001-a-1090">whatsoever</w>
     <pc xml:id="A32480-001-a-1100">,</pc>
     <w lemma="which" pos="crq" xml:id="A32480-001-a-1110">which</w>
     <w lemma="shall" pos="vmb" xml:id="A32480-001-a-1120">shall</w>
     <w lemma="appear" pos="vvi" xml:id="A32480-001-a-1130">appear</w>
     <w lemma="to" pos="prt" xml:id="A32480-001-a-1140">to</w>
     <w lemma="be" pos="vvi" xml:id="A32480-001-a-1150">be</w>
     <w lemma="take" pos="vvn" xml:id="A32480-001-a-1160">taken</w>
     <w lemma="from" pos="acp" xml:id="A32480-001-a-1170">from</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-1180">the</w>
     <w lemma="subject" pos="n2" xml:id="A32480-001-a-1190">Subjects</w>
     <w lemma="of" pos="acp" xml:id="A32480-001-a-1200">of</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-1210">the</w>
     <w lemma="say" pos="j-vn" xml:id="A32480-001-a-1220">said</w>
     <w lemma="king" pos="n1" xml:id="A32480-001-a-1230">King</w>
     <w lemma="of" pos="acp" xml:id="A32480-001-a-1240">of</w>
     <hi xml:id="A32480-e160">
      <w lemma="Denmark" pos="nn1" xml:id="A32480-001-a-1250">Denmark</w>
      <pc xml:id="A32480-001-a-1260">,</pc>
     </hi>
     <w lemma="after" pos="acp" xml:id="A32480-001-a-1270">after</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-1280">the</w>
     <w lemma="four" pos="ord" xml:id="A32480-001-a-1290">Fourth</w>
     <w lemma="day" pos="n1" xml:id="A32480-001-a-1300">day</w>
     <w lemma="of" pos="acp" xml:id="A32480-001-a-1310">of</w>
     <w lemma="September" pos="nn1" rend="hi" xml:id="A32480-001-a-1320">September</w>
     <w lemma="next" pos="ord" xml:id="A32480-001-a-1330">next</w>
     <pc xml:id="A32480-001-a-1340">,</pc>
     <w lemma="in" pos="acp" xml:id="A32480-001-a-1350">in</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-1360">the</w>
     <w lemma="north" pos="n1" xml:id="A32480-001-a-1370">North</w>
     <w lemma="sea" pos="n2" xml:id="A32480-001-a-1380">Seas</w>
     <pc xml:id="A32480-001-a-1390">,</pc>
     <w lemma="as" pos="acp" xml:id="A32480-001-a-1400">as</w>
     <w lemma="also" pos="av" xml:id="A32480-001-a-1410">also</w>
     <w lemma="in" pos="acp" xml:id="A32480-001-a-1420">in</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-1430">the</w>
     <w lemma="baltic" pos="jnn" reg="Baltic" xml:id="A32480-001-a-1440">Baltick</w>
     <w lemma="and" pos="cc" xml:id="A32480-001-a-1450">and</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-1460">the</w>
     <w lemma="channel" pos="n1" xml:id="A32480-001-a-1470">Channel</w>
     <pc xml:id="A32480-001-a-1480">;</pc>
     <w lemma="after" pos="acp" xml:id="A32480-001-a-1490">After</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-1500">the</w>
     <w lemma="two" pos="crd" xml:id="A32480-001-a-1510">Two</w>
     <w lemma="and" pos="cc" xml:id="A32480-001-a-1520">and</w>
     <w lemma="twenty" pos="ord" xml:id="A32480-001-a-1530">twentieth</w>
     <w lemma="day" pos="n1" xml:id="A32480-001-a-1540">day</w>
     <w lemma="of" pos="acp" xml:id="A32480-001-a-1550">of</w>
     <w lemma="September" pos="nn1" rend="hi" xml:id="A32480-001-a-1560">September</w>
     <w lemma="next" pos="ord" xml:id="A32480-001-a-1570">next</w>
     <pc xml:id="A32480-001-a-1580">,</pc>
     <w lemma="from" pos="acp" xml:id="A32480-001-a-1590">from</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-1600">the</w>
     <w lemma="mouth" pos="n1" xml:id="A32480-001-a-1610">mouth</w>
     <w lemma="of" pos="acp" xml:id="A32480-001-a-1620">of</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-1630">the</w>
     <w lemma="channel" pos="n1" xml:id="A32480-001-a-1640">Channel</w>
     <w lemma="to" pos="acp" xml:id="A32480-001-a-1650">to</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-1660">the</w>
     <hi xml:id="A32480-e190">
      <w lemma="cape" pos="n1" xml:id="A32480-001-a-1670">Cape</w>
      <w lemma="saint" pos="n1" xml:id="A32480-001-a-1680">Saint</w>
      <w lemma="Vincent" pos="nn1" xml:id="A32480-001-a-1690">Vincent</w>
      <pc xml:id="A32480-001-a-1700">;</pc>
     </hi>
     <w lemma="after" pos="acp" xml:id="A32480-001-a-1710">After</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-1720">the</w>
     <w lemma="three" pos="crd" xml:id="A32480-001-a-1730">Three</w>
     <w lemma="and" pos="cc" xml:id="A32480-001-a-1740">and</w>
     <w lemma="twenty" pos="ord" xml:id="A32480-001-a-1750">twentieth</w>
     <w lemma="day" pos="n1" xml:id="A32480-001-a-1760">day</w>
     <w lemma="of" pos="acp" xml:id="A32480-001-a-1770">of</w>
     <w lemma="October" pos="nn1" rend="hi" xml:id="A32480-001-a-1780">October</w>
     <w lemma="next" pos="ord" xml:id="A32480-001-a-1790">next</w>
     <w lemma="ensue" pos="vvg" xml:id="A32480-001-a-1800">ensuing</w>
     <pc xml:id="A32480-001-a-1810">,</pc>
     <w lemma="on" pos="acp" xml:id="A32480-001-a-1820">on</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-1830">the</w>
     <w lemma="other" pos="d" xml:id="A32480-001-a-1840">other</w>
     <w lemma="side" pos="n1" xml:id="A32480-001-a-1850">side</w>
     <w lemma="of" pos="acp" xml:id="A32480-001-a-1860">of</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-1870">the</w>
     <w lemma="say" pos="j-vn" xml:id="A32480-001-a-1880">said</w>
     <w lemma="cape" pos="n1" xml:id="A32480-001-a-1890">Cape</w>
     <w lemma="to" pos="acp" xml:id="A32480-001-a-1900">to</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-1910">the</w>
     <w lemma="equinoctial" pos="j" xml:id="A32480-001-a-1920">Equinoctial</w>
     <w lemma="line" pos="n1" xml:id="A32480-001-a-1930">Line</w>
     <pc xml:id="A32480-001-a-1940">,</pc>
     <w lemma="as" pos="acp" xml:id="A32480-001-a-1950">as</w>
     <w lemma="well" pos="av" xml:id="A32480-001-a-1960">well</w>
     <w lemma="in" pos="acp" xml:id="A32480-001-a-1970">in</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-1980">the</w>
     <w lemma="ocean" pos="n1" xml:id="A32480-001-a-1990">Ocean</w>
     <w lemma="and" pos="cc" xml:id="A32480-001-a-2000">and</w>
     <w lemma="mediterranean" pos="jnn" xml:id="A32480-001-a-2010">Mediterranean</w>
     <w lemma="sea" pos="n1" xml:id="A32480-001-a-2020">Sea</w>
     <pc xml:id="A32480-001-a-2030">,</pc>
     <w lemma="as" pos="acp" xml:id="A32480-001-a-2040">as</w>
     <w lemma="elsewhere" pos="av" xml:id="A32480-001-a-2050">elsewhere</w>
     <pc xml:id="A32480-001-a-2060">:</pc>
     <w lemma="and" pos="cc" xml:id="A32480-001-a-2070">And</w>
     <w lemma="last" pos="ord" xml:id="A32480-001-a-2080">lastly</w>
     <pc xml:id="A32480-001-a-2090">,</pc>
     <w lemma="after" pos="acp" xml:id="A32480-001-a-2100">after</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-2110">the</w>
     <w lemma="fourteen" pos="ord" xml:id="A32480-001-a-2120">Fourteenth</w>
     <w lemma="day" pos="n1" xml:id="A32480-001-a-2130">day</w>
     <w lemma="of" pos="acp" xml:id="A32480-001-a-2140">of</w>
     <hi xml:id="A32480-e210">
      <w lemma="April" pos="nn1" xml:id="A32480-001-a-2150">April</w>
      <pc xml:id="A32480-001-a-2160">,</pc>
     </hi>
     <w lemma="1668." pos="crd" xml:id="A32480-001-a-2170">1668.</w>
     <w lemma="on" pos="acp" xml:id="A32480-001-a-2180">on</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-2190">the</w>
     <w lemma="other" pos="d" xml:id="A32480-001-a-2200">other</w>
     <w lemma="side" pos="n1" xml:id="A32480-001-a-2210">side</w>
     <w lemma="of" pos="acp" xml:id="A32480-001-a-2220">of</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-2230">the</w>
     <w lemma="aforesaid" pos="j" xml:id="A32480-001-a-2240">aforesaid</w>
     <w lemma="line" pos="n1" xml:id="A32480-001-a-2250">Line</w>
     <w lemma="throughout" pos="acp" xml:id="A32480-001-a-2260">throughout</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-2270">the</w>
     <w lemma="whole" pos="j" xml:id="A32480-001-a-2280">whole</w>
     <w lemma="world" pos="n1" xml:id="A32480-001-a-2290">World</w>
     <pc xml:id="A32480-001-a-2300">,</pc>
     <w lemma="without" pos="acp" xml:id="A32480-001-a-2310">without</w>
     <w lemma="any" pos="d" xml:id="A32480-001-a-2320">any</w>
     <w lemma="exception" pos="n1" xml:id="A32480-001-a-2330">exception</w>
     <w lemma="or" pos="cc" xml:id="A32480-001-a-2340">or</w>
     <w lemma="distinction" pos="n1" xml:id="A32480-001-a-2350">distinction</w>
     <w lemma="of" pos="acp" xml:id="A32480-001-a-2360">of</w>
     <w lemma="time" pos="n1" xml:id="A32480-001-a-2370">Time</w>
     <w lemma="or" pos="cc" xml:id="A32480-001-a-2380">or</w>
     <w lemma="place" pos="n1" xml:id="A32480-001-a-2390">Place</w>
     <pc xml:id="A32480-001-a-2400">,</pc>
     <w lemma="or" pos="cc" xml:id="A32480-001-a-2410">or</w>
     <w lemma="without" pos="acp" xml:id="A32480-001-a-2420">without</w>
     <w lemma="any" pos="d" xml:id="A32480-001-a-2430">any</w>
     <w lemma="form" pos="n1" xml:id="A32480-001-a-2440">Form</w>
     <w lemma="of" pos="acp" xml:id="A32480-001-a-2450">of</w>
     <w lemma="process" pos="n1" xml:id="A32480-001-a-2460">Process</w>
     <pc xml:id="A32480-001-a-2470">,</pc>
     <w lemma="shall" pos="vmb" xml:id="A32480-001-a-2480">shall</w>
     <w lemma="immediate" pos="av-j" xml:id="A32480-001-a-2490">immediately</w>
     <pc xml:id="A32480-001-a-2500">,</pc>
     <w lemma="and" pos="cc" xml:id="A32480-001-a-2510">and</w>
     <w lemma="without" pos="acp" xml:id="A32480-001-a-2520">without</w>
     <w lemma="damage" pos="n1" xml:id="A32480-001-a-2530">Damage</w>
     <pc xml:id="A32480-001-a-2540">,</pc>
     <w lemma="be" pos="vvb" xml:id="A32480-001-a-2550">be</w>
     <w lemma="restore" pos="vvn" xml:id="A32480-001-a-2560">restored</w>
     <w lemma="to" pos="acp" xml:id="A32480-001-a-2570">to</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-2580">the</w>
     <w lemma="owner" pos="n2" xml:id="A32480-001-a-2590">Owners</w>
     <pc xml:id="A32480-001-a-2600">,</pc>
     <w lemma="according" pos="j" xml:id="A32480-001-a-2610">according</w>
     <w lemma="to" pos="acp" xml:id="A32480-001-a-2620">to</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-2630">the</w>
     <w lemma="say" pos="j-vn" xml:id="A32480-001-a-2640">said</w>
     <w lemma="treaty" pos="n1" xml:id="A32480-001-a-2650">Treaty</w>
     <pc unit="sentence" xml:id="A32480-001-a-2660">.</pc>
     <w lemma="and" pos="cc" xml:id="A32480-001-a-2670">And</w>
     <w lemma="hereof" pos="av" xml:id="A32480-001-a-2680">hereof</w>
     <w lemma="his" pos="po" xml:id="A32480-001-a-2690">His</w>
     <w lemma="majesty" pos="n1" xml:id="A32480-001-a-2700">Majesty</w>
     <w lemma="will" pos="n2" reg="Wills" xml:id="A32480-001-a-2710">Willeth</w>
     <w lemma="and" pos="cc" xml:id="A32480-001-a-2720">and</w>
     <w lemma="command" pos="vvz" xml:id="A32480-001-a-2730">Commandeth</w>
     <w lemma="all" pos="d" xml:id="A32480-001-a-2740">all</w>
     <w lemma="his" pos="po" xml:id="A32480-001-a-2750">His</w>
     <w lemma="subject" pos="n2" xml:id="A32480-001-a-2760">Subjects</w>
     <w lemma="to" pos="prt" xml:id="A32480-001-a-2770">to</w>
     <w lemma="take" pos="vvi" xml:id="A32480-001-a-2780">take</w>
     <w lemma="notice" pos="n1" xml:id="A32480-001-a-2790">notice</w>
     <pc xml:id="A32480-001-a-2800">,</pc>
     <w lemma="and" pos="cc" xml:id="A32480-001-a-2810">and</w>
     <w lemma="to" pos="prt" xml:id="A32480-001-a-2820">to</w>
     <w lemma="conform" pos="vvi" xml:id="A32480-001-a-2830">conform</w>
     <w lemma="themselves" pos="pr" xml:id="A32480-001-a-2840">themselves</w>
     <w lemma="thereunto" pos="av" xml:id="A32480-001-a-2850">thereunto</w>
     <w lemma="according" pos="av-j" xml:id="A32480-001-a-2860">accordingly</w>
     <pc unit="sentence" xml:id="A32480-001-a-2870">.</pc>
    </p>
    <closer xml:id="A32480-e220">
     <dateline xml:id="A32480-e230">
      <w lemma="give" pos="vvn" xml:id="A32480-001-a-2880">Given</w>
      <w lemma="at" pos="acp" xml:id="A32480-001-a-2890">at</w>
      <w lemma="our" pos="po" xml:id="A32480-001-a-2900">Our</w>
      <w lemma="court" pos="n1" xml:id="A32480-001-a-2910">Court</w>
      <w lemma="at" pos="acp" xml:id="A32480-001-a-2920">at</w>
      <hi xml:id="A32480-e240">
       <w lemma="Whitehall" pos="nn1" xml:id="A32480-001-a-2930">Whitehall</w>
       <pc xml:id="A32480-001-a-2940">,</pc>
      </hi>
      <date xml:id="A32480-e250">
       <w lemma="the" pos="d" xml:id="A32480-001-a-2950">the</w>
       <w lemma="four" pos="crd" xml:id="A32480-001-a-2960">Four</w>
       <w lemma="and" pos="cc" xml:id="A32480-001-a-2970">and</w>
       <w lemma="twenty" pos="ord" xml:id="A32480-001-a-2980">twentieth</w>
       <w lemma="day" pos="n1" xml:id="A32480-001-a-2990">day</w>
       <w lemma="of" pos="acp" xml:id="A32480-001-a-3000">of</w>
       <hi xml:id="A32480-e260">
        <w lemma="August" pos="nn1" xml:id="A32480-001-a-3010">August</w>
        <pc xml:id="A32480-001-a-3020">,</pc>
       </hi>
       <w lemma="1667." pos="crd" xml:id="A32480-001-a-3030">1667.</w>
       <pc unit="sentence" xml:id="A32480-001-a-3040"/>
       <w lemma="in" pos="acp" xml:id="A32480-001-a-3050">In</w>
       <w lemma="the" pos="d" xml:id="A32480-001-a-3060">the</w>
       <w lemma="nineteen" pos="ord" xml:id="A32480-001-a-3070">Nineteenth</w>
       <w lemma="year" pos="n1" xml:id="A32480-001-a-3080">Year</w>
       <w lemma="of" pos="acp" xml:id="A32480-001-a-3090">of</w>
       <w lemma="our" pos="po" xml:id="A32480-001-a-3100">Our</w>
       <w lemma="reign" pos="n1" xml:id="A32480-001-a-3110">Reign</w>
       <pc unit="sentence" xml:id="A32480-001-a-3120">.</pc>
      </date>
     </dateline>
     <lb xml:id="A32480-e270"/>
     <w lemma="GOD" pos="nn1" xml:id="A32480-001-a-3130">GOD</w>
     <w lemma="save" pos="acp" xml:id="A32480-001-a-3140">SAVE</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-3150">THE</w>
     <w lemma="king" pos="n1" xml:id="A32480-001-a-3160">KING</w>
     <pc unit="sentence" xml:id="A32480-001-a-3170">.</pc>
    </closer>
   </div>
  </body>
  <back xml:id="A32480-e280">
   <div type="colophon" xml:id="A32480-e290">
    <p xml:id="A32480-e300">
     <w lemma="in" pos="acp" xml:id="A32480-001-a-3180">In</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-3190">the</w>
     <hi xml:id="A32480-e310">
      <w lemma="Savoy" pos="nn1" xml:id="A32480-001-a-3200">SAVOY</w>
      <pc xml:id="A32480-001-a-3210">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A32480-001-a-3220">Printed</w>
     <w lemma="by" pos="acp" xml:id="A32480-001-a-3230">by</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-3240">the</w>
     <w lemma="assign" pos="vvz" xml:id="A32480-001-a-3250">Assigns</w>
     <w lemma="of" pos="acp" xml:id="A32480-001-a-3260">of</w>
     <hi xml:id="A32480-e320">
      <w lemma="John" pos="nn1" xml:id="A32480-001-a-3270">John</w>
      <w lemma="bill" pos="n1" xml:id="A32480-001-a-3280">Bill</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="A32480-001-a-3290">and</w>
     <hi xml:id="A32480-e330">
      <w lemma="Christopher" pos="nn1" xml:id="A32480-001-a-3300">Christopher</w>
      <w lemma="Barker" pos="nn1" xml:id="A32480-001-a-3310">Barker</w>
      <pc xml:id="A32480-001-a-3320">,</pc>
     </hi>
     <w lemma="printer" pos="n2" xml:id="A32480-001-a-3330">Printers</w>
     <w lemma="to" pos="acp" xml:id="A32480-001-a-3340">to</w>
     <w lemma="the" pos="d" xml:id="A32480-001-a-3350">the</w>
     <w lemma="king" pos="n2" xml:id="A32480-001-a-3360">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A32480-001-a-3370">most</w>
     <w lemma="excellent" pos="j" xml:id="A32480-001-a-3380">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A32480-001-a-3390">Majesty</w>
     <pc unit="sentence" xml:id="A32480-001-a-3400">.</pc>
     <w lemma="1667." pos="crd" xml:id="A32480-001-a-3410">1667.</w>
     <pc unit="sentence" xml:id="A32480-001-a-3420"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
