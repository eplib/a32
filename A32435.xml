<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A32435">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>By the King, a proclamation for dissolving this present Parliament and declaring the speedy calling of a new one</title>
    <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A32435 of text R36160 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing C3334). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A32435</idno>
    <idno type="STC">Wing C3334</idno>
    <idno type="STC">ESTC R36160</idno>
    <idno type="EEBO-CITATION">15613405</idno>
    <idno type="OCLC">ocm 15613405</idno>
    <idno type="VID">104131</idno>
    <idno type="PROQUESTGOID">2240888485</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A32435)</note>
    <note>Transcribed from: (Early English Books Online ; image set 104131)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1588:57)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>By the King, a proclamation for dissolving this present Parliament and declaring the speedy calling of a new one</title>
      <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
      <author>Charles II, King of England, 1630-1685.</author>
     </titleStmt>
     <extent>1 broadside.</extent>
     <publicationStmt>
      <publisher>Printed by the assigns of John Bill, Thomas Newcomb, and Henry Hills ...,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1680 [i.e. 1681]</date>
     </publicationStmt>
     <notesStmt>
      <note>"Given at our court at Whitehall the eighteenth day of January in the two and thirtieth year of our reign."</note>
      <note>Reproduction of original in the Huntington Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Great Britain -- History -- Charles II, 1660-1685.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>By the King. A proclamation for dissolving this present Parliament, and declaring the speedy calling of a new one.</ep:title>
    <ep:author>England and Wales. Sovereign </ep:author>
    <ep:publicationYear>1681</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>283</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-03</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-07</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-08</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-08</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-09</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A32435-t">
  <body xml:id="A32435-e0">
   <div type="royal_proclamation" xml:id="A32435-e10">
    <pb facs="tcp:104131:1" rend="simple:additions" xml:id="A32435-001-a"/>
    <head xml:id="A32435-e20">
     <figure xml:id="A32435-e30">
      <p xml:id="A32435-e40">
       <w lemma="C2" pos="crd" xml:id="A32435-001-a-0010">C2</w>
       <w lemma="r" pos="sy" xml:id="A32435-001-a-0020">R</w>
      </p>
      <p xml:id="A32435-e50">
       <w lemma="n/a" pos="ffr" xml:id="A32435-001-a-0030">DIEV</w>
       <w lemma="n/a" pos="ffr" xml:id="A32435-001-a-0040">ET</w>
       <w lemma="n/a" pos="ffr" xml:id="A32435-001-a-0050">MON</w>
       <w lemma="n/a" pos="ffr" xml:id="A32435-001-a-0060">DROIT</w>
      </p>
      <p xml:id="A32435-e60">
       <w lemma="n/a" pos="ffr" xml:id="A32435-001-a-0070">HONI</w>
       <w lemma="n/a" pos="ffr" xml:id="A32435-001-a-0080">SOIT</w>
       <w lemma="n/a" pos="ffr" xml:id="A32435-001-a-0090">QVI</w>
       <w lemma="n/a" pos="ffr" xml:id="A32435-001-a-0100">MAL</w>
       <w lemma="n/a" pos="ffr" xml:id="A32435-001-a-0110">Y</w>
       <w lemma="n/a" pos="ffr" xml:id="A32435-001-a-0120">PENSE</w>
      </p>
      <figDesc xml:id="A32435-e70">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <head xml:id="A32435-e80">
     <w lemma="by" pos="acp" xml:id="A32435-001-a-0130">By</w>
     <w lemma="the" pos="d" xml:id="A32435-001-a-0140">the</w>
     <w lemma="King" pos="n1" xml:id="A32435-001-a-0150">King</w>
     <pc unit="sentence" xml:id="A32435-001-a-0151">.</pc>
    </head>
    <head type="sub" xml:id="A32435-e90">
     <w lemma="a" pos="d" xml:id="A32435-001-a-0170">A</w>
     <w lemma="proclamation" pos="n1" xml:id="A32435-001-a-0180">PROCLAMATION</w>
     <hi xml:id="A32435-e100">
      <w lemma="for" pos="acp" xml:id="A32435-001-a-0190">For</w>
      <w lemma="dissolve" pos="vvg" xml:id="A32435-001-a-0200">dissolving</w>
      <w lemma="this" pos="d" xml:id="A32435-001-a-0210">this</w>
      <w lemma="present" pos="j" xml:id="A32435-001-a-0220">present</w>
     </hi>
     <w lemma="parliament" pos="n1" xml:id="A32435-001-a-0230">PARLIAMENT</w>
     <pc xml:id="A32435-001-a-0240">,</pc>
     <w lemma="and" pos="cc" xml:id="A32435-001-a-0250">And</w>
     <w lemma="declare" pos="vvg" xml:id="A32435-001-a-0260">Declaring</w>
     <w lemma="the" pos="d" xml:id="A32435-001-a-0270">the</w>
     <w lemma="speedy" pos="j" xml:id="A32435-001-a-0280">speedy</w>
     <w lemma="call" pos="vvg" xml:id="A32435-001-a-0290">Calling</w>
     <w lemma="of" pos="acp" xml:id="A32435-001-a-0300">of</w>
     <w lemma="a" pos="d" xml:id="A32435-001-a-0310">a</w>
     <w lemma="new" pos="j" xml:id="A32435-001-a-0320">New</w>
     <w lemma="One." pos="nn1" xml:id="A32435-001-a-0330">One.</w>
     <pc unit="sentence" xml:id="A32435-001-a-0340"/>
    </head>
    <opener xml:id="A32435-e110">
     <signed xml:id="A32435-e120">
      <w lemma="CHARLES" pos="nn1" xml:id="A32435-001-a-0350">CHARLES</w>
      <w lemma="r." pos="ab" xml:id="A32435-001-a-0360">R.</w>
      <pc unit="sentence" xml:id="A32435-001-a-0370"/>
     </signed>
    </opener>
    <p xml:id="A32435-e130">
     <w lemma="whereas" pos="cs" rend="decorinit" xml:id="A32435-001-a-0380">WHereas</w>
     <w lemma="this" pos="d" xml:id="A32435-001-a-0390">this</w>
     <w lemma="present" pos="j" xml:id="A32435-001-a-0400">present</w>
     <w lemma="parliament" pos="n1" xml:id="A32435-001-a-0410">Parliament</w>
     <w lemma="which" pos="crq" xml:id="A32435-001-a-0420">which</w>
     <w lemma="be" pos="vvd" xml:id="A32435-001-a-0430">was</w>
     <w lemma="summon" pos="vvn" xml:id="A32435-001-a-0440">Summoned</w>
     <w lemma="to" pos="prt" xml:id="A32435-001-a-0450">to</w>
     <w lemma="begin" pos="vvi" xml:id="A32435-001-a-0460">Begin</w>
     <w lemma="at" pos="acp" xml:id="A32435-001-a-0470">at</w>
     <w lemma="Westminster" pos="nn1" rend="hi" xml:id="A32435-001-a-0480">Westminster</w>
     <w lemma="the" pos="d" xml:id="A32435-001-a-0490">the</w>
     <w lemma="seventeénth" pos="n1" xml:id="A32435-001-a-0500">Seventeénth</w>
     <w lemma="day" pos="n1" xml:id="A32435-001-a-0510">day</w>
     <w lemma="of" pos="acp" xml:id="A32435-001-a-0520">of</w>
     <hi xml:id="A32435-e150">
      <w lemma="October" pos="nn1" xml:id="A32435-001-a-0530">October</w>
      <pc xml:id="A32435-001-a-0540">,</pc>
     </hi>
     <w lemma="1679" pos="crd" xml:id="A32435-001-a-0550">1679</w>
     <pc xml:id="A32435-001-a-0560">,</pc>
     <w lemma="and" pos="cc" xml:id="A32435-001-a-0570">and</w>
     <w lemma="by" pos="acp" xml:id="A32435-001-a-0580">by</w>
     <w lemma="several" pos="j" xml:id="A32435-001-a-0590">several</w>
     <w lemma="prorogation" pos="n2" xml:id="A32435-001-a-0600">Prorogations</w>
     <w lemma="continue" pos="vvn" xml:id="A32435-001-a-0610">continued</w>
     <w lemma="to" pos="acp" xml:id="A32435-001-a-0620">to</w>
     <w lemma="the" pos="d" xml:id="A32435-001-a-0630">the</w>
     <w lemma="one" pos="crd" xml:id="A32435-001-a-0640">One</w>
     <w lemma="and" pos="cc" xml:id="A32435-001-a-0650">and</w>
     <w lemma="twenty" pos="ord" xml:id="A32435-001-a-0660">twentieth</w>
     <w lemma="day" pos="n1" xml:id="A32435-001-a-0670">day</w>
     <w lemma="of" pos="acp" xml:id="A32435-001-a-0680">of</w>
     <w lemma="October" pos="nn1" rend="hi" xml:id="A32435-001-a-0690">October</w>
     <w lemma="last" pos="ord" xml:id="A32435-001-a-0700">last</w>
     <pc xml:id="A32435-001-a-0710">,</pc>
     <w lemma="be" pos="vvd" xml:id="A32435-001-a-0720">was</w>
     <w lemma="late" pos="av-j" xml:id="A32435-001-a-0730">lately</w>
     <w lemma="prorogue" pos="vvn" xml:id="A32435-001-a-0740">Prorogued</w>
     <w lemma="until" pos="acp" xml:id="A32435-001-a-0750">until</w>
     <w lemma="Thursday" pos="nn1" xml:id="A32435-001-a-0760">Thursday</w>
     <w lemma="the" pos="d" xml:id="A32435-001-a-0770">the</w>
     <w lemma="twenty" pos="ord" xml:id="A32435-001-a-0780">Twentieth</w>
     <w lemma="day" pos="n1" xml:id="A32435-001-a-0790">day</w>
     <w lemma="of" pos="acp" xml:id="A32435-001-a-0800">of</w>
     <w lemma="this" pos="d" xml:id="A32435-001-a-0810">this</w>
     <w lemma="instant" pos="j" xml:id="A32435-001-a-0820">instant</w>
     <hi xml:id="A32435-e170">
      <w lemma="January" pos="nn1" xml:id="A32435-001-a-0830">January</w>
      <pc xml:id="A32435-001-a-0840">,</pc>
     </hi>
     <w lemma="the" pos="d" xml:id="A32435-001-a-0850">The</w>
     <w lemma="king" pos="n2" xml:id="A32435-001-a-0860">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A32435-001-a-0870">most</w>
     <w lemma="excellent" pos="j" xml:id="A32435-001-a-0880">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A32435-001-a-0890">Majesty</w>
     <w lemma="be" pos="vvg" xml:id="A32435-001-a-0900">being</w>
     <w lemma="resolve" pos="vvn" xml:id="A32435-001-a-0910">resolved</w>
     <w lemma="to" pos="prt" xml:id="A32435-001-a-0920">to</w>
     <w lemma="meét" pos="vvi" xml:id="A32435-001-a-0930">Meét</w>
     <w lemma="his" pos="po" xml:id="A32435-001-a-0940">His</w>
     <w lemma="people" pos="n1" xml:id="A32435-001-a-0950">People</w>
     <pc xml:id="A32435-001-a-0960">,</pc>
     <w lemma="and" pos="cc" xml:id="A32435-001-a-0970">and</w>
     <w lemma="to" pos="prt" xml:id="A32435-001-a-0980">to</w>
     <w lemma="have" pos="vvi" xml:id="A32435-001-a-0990">have</w>
     <w lemma="there" pos="av" xml:id="A32435-001-a-1000">there</w>
     <w lemma="advice" pos="n1" xml:id="A32435-001-a-1010">Advice</w>
     <w lemma="in" pos="acp" xml:id="A32435-001-a-1020">in</w>
     <w lemma="frequent" pos="j" xml:id="A32435-001-a-1030">frequent</w>
     <w lemma="parliament" pos="n2" xml:id="A32435-001-a-1040">Parliaments</w>
     <pc xml:id="A32435-001-a-1050">,</pc>
     <w lemma="have" pos="vvz" xml:id="A32435-001-a-1060">hath</w>
     <w lemma="think" pos="vvn" xml:id="A32435-001-a-1070">thought</w>
     <w lemma="fit" pos="j" xml:id="A32435-001-a-1080">fit</w>
     <w lemma="to" pos="prt" xml:id="A32435-001-a-1090">to</w>
     <w lemma="dissolve" pos="vvi" xml:id="A32435-001-a-1100">Dissolve</w>
     <w lemma="this" pos="d" xml:id="A32435-001-a-1110">this</w>
     <w lemma="present" pos="j" xml:id="A32435-001-a-1120">present</w>
     <w lemma="parliament" pos="n1" xml:id="A32435-001-a-1130">Parliament</w>
     <pc xml:id="A32435-001-a-1140">,</pc>
     <w lemma="and" pos="cc" xml:id="A32435-001-a-1150">And</w>
     <w lemma="do" pos="vvz" xml:id="A32435-001-a-1160">doth</w>
     <w lemma="by" pos="acp" xml:id="A32435-001-a-1170">by</w>
     <w lemma="this" pos="d" xml:id="A32435-001-a-1180">this</w>
     <w lemma="his" pos="po" xml:id="A32435-001-a-1190">His</w>
     <w lemma="royal" pos="j" xml:id="A32435-001-a-1200">Royal</w>
     <w lemma="proclamation" pos="n1" xml:id="A32435-001-a-1210">Proclamation</w>
     <w lemma="dissolve" pos="vvb" xml:id="A32435-001-a-1220">Dissolve</w>
     <w lemma="the" pos="d" xml:id="A32435-001-a-1230">the</w>
     <w lemma="same" pos="d" xml:id="A32435-001-a-1240">same</w>
     <w lemma="according" pos="av-j" xml:id="A32435-001-a-1250">accordingly</w>
     <pc unit="sentence" xml:id="A32435-001-a-1260">.</pc>
     <w lemma="and" pos="cc" xml:id="A32435-001-a-1270">And</w>
     <w lemma="the" pos="d" xml:id="A32435-001-a-1280">the</w>
     <w lemma="lord" pos="n2" xml:id="A32435-001-a-1290">Lords</w>
     <w lemma="spiritual" pos="j" xml:id="A32435-001-a-1300">Spiritual</w>
     <w lemma="and" pos="cc" xml:id="A32435-001-a-1310">and</w>
     <w lemma="temporal" pos="j" xml:id="A32435-001-a-1320">Temporal</w>
     <pc xml:id="A32435-001-a-1330">,</pc>
     <w lemma="and" pos="cc" xml:id="A32435-001-a-1340">and</w>
     <w lemma="the" pos="d" xml:id="A32435-001-a-1350">the</w>
     <w lemma="knight" pos="n2" xml:id="A32435-001-a-1360">Knights</w>
     <pc xml:id="A32435-001-a-1370">,</pc>
     <w lemma="citizen" pos="n2" xml:id="A32435-001-a-1380">Citizens</w>
     <w lemma="and" pos="cc" xml:id="A32435-001-a-1390">and</w>
     <w lemma="burgess" pos="n2" xml:id="A32435-001-a-1400">Burgesses</w>
     <w lemma="of" pos="acp" xml:id="A32435-001-a-1410">of</w>
     <w lemma="this" pos="d" xml:id="A32435-001-a-1420">this</w>
     <w lemma="present" pos="j" xml:id="A32435-001-a-1430">present</w>
     <w lemma="parliament" pos="n1" xml:id="A32435-001-a-1440">Parliament</w>
     <w lemma="be" pos="vvb" xml:id="A32435-001-a-1450">are</w>
     <w lemma="hereby" pos="av" xml:id="A32435-001-a-1460">hereby</w>
     <w lemma="discharge" pos="vvn" xml:id="A32435-001-a-1470">Discharged</w>
     <w lemma="from" pos="acp" xml:id="A32435-001-a-1480">from</w>
     <w lemma="their" pos="po" xml:id="A32435-001-a-1490">their</w>
     <w lemma="meét" pos="n1-vg" xml:id="A32435-001-a-1500">Meéting</w>
     <w lemma="on" pos="acp" xml:id="A32435-001-a-1510">on</w>
     <w lemma="the" pos="d" xml:id="A32435-001-a-1520">the</w>
     <w lemma="say" pos="vvd" xml:id="A32435-001-a-1530">said</w>
     <w lemma="twenty" pos="ord" xml:id="A32435-001-a-1540">Twentieth</w>
     <w lemma="of" pos="acp" xml:id="A32435-001-a-1550">of</w>
     <w lemma="this" pos="d" xml:id="A32435-001-a-1560">this</w>
     <w lemma="instant" pos="j" xml:id="A32435-001-a-1570">Instant</w>
     <hi xml:id="A32435-e180">
      <w lemma="January" pos="nn1" xml:id="A32435-001-a-1580">January</w>
      <pc unit="sentence" xml:id="A32435-001-a-1590">.</pc>
     </hi>
    </p>
    <p xml:id="A32435-e190">
     <w lemma="and" pos="cc" xml:id="A32435-001-a-1600">And</w>
     <w lemma="his" pos="po" xml:id="A32435-001-a-1610">His</w>
     <w lemma="majesty" pos="n1" xml:id="A32435-001-a-1620">Majesty</w>
     <w lemma="be" pos="vvz" xml:id="A32435-001-a-1630">is</w>
     <w lemma="gracious" pos="av-j" xml:id="A32435-001-a-1640">Graciously</w>
     <w lemma="please" pos="vvn" xml:id="A32435-001-a-1650">pleased</w>
     <w lemma="to" pos="prt" xml:id="A32435-001-a-1660">to</w>
     <w lemma="make" pos="vvi" xml:id="A32435-001-a-1670">make</w>
     <w lemma="know" pos="vvn" xml:id="A32435-001-a-1680">known</w>
     <w lemma="to" pos="acp" xml:id="A32435-001-a-1690">to</w>
     <w lemma="all" pos="d" xml:id="A32435-001-a-1700">all</w>
     <w lemma="his" pos="po" xml:id="A32435-001-a-1710">His</w>
     <w lemma="love" pos="j-vg" xml:id="A32435-001-a-1720">Loving</w>
     <w lemma="subject" pos="n2" xml:id="A32435-001-a-1730">Subjects</w>
     <pc xml:id="A32435-001-a-1740">,</pc>
     <w lemma="that" pos="cs" xml:id="A32435-001-a-1750">that</w>
     <w lemma="he" pos="pns" xml:id="A32435-001-a-1760">He</w>
     <w lemma="have" pos="vvz" xml:id="A32435-001-a-1770">hath</w>
     <w lemma="give" pos="vvn" xml:id="A32435-001-a-1780">given</w>
     <w lemma="direction" pos="n2" xml:id="A32435-001-a-1790">directions</w>
     <w lemma="to" pos="acp" xml:id="A32435-001-a-1800">to</w>
     <w lemma="the" pos="d" xml:id="A32435-001-a-1810">the</w>
     <w lemma="lord" pos="n1" xml:id="A32435-001-a-1820">Lord</w>
     <w lemma="chancellor" pos="n1" xml:id="A32435-001-a-1830">Chancellor</w>
     <w lemma="of" pos="acp" xml:id="A32435-001-a-1840">of</w>
     <hi xml:id="A32435-e200">
      <w lemma="England" pos="nn1" xml:id="A32435-001-a-1850">England</w>
      <pc xml:id="A32435-001-a-1860">,</pc>
     </hi>
     <w lemma="for" pos="acp" xml:id="A32435-001-a-1870">for</w>
     <w lemma="the" pos="d" xml:id="A32435-001-a-1880">the</w>
     <w lemma="issue" pos="vvg" xml:id="A32435-001-a-1890">issuing</w>
     <w lemma="out" pos="av" xml:id="A32435-001-a-1900">out</w>
     <w lemma="of" pos="acp" xml:id="A32435-001-a-1910">of</w>
     <w lemma="writ" pos="n2" xml:id="A32435-001-a-1920">Writs</w>
     <w lemma="in" pos="acp" xml:id="A32435-001-a-1930">in</w>
     <w lemma="due" pos="j" xml:id="A32435-001-a-1940">due</w>
     <w lemma="form" pos="n1" xml:id="A32435-001-a-1950">Form</w>
     <w lemma="of" pos="acp" xml:id="A32435-001-a-1960">of</w>
     <w lemma="law" pos="n1" xml:id="A32435-001-a-1970">Law</w>
     <pc xml:id="A32435-001-a-1980">,</pc>
     <w lemma="for" pos="acp" xml:id="A32435-001-a-1990">for</w>
     <w lemma="the" pos="d" xml:id="A32435-001-a-2000">the</w>
     <w lemma="call" pos="vvg" xml:id="A32435-001-a-2010">Calling</w>
     <w lemma="of" pos="acp" xml:id="A32435-001-a-2020">of</w>
     <w lemma="a" pos="d" xml:id="A32435-001-a-2030">a</w>
     <w lemma="new" pos="j" xml:id="A32435-001-a-2040">New</w>
     <w lemma="parliament" pos="n1" xml:id="A32435-001-a-2050">Parliament</w>
     <pc xml:id="A32435-001-a-2060">,</pc>
     <w lemma="which" pos="crq" xml:id="A32435-001-a-2070">which</w>
     <w lemma="shall" pos="vmb" xml:id="A32435-001-a-2080">shall</w>
     <w lemma="begin" pos="vvi" xml:id="A32435-001-a-2090">Begin</w>
     <w lemma="and" pos="cc" xml:id="A32435-001-a-2100">and</w>
     <w lemma="be" pos="vvi" xml:id="A32435-001-a-2110">be</w>
     <w lemma="hold" pos="vvn" xml:id="A32435-001-a-2120">Holden</w>
     <w lemma="at" pos="acp" xml:id="A32435-001-a-2130">at</w>
     <hi xml:id="A32435-e210">
      <w lemma="Oxford" pos="nn1" xml:id="A32435-001-a-2140">Oxford</w>
      <pc xml:id="A32435-001-a-2150">,</pc>
     </hi>
     <w lemma="on" pos="acp" xml:id="A32435-001-a-2160">on</w>
     <w lemma="the" pos="d" xml:id="A32435-001-a-2170">the</w>
     <w lemma="one" pos="crd" xml:id="A32435-001-a-2180">One</w>
     <w lemma="and" pos="cc" xml:id="A32435-001-a-2190">and</w>
     <w lemma="twenty" pos="ord" xml:id="A32435-001-a-2200">twentieth</w>
     <w lemma="day" pos="n1" xml:id="A32435-001-a-2210">day</w>
     <w lemma="of" pos="acp" xml:id="A32435-001-a-2220">of</w>
     <w lemma="March" pos="nn1" rend="hi" xml:id="A32435-001-a-2230">March</w>
     <w lemma="next" pos="ord" xml:id="A32435-001-a-2240">next</w>
     <pc xml:id="A32435-001-a-2250">,</pc>
     <w lemma="1680." pos="crd" xml:id="A32435-001-a-2260">1680.</w>
     <pc unit="sentence" xml:id="A32435-001-a-2270"/>
    </p>
    <closer xml:id="A32435-e230">
     <dateline xml:id="A32435-e240">
      <w lemma="give" pos="vvn" xml:id="A32435-001-a-2280">Given</w>
      <w lemma="at" pos="acp" xml:id="A32435-001-a-2290">at</w>
      <w lemma="our" pos="po" xml:id="A32435-001-a-2300">our</w>
      <w lemma="court" pos="n1" xml:id="A32435-001-a-2310">Court</w>
      <w lemma="at" pos="acp" xml:id="A32435-001-a-2320">at</w>
      <w lemma="Whitehall" pos="nn1" rend="hi" xml:id="A32435-001-a-2330">Whitehall</w>
      <date xml:id="A32435-e260">
       <w lemma="the" pos="d" xml:id="A32435-001-a-2340">the</w>
       <w lemma="eighteen" pos="ord" xml:id="A32435-001-a-2350">Eighteenth</w>
       <w lemma="day" pos="n1" xml:id="A32435-001-a-2360">day</w>
       <w lemma="of" pos="acp" xml:id="A32435-001-a-2370">of</w>
       <hi xml:id="A32435-e270">
        <w lemma="January" pos="nn1" xml:id="A32435-001-a-2380">January</w>
        <pc xml:id="A32435-001-a-2390">,</pc>
       </hi>
       <w lemma="in" pos="acp" xml:id="A32435-001-a-2400">in</w>
       <w lemma="the" pos="d" xml:id="A32435-001-a-2410">the</w>
       <w lemma="two" pos="crd" xml:id="A32435-001-a-2420">Two</w>
       <w lemma="and" pos="cc" xml:id="A32435-001-a-2430">and</w>
       <w lemma="thirty" pos="ord" xml:id="A32435-001-a-2440">thirtieth</w>
       <w lemma="year" pos="n1" xml:id="A32435-001-a-2450">year</w>
       <w lemma="of" pos="acp" xml:id="A32435-001-a-2460">of</w>
       <w lemma="our" pos="po" xml:id="A32435-001-a-2470">Our</w>
       <w lemma="reign" pos="n1" xml:id="A32435-001-a-2480">Reign</w>
       <pc unit="sentence" xml:id="A32435-001-a-2490">.</pc>
      </date>
     </dateline>
     <lb xml:id="A32435-e280"/>
     <w lemma="God" pos="nn1" xml:id="A32435-001-a-2500">God</w>
     <w lemma="save" pos="vvb" xml:id="A32435-001-a-2510">save</w>
     <w lemma="the" pos="d" xml:id="A32435-001-a-2520">the</w>
     <w lemma="King" pos="n1" xml:id="A32435-001-a-2530">King</w>
     <pc unit="sentence" xml:id="A32435-001-a-2531">.</pc>
    </closer>
   </div>
  </body>
  <back xml:id="A32435-e290">
   <div type="colophon" xml:id="A32435-e300">
    <p xml:id="A32435-e310">
     <hi xml:id="A32435-e320">
      <w lemma="LONDON" pos="nn1" xml:id="A32435-001-a-2550">LONDON</w>
      <pc xml:id="A32435-001-a-2560">,</pc>
      <hi xml:id="A32435-e330">
       <w lemma="print" pos="vvn" xml:id="A32435-001-a-2570">Printed</w>
       <w lemma="by" pos="acp" xml:id="A32435-001-a-2580">by</w>
       <w lemma="the" pos="d" xml:id="A32435-001-a-2590">the</w>
       <w lemma="assign" pos="vvz" xml:id="A32435-001-a-2600">Assigns</w>
       <w lemma="of" pos="acp" xml:id="A32435-001-a-2610">of</w>
      </hi>
      <w lemma="John" pos="nn1" xml:id="A32435-001-a-2620">John</w>
      <w lemma="bill" pos="n1" xml:id="A32435-001-a-2630">Bill</w>
      <pc xml:id="A32435-001-a-2640">,</pc>
      <w lemma="Thomas" pos="nn1" xml:id="A32435-001-a-2650">Thomas</w>
      <w lemma="newcomb" pos="nn1" xml:id="A32435-001-a-2660">Newcomb</w>
      <pc xml:id="A32435-001-a-2670">,</pc>
      <w lemma="and" pos="cc" rend="hi" xml:id="A32435-001-a-2680">and</w>
      <w lemma="Henry" pos="nn1" xml:id="A32435-001-a-2690">Henry</w>
      <w lemma="hill" pos="n2" xml:id="A32435-001-a-2700">Hills</w>
      <pc xml:id="A32435-001-a-2710">,</pc>
      <hi xml:id="A32435-e350">
       <w lemma="printer" pos="n2" xml:id="A32435-001-a-2720">Printers</w>
       <w lemma="to" pos="acp" xml:id="A32435-001-a-2730">to</w>
       <w lemma="the" pos="d" xml:id="A32435-001-a-2740">the</w>
       <w lemma="king" pos="n2" xml:id="A32435-001-a-2750">Kings</w>
       <w lemma="most" pos="avs-d" xml:id="A32435-001-a-2760">most</w>
       <w lemma="excellent" pos="j" xml:id="A32435-001-a-2770">Excellent</w>
       <w lemma="majesty" pos="n1" xml:id="A32435-001-a-2780">Majesty</w>
       <pc unit="sentence" xml:id="A32435-001-a-2790">.</pc>
       <w lemma="1680." pos="crd" xml:id="A32435-001-a-2800">1680.</w>
       <pc unit="sentence" xml:id="A32435-001-a-2810"/>
      </hi>
     </hi>
    </p>
   </div>
  </back>
 </text>
</TEI>
