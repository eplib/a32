<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A32502">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>By the King. A proclamation for removing the receipt of His Majesties exchequer from Westminster to Nonsuch</title>
    <title>Proclamations. 1665-07-26.</title>
    <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A32502 of text R216305 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing C3416). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A32502</idno>
    <idno type="STC">Wing C3416</idno>
    <idno type="STC">ESTC R216305</idno>
    <idno type="EEBO-CITATION">99828039</idno>
    <idno type="PROQUEST">99828039</idno>
    <idno type="VID">32466</idno>
    <idno type="PROQUESTGOID">2240876761</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A32502)</note>
    <note>Transcribed from: (Early English Books Online ; image set 32466)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1944:15)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>By the King. A proclamation for removing the receipt of His Majesties exchequer from Westminster to Nonsuch</title>
      <title>Proclamations. 1665-07-26.</title>
      <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
      <author>Charles II, King of England, 1630-1685. aut</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>printed by John Bill and Christopher Barker, printers to the Kings most Excellent Majesty,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1665.</date>
     </publicationStmt>
     <notesStmt>
      <note>At end: Given at our court at Hampton-Court, the six and twentieth day of July, 1665. in the seventeenth year of our reign. God save the King.</note>
      <note>Reproduction of the original in the Guildhall Library, London.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>England and Wales. -- Exchequer -- Early works to 1800.</term>
     <term>Finance -- England -- Early works to 1800.</term>
     <term>Great Britain -- History -- Charles II, 1600-1685 -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>By the King. A proclamation for removing the receipt of His Majesties exchequer from Westminster to Nonsuch.</ep:title>
    <ep:author>England and Wales. Sovereign </ep:author>
    <ep:publicationYear>1665</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>395</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-07</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-08</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-09</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-09</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A32502-t">
  <body xml:id="A32502-e0">
   <div type="royal_proclamation" xml:id="A32502-e10">
    <pb facs="tcp:32466:1" rend="simple:additions" xml:id="A32502-001-a"/>
    <head xml:id="A32502-e20">
     <figure xml:id="A32502-e30">
      <p xml:id="A32502-e40">
       <w lemma="c" pos="sy" xml:id="A32502-001-a-0010">C</w>
       <w lemma="r" pos="sy" xml:id="A32502-001-a-0020">R</w>
      </p>
      <p xml:id="A32502-e50">
       <w lemma="n/a" pos="ffr" xml:id="A32502-001-a-0030">DIEV</w>
       <w lemma="n/a" pos="ffr" xml:id="A32502-001-a-0040">ET</w>
       <w lemma="n/a" pos="ffr" xml:id="A32502-001-a-0050">MON</w>
       <w lemma="n/a" pos="ffr" xml:id="A32502-001-a-0060">DROIT</w>
      </p>
      <p xml:id="A32502-e60">
       <w lemma="n/a" pos="ffr" xml:id="A32502-001-a-0070">HONI</w>
       <w lemma="n/a" pos="ffr" xml:id="A32502-001-a-0080">SOIT</w>
       <w lemma="n/a" pos="ffr" xml:id="A32502-001-a-0090">QVI</w>
       <w lemma="n/a" pos="ffr" xml:id="A32502-001-a-0100">MAL</w>
       <w lemma="n/a" pos="ffr" xml:id="A32502-001-a-0110">Y</w>
       <w lemma="n/a" pos="ffr" xml:id="A32502-001-a-0120">PENSE</w>
      </p>
      <figDesc xml:id="A32502-e70">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <byline xml:id="A32502-e80">
     <w lemma="by" pos="acp" xml:id="A32502-001-a-0130">By</w>
     <w lemma="the" pos="d" xml:id="A32502-001-a-0140">the</w>
     <w lemma="King" pos="n1" xml:id="A32502-001-a-0150">King</w>
     <pc unit="sentence" xml:id="A32502-001-a-0151">.</pc>
    </byline>
    <head xml:id="A32502-e90">
     <w lemma="a" pos="d" xml:id="A32502-001-a-0170">A</w>
     <w lemma="proclamation" pos="n1" xml:id="A32502-001-a-0180">PROCLAMATION</w>
     <w lemma="for" pos="acp" xml:id="A32502-001-a-0190">For</w>
     <w lemma="remove" pos="vvg" xml:id="A32502-001-a-0200">removing</w>
     <w lemma="the" pos="d" xml:id="A32502-001-a-0210">the</w>
     <w lemma="receipt" pos="n1" xml:id="A32502-001-a-0220">Receipt</w>
     <w lemma="of" pos="acp" xml:id="A32502-001-a-0230">of</w>
     <w lemma="his" pos="po" xml:id="A32502-001-a-0240">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A32502-001-a-0250">Majesties</w>
     <w lemma="exchequer" pos="n1" xml:id="A32502-001-a-0260">Exchequer</w>
     <w lemma="from" pos="acp" xml:id="A32502-001-a-0270">from</w>
     <w lemma="Westminster" pos="nn1" rend="hi" xml:id="A32502-001-a-0280">Westminster</w>
     <w lemma="to" pos="acp" xml:id="A32502-001-a-0290">to</w>
     <hi xml:id="A32502-e110">
      <w lemma="Nonsuch" pos="nn1" xml:id="A32502-001-a-0300">Nonsuch</w>
      <pc unit="sentence" xml:id="A32502-001-a-0310">.</pc>
     </hi>
    </head>
    <opener xml:id="A32502-e120">
     <signed xml:id="A32502-e130">
      <w lemma="CHARLES" pos="nn1" xml:id="A32502-001-a-0320">CHARLES</w>
      <w lemma="r." pos="ab" xml:id="A32502-001-a-0330">R.</w>
      <pc unit="sentence" xml:id="A32502-001-a-0340"/>
     </signed>
    </opener>
    <p xml:id="A32502-e140">
     <w lemma="the" pos="d" rend="decorinit" xml:id="A32502-001-a-0350">THe</w>
     <w lemma="king" pos="ng1" reg="King's" xml:id="A32502-001-a-0360">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A32502-001-a-0370">most</w>
     <w lemma="excellent" pos="j" xml:id="A32502-001-a-0380">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A32502-001-a-0390">Majesty</w>
     <w lemma="take" pos="vvg" xml:id="A32502-001-a-0400">taking</w>
     <w lemma="into" pos="acp" xml:id="A32502-001-a-0410">into</w>
     <w lemma="his" pos="po" xml:id="A32502-001-a-0420">His</w>
     <w lemma="princely" pos="j" xml:id="A32502-001-a-0430">Princely</w>
     <w lemma="consideration" pos="n1" xml:id="A32502-001-a-0440">Consideration</w>
     <w lemma="the" pos="d" xml:id="A32502-001-a-0450">the</w>
     <w lemma="great" pos="j" xml:id="A32502-001-a-0460">great</w>
     <w lemma="and" pos="cc" xml:id="A32502-001-a-0470">and</w>
     <w lemma="dangerous" pos="j" xml:id="A32502-001-a-0480">dangerous</w>
     <w lemma="increase" pos="n1" xml:id="A32502-001-a-0490">increase</w>
     <w lemma="of" pos="acp" xml:id="A32502-001-a-0500">of</w>
     <w lemma="the" pos="d" xml:id="A32502-001-a-0510">the</w>
     <w lemma="plague" pos="n1" xml:id="A32502-001-a-0520">Plague</w>
     <w lemma="in" pos="acp" xml:id="A32502-001-a-0530">in</w>
     <w lemma="and" pos="cc" xml:id="A32502-001-a-0540">and</w>
     <w lemma="about" pos="acp" xml:id="A32502-001-a-0550">about</w>
     <w lemma="the" pos="d" xml:id="A32502-001-a-0560">the</w>
     <w lemma="city" pos="n1" xml:id="A32502-001-a-0570">City</w>
     <w lemma="of" pos="acp" xml:id="A32502-001-a-0580">of</w>
     <hi xml:id="A32502-e150">
      <w lemma="Westminster" pos="nn1" xml:id="A32502-001-a-0590">Westminster</w>
      <pc xml:id="A32502-001-a-0600">,</pc>
     </hi>
     <w lemma="where" pos="crq" xml:id="A32502-001-a-0610">where</w>
     <w lemma="his" pos="po" xml:id="A32502-001-a-0620">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A32502-001-a-0630">Majesties</w>
     <w lemma="receipt" pos="n1" xml:id="A32502-001-a-0640">Receipt</w>
     <w lemma="of" pos="acp" xml:id="A32502-001-a-0650">of</w>
     <w lemma="exchequer" pos="n1" xml:id="A32502-001-a-0660">Exchequer</w>
     <w lemma="have" pos="vvz" xml:id="A32502-001-a-0670">hath</w>
     <w lemma="be" pos="vvn" xml:id="A32502-001-a-0680">been</w>
     <w lemma="hitherto" pos="av" xml:id="A32502-001-a-0690">hitherto</w>
     <w lemma="keep" pos="vvn" xml:id="A32502-001-a-0700">kept</w>
     <pc xml:id="A32502-001-a-0710">;</pc>
     <w lemma="&amp;" pos="cc" xml:id="A32502-001-a-0720">&amp;</w>
     <w lemma="willing" pos="j" xml:id="A32502-001-a-0730">willing</w>
     <pc xml:id="A32502-001-a-0740">,</pc>
     <w lemma="as" pos="acp" xml:id="A32502-001-a-0750">as</w>
     <w lemma="much" pos="av-d" xml:id="A32502-001-a-0760">much</w>
     <w lemma="as" pos="acp" xml:id="A32502-001-a-0770">as</w>
     <w lemma="be" pos="vvz" xml:id="A32502-001-a-0780">is</w>
     <w lemma="possible" pos="j" xml:id="A32502-001-a-0790">possible</w>
     <pc xml:id="A32502-001-a-0800">,</pc>
     <w lemma="to" pos="prt" xml:id="A32502-001-a-0810">to</w>
     <w lemma="prevent" pos="vvi" xml:id="A32502-001-a-0820">prevent</w>
     <w lemma="the" pos="d" xml:id="A32502-001-a-0830">the</w>
     <w lemma="further" pos="jc" xml:id="A32502-001-a-0840">further</w>
     <w lemma="danger" pos="n1" xml:id="A32502-001-a-0850">danger</w>
     <w lemma="which" pos="crq" xml:id="A32502-001-a-0860">which</w>
     <w lemma="may" pos="vmd" xml:id="A32502-001-a-0870">might</w>
     <w lemma="ensue" pos="vvi" xml:id="A32502-001-a-0880">ensue</w>
     <w lemma="as" pos="acp" xml:id="A32502-001-a-0890">as</w>
     <w lemma="well" pos="av" xml:id="A32502-001-a-0900">well</w>
     <w lemma="to" pos="prt" xml:id="A32502-001-a-0910">to</w>
     <w lemma="his" pos="po" xml:id="A32502-001-a-0920">His</w>
     <w lemma="own" pos="d" xml:id="A32502-001-a-0930">own</w>
     <w lemma="officer" pos="n2" xml:id="A32502-001-a-0940">Officers</w>
     <pc xml:id="A32502-001-a-0950">,</pc>
     <w lemma="which" pos="crq" xml:id="A32502-001-a-0960">which</w>
     <w lemma="be" pos="vvb" xml:id="A32502-001-a-0970">are</w>
     <w lemma="necessary" pos="av-j" xml:id="A32502-001-a-0980">necessarily</w>
     <w lemma="to" pos="prt" xml:id="A32502-001-a-0990">to</w>
     <w lemma="attend" pos="vvi" xml:id="A32502-001-a-1000">attend</w>
     <w lemma="the" pos="d" xml:id="A32502-001-a-1010">the</w>
     <w lemma="same" pos="d" xml:id="A32502-001-a-1020">same</w>
     <w lemma="receipt" pos="n1" xml:id="A32502-001-a-1030">Receipt</w>
     <pc xml:id="A32502-001-a-1040">,</pc>
     <w lemma="as" pos="acp" xml:id="A32502-001-a-1050">as</w>
     <w lemma="to" pos="acp" xml:id="A32502-001-a-1060">to</w>
     <w lemma="other" pos="d" xml:id="A32502-001-a-1070">other</w>
     <w lemma="his" pos="po" xml:id="A32502-001-a-1080">His</w>
     <w lemma="love" pos="j-vg" xml:id="A32502-001-a-1090">loving</w>
     <w lemma="subject" pos="n2" xml:id="A32502-001-a-1100">Subjects</w>
     <pc xml:id="A32502-001-a-1110">,</pc>
     <w lemma="who" pos="crq" xml:id="A32502-001-a-1120">who</w>
     <w lemma="shall" pos="vmb" xml:id="A32502-001-a-1130">shall</w>
     <w lemma="have" pos="vvi" xml:id="A32502-001-a-1140">have</w>
     <w lemma="occasion" pos="n1" xml:id="A32502-001-a-1150">occasion</w>
     <w lemma="either" pos="av-d" xml:id="A32502-001-a-1160">either</w>
     <w lemma="for" pos="acp" xml:id="A32502-001-a-1170">for</w>
     <w lemma="receipt" pos="n1" xml:id="A32502-001-a-1180">Receipt</w>
     <w lemma="or" pos="cc" xml:id="A32502-001-a-1190">or</w>
     <w lemma="payment" pos="n1" xml:id="A32502-001-a-1200">Payment</w>
     <w lemma="of" pos="acp" xml:id="A32502-001-a-1210">of</w>
     <w lemma="money" pos="n2" xml:id="A32502-001-a-1220">Moneys</w>
     <w lemma="to" pos="prt" xml:id="A32502-001-a-1230">to</w>
     <w lemma="repair" pos="vvi" xml:id="A32502-001-a-1240">repair</w>
     <w lemma="thither" pos="av" xml:id="A32502-001-a-1250">thither</w>
     <pc xml:id="A32502-001-a-1260">;</pc>
     <w lemma="have" pos="vvz" xml:id="A32502-001-a-1270">Hath</w>
     <w lemma="therefore" pos="av" xml:id="A32502-001-a-1280">therefore</w>
     <w lemma="take" pos="vvn" xml:id="A32502-001-a-1290">taken</w>
     <w lemma="order" pos="n1" xml:id="A32502-001-a-1300">Order</w>
     <w lemma="for" pos="acp" xml:id="A32502-001-a-1310">for</w>
     <w lemma="the" pos="d" xml:id="A32502-001-a-1320">the</w>
     <w lemma="present" pos="j" xml:id="A32502-001-a-1330">present</w>
     <w lemma="remove" pos="n1" xml:id="A32502-001-a-1340">Remove</w>
     <w lemma="of" pos="acp" xml:id="A32502-001-a-1350">of</w>
     <w lemma="the" pos="d" xml:id="A32502-001-a-1360">the</w>
     <w lemma="receipt" pos="n1" xml:id="A32502-001-a-1370">Receipt</w>
     <w lemma="of" pos="acp" xml:id="A32502-001-a-1380">of</w>
     <w lemma="his" pos="po" xml:id="A32502-001-a-1390">His</w>
     <w lemma="say" pos="j-vn" xml:id="A32502-001-a-1400">said</w>
     <w lemma="exchequer" pos="n1" xml:id="A32502-001-a-1410">Exchequer</w>
     <pc xml:id="A32502-001-a-1420">,</pc>
     <w lemma="together" pos="av" xml:id="A32502-001-a-1430">together</w>
     <w lemma="with" pos="acp" xml:id="A32502-001-a-1440">with</w>
     <w lemma="the" pos="d" xml:id="A32502-001-a-1450">the</w>
     <w lemma="tally-office" pos="n1" xml:id="A32502-001-a-1460">Tally-Office</w>
     <pc xml:id="A32502-001-a-1470">,</pc>
     <w lemma="and" pos="cc" xml:id="A32502-001-a-1480">and</w>
     <w lemma="all" pos="d" xml:id="A32502-001-a-1490">all</w>
     <w lemma="thing" pos="n2" xml:id="A32502-001-a-1500">things</w>
     <w lemma="thereunto" pos="av" xml:id="A32502-001-a-1510">thereunto</w>
     <w lemma="belong" pos="vvg" xml:id="A32502-001-a-1520">belonging</w>
     <pc xml:id="A32502-001-a-1530">,</pc>
     <w lemma="from" pos="acp" xml:id="A32502-001-a-1540">from</w>
     <w lemma="thence" pos="av" xml:id="A32502-001-a-1550">thence</w>
     <w lemma="to" pos="acp" xml:id="A32502-001-a-1560">to</w>
     <w lemma="his" pos="po" xml:id="A32502-001-a-1570">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A32502-001-a-1580">Majesties</w>
     <w lemma="honour" pos="n1" xml:id="A32502-001-a-1590">Honour</w>
     <w lemma="of" pos="acp" xml:id="A32502-001-a-1600">of</w>
     <w lemma="Nonsuch" pos="nn1" rend="hi" xml:id="A32502-001-a-1610">Nonsuch</w>
     <w lemma="in" pos="acp" xml:id="A32502-001-a-1620">in</w>
     <w lemma="the" pos="d" xml:id="A32502-001-a-1630">the</w>
     <w lemma="county" pos="n1" xml:id="A32502-001-a-1640">County</w>
     <w lemma="of" pos="acp" xml:id="A32502-001-a-1650">of</w>
     <hi xml:id="A32502-e170">
      <w lemma="Surrey" pos="nn1" xml:id="A32502-001-a-1660">Surrey</w>
      <pc xml:id="A32502-001-a-1670">:</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A32502-001-a-1680">And</w>
     <w lemma="have" pos="vvz" xml:id="A32502-001-a-1690">hath</w>
     <w lemma="think" pos="vvn" xml:id="A32502-001-a-1700">thought</w>
     <w lemma="fit" pos="j" xml:id="A32502-001-a-1710">fit</w>
     <w lemma="by" pos="acp" xml:id="A32502-001-a-1720">by</w>
     <w lemma="this" pos="d" xml:id="A32502-001-a-1730">this</w>
     <w lemma="his" pos="po" xml:id="A32502-001-a-1740">His</w>
     <w lemma="proclamation" pos="n1" xml:id="A32502-001-a-1750">Proclamation</w>
     <w lemma="to" pos="prt" xml:id="A32502-001-a-1760">to</w>
     <w lemma="publish" pos="vvb" xml:id="A32502-001-a-1770">Publish</w>
     <pc xml:id="A32502-001-a-1780">,</pc>
     <w lemma="that" pos="cs" xml:id="A32502-001-a-1790">That</w>
     <w lemma="the" pos="d" xml:id="A32502-001-a-1800">the</w>
     <w lemma="same" pos="d" xml:id="A32502-001-a-1810">same</w>
     <w lemma="shall" pos="vmb" xml:id="A32502-001-a-1820">shall</w>
     <w lemma="be" pos="vvi" xml:id="A32502-001-a-1830">be</w>
     <w lemma="there" pos="av" xml:id="A32502-001-a-1840">there</w>
     <w lemma="open" pos="vvn" xml:id="A32502-001-a-1850">opened</w>
     <w lemma="on" pos="acp" xml:id="A32502-001-a-1860">on</w>
     <w lemma="the" pos="d" xml:id="A32502-001-a-1870">the</w>
     <w lemma="fifteen" pos="ord" xml:id="A32502-001-a-1880">Fifteenth</w>
     <w lemma="day" pos="n1" xml:id="A32502-001-a-1890">day</w>
     <w lemma="of" pos="acp" xml:id="A32502-001-a-1900">of</w>
     <w lemma="August" pos="nn1" rend="hi" xml:id="A32502-001-a-1910">August</w>
     <w lemma="next" pos="ord" xml:id="A32502-001-a-1920">next</w>
     <pc xml:id="A32502-001-a-1930">,</pc>
     <w lemma="to" pos="acp" xml:id="A32502-001-a-1940">to</w>
     <w lemma="the" pos="d" xml:id="A32502-001-a-1950">the</w>
     <w lemma="end" pos="n1" xml:id="A32502-001-a-1960">end</w>
     <w lemma="that" pos="cs" xml:id="A32502-001-a-1970">that</w>
     <w lemma="all" pos="d" xml:id="A32502-001-a-1980">all</w>
     <w lemma="person" pos="n2" xml:id="A32502-001-a-1990">persons</w>
     <w lemma="who" pos="crq" xml:id="A32502-001-a-2000">whom</w>
     <w lemma="the" pos="d" xml:id="A32502-001-a-2010">the</w>
     <w lemma="same" pos="d" xml:id="A32502-001-a-2020">same</w>
     <w lemma="may" pos="vmb" xml:id="A32502-001-a-2030">may</w>
     <w lemma="concern" pos="vvi" xml:id="A32502-001-a-2040">concern</w>
     <pc xml:id="A32502-001-a-2050">,</pc>
     <w lemma="may" pos="vmb" xml:id="A32502-001-a-2060">may</w>
     <w lemma="take" pos="vvi" xml:id="A32502-001-a-2070">take</w>
     <w lemma="notice" pos="n1" xml:id="A32502-001-a-2080">notice</w>
     <w lemma="whither" pos="crq" xml:id="A32502-001-a-2090">whither</w>
     <w lemma="to" pos="prt" xml:id="A32502-001-a-2100">to</w>
     <w lemma="repair" pos="vvi" xml:id="A32502-001-a-2110">repair</w>
     <w lemma="upon" pos="acp" xml:id="A32502-001-a-2120">upon</w>
     <w lemma="all" pos="d" xml:id="A32502-001-a-2130">all</w>
     <w lemma="occasion" pos="n2" xml:id="A32502-001-a-2140">occasions</w>
     <pc xml:id="A32502-001-a-2150">,</pc>
     <w lemma="concern" pos="vvg" xml:id="A32502-001-a-2160">concerning</w>
     <w lemma="the" pos="d" xml:id="A32502-001-a-2170">the</w>
     <w lemma="bring" pos="n1-vg" xml:id="A32502-001-a-2180">bringing</w>
     <w lemma="in" pos="acp" xml:id="A32502-001-a-2190">in</w>
     <pc xml:id="A32502-001-a-2200">,</pc>
     <w lemma="or" pos="cc" xml:id="A32502-001-a-2210">or</w>
     <w lemma="issue" pos="vvg" xml:id="A32502-001-a-2220">issuing</w>
     <w lemma="out" pos="av" xml:id="A32502-001-a-2230">out</w>
     <w lemma="of" pos="acp" xml:id="A32502-001-a-2240">of</w>
     <w lemma="his" pos="po" xml:id="A32502-001-a-2250">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A32502-001-a-2260">Majesties</w>
     <w lemma="treasure" pos="n1" xml:id="A32502-001-a-2270">Treasure</w>
     <w lemma="at" pos="acp" xml:id="A32502-001-a-2280">at</w>
     <w lemma="the" pos="d" xml:id="A32502-001-a-2290">the</w>
     <w lemma="receipt" pos="n1" xml:id="A32502-001-a-2300">Receipt</w>
     <w lemma="of" pos="acp" xml:id="A32502-001-a-2310">of</w>
     <w lemma="his" pos="po" xml:id="A32502-001-a-2320">His</w>
     <w lemma="exchequer" pos="n1" xml:id="A32502-001-a-2330">Exchequer</w>
     <pc unit="sentence" xml:id="A32502-001-a-2340">.</pc>
     <w lemma="will" pos="vvg" xml:id="A32502-001-a-2350">Willing</w>
     <w lemma="and" pos="cc" xml:id="A32502-001-a-2360">and</w>
     <w lemma="require" pos="vvg" xml:id="A32502-001-a-2370">requiring</w>
     <w lemma="all" pos="d" xml:id="A32502-001-a-2380">all</w>
     <w lemma="sheriff" pos="n2" xml:id="A32502-001-a-2390">Sheriffs</w>
     <pc xml:id="A32502-001-a-2400">,</pc>
     <w lemma="bailiff" pos="n2" reg="Bailiffs" xml:id="A32502-001-a-2410">Bayliffs</w>
     <pc xml:id="A32502-001-a-2420">,</pc>
     <w lemma="collector" pos="n2" xml:id="A32502-001-a-2430">Collectors</w>
     <pc xml:id="A32502-001-a-2440">,</pc>
     <w lemma="and" pos="cc" xml:id="A32502-001-a-2450">and</w>
     <w lemma="all" pos="d" xml:id="A32502-001-a-2460">all</w>
     <w lemma="other" pos="d" xml:id="A32502-001-a-2470">other</w>
     <w lemma="officer" pos="n2" xml:id="A32502-001-a-2480">Officers</w>
     <pc xml:id="A32502-001-a-2490">,</pc>
     <w lemma="accountant" pos="n2" xml:id="A32502-001-a-2500">Accomptants</w>
     <pc xml:id="A32502-001-a-2510">,</pc>
     <w lemma="and" pos="cc" xml:id="A32502-001-a-2520">and</w>
     <w lemma="person" pos="n2" xml:id="A32502-001-a-2530">persons</w>
     <w lemma="whatsoever" pos="crq" xml:id="A32502-001-a-2540">whatsoever</w>
     <pc xml:id="A32502-001-a-2550">,</pc>
     <w lemma="who" pos="crq" xml:id="A32502-001-a-2560">who</w>
     <w lemma="be" pos="vvb" xml:id="A32502-001-a-2570">are</w>
     <w lemma="to" pos="prt" xml:id="A32502-001-a-2580">to</w>
     <w lemma="pay" pos="vvi" xml:id="A32502-001-a-2590">pay</w>
     <w lemma="in" pos="acp" xml:id="A32502-001-a-2600">in</w>
     <w lemma="any" pos="d" xml:id="A32502-001-a-2610">any</w>
     <w lemma="money" pos="n2" xml:id="A32502-001-a-2620">Moneys</w>
     <w lemma="into" pos="acp" xml:id="A32502-001-a-2630">into</w>
     <w lemma="the" pos="d" xml:id="A32502-001-a-2640">the</w>
     <w lemma="say" pos="j-vn" xml:id="A32502-001-a-2650">said</w>
     <w lemma="receipt" pos="n1" xml:id="A32502-001-a-2660">Receipt</w>
     <w lemma="of" pos="acp" xml:id="A32502-001-a-2670">of</w>
     <w lemma="his" pos="po" xml:id="A32502-001-a-2680">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A32502-001-a-2690">Majesties</w>
     <w lemma="exchequer" pos="n1" xml:id="A32502-001-a-2700">Exchequer</w>
     <pc xml:id="A32502-001-a-2710">,</pc>
     <w lemma="or" pos="cc" xml:id="A32502-001-a-2720">or</w>
     <w lemma="otherwise" pos="av" xml:id="A32502-001-a-2730">otherwise</w>
     <w lemma="to" pos="prt" xml:id="A32502-001-a-2740">to</w>
     <w lemma="attend" pos="vvi" xml:id="A32502-001-a-2750">attend</w>
     <w lemma="the" pos="d" xml:id="A32502-001-a-2760">the</w>
     <w lemma="same" pos="d" xml:id="A32502-001-a-2770">same</w>
     <pc xml:id="A32502-001-a-2780">,</pc>
     <w lemma="to" pos="prt" xml:id="A32502-001-a-2790">to</w>
     <w lemma="keep" pos="vvi" xml:id="A32502-001-a-2800">keep</w>
     <w lemma="their" pos="po" xml:id="A32502-001-a-2810">their</w>
     <w lemma="day" pos="n2" xml:id="A32502-001-a-2820">days</w>
     <w lemma="and" pos="cc" xml:id="A32502-001-a-2830">and</w>
     <w lemma="time" pos="n2" xml:id="A32502-001-a-2840">times</w>
     <w lemma="at" pos="acp" xml:id="A32502-001-a-2850">at</w>
     <w lemma="Nonsuch" pos="nn1" rend="hi" xml:id="A32502-001-a-2860">Nonsuch</w>
     <w lemma="aforesaid" pos="j" xml:id="A32502-001-a-2870">aforesaid</w>
     <pc xml:id="A32502-001-a-2880">,</pc>
     <w lemma="and" pos="cc" xml:id="A32502-001-a-2890">and</w>
     <w lemma="there" pos="av" xml:id="A32502-001-a-2900">there</w>
     <w lemma="to" pos="prt" xml:id="A32502-001-a-2910">to</w>
     <w lemma="do" pos="vvi" xml:id="A32502-001-a-2920">do</w>
     <pc xml:id="A32502-001-a-2930">,</pc>
     <w lemma="pay" pos="vvb" xml:id="A32502-001-a-2940">pay</w>
     <pc xml:id="A32502-001-a-2950">,</pc>
     <w lemma="and" pos="cc" xml:id="A32502-001-a-2960">and</w>
     <w lemma="perform" pos="vvi" xml:id="A32502-001-a-2970">perform</w>
     <w lemma="in" pos="acp" xml:id="A32502-001-a-2980">in</w>
     <w lemma="all" pos="d" xml:id="A32502-001-a-2990">all</w>
     <w lemma="thing" pos="n2" xml:id="A32502-001-a-3000">things</w>
     <pc xml:id="A32502-001-a-3010">,</pc>
     <w lemma="as" pos="acp" xml:id="A32502-001-a-3020">as</w>
     <w lemma="they" pos="pns" xml:id="A32502-001-a-3030">they</w>
     <w lemma="shall" pos="vmd" xml:id="A32502-001-a-3040">should</w>
     <pc xml:id="A32502-001-a-3050">,</pc>
     <w lemma="or" pos="cc" xml:id="A32502-001-a-3060">or</w>
     <w lemma="aught" pos="pi" reg="aught" xml:id="A32502-001-a-3070">ought</w>
     <w lemma="to" pos="prt" xml:id="A32502-001-a-3080">to</w>
     <w lemma="have" pos="vvi" xml:id="A32502-001-a-3090">have</w>
     <w lemma="do" pos="vvn" xml:id="A32502-001-a-3100">done</w>
     <w lemma="at" pos="acp" xml:id="A32502-001-a-3110">at</w>
     <hi xml:id="A32502-e200">
      <w lemma="Westminster" pos="nn1" xml:id="A32502-001-a-3120">Westminster</w>
      <pc xml:id="A32502-001-a-3130">,</pc>
     </hi>
     <w lemma="if" pos="cs" xml:id="A32502-001-a-3140">if</w>
     <w lemma="the" pos="d" xml:id="A32502-001-a-3150">the</w>
     <w lemma="say" pos="j-vn" xml:id="A32502-001-a-3160">said</w>
     <w lemma="receipt" pos="n1" xml:id="A32502-001-a-3170">Receipt</w>
     <w lemma="of" pos="acp" xml:id="A32502-001-a-3180">of</w>
     <w lemma="exchequer" pos="n1" xml:id="A32502-001-a-3190">Exchequer</w>
     <w lemma="have" pos="vvd" xml:id="A32502-001-a-3200">had</w>
     <w lemma="continue" pos="vvn" xml:id="A32502-001-a-3210">continued</w>
     <w lemma="there" pos="av" xml:id="A32502-001-a-3220">there</w>
     <pc unit="sentence" xml:id="A32502-001-a-3230">.</pc>
     <w lemma="and" pos="cc" xml:id="A32502-001-a-3240">And</w>
     <w lemma="this" pos="d" xml:id="A32502-001-a-3250">this</w>
     <w lemma="to" pos="prt" xml:id="A32502-001-a-3260">to</w>
     <w lemma="be" pos="vvi" xml:id="A32502-001-a-3270">be</w>
     <w lemma="do" pos="vvn" xml:id="A32502-001-a-3280">done</w>
     <w lemma="and" pos="cc" xml:id="A32502-001-a-3290">and</w>
     <w lemma="observe" pos="vvn" xml:id="A32502-001-a-3300">observed</w>
     <w lemma="until" pos="acp" xml:id="A32502-001-a-3310">until</w>
     <w lemma="his" pos="po" xml:id="A32502-001-a-3320">His</w>
     <w lemma="majesty" pos="n1" xml:id="A32502-001-a-3330">Majesty</w>
     <w lemma="shall" pos="vmb" xml:id="A32502-001-a-3340">shall</w>
     <w lemma="publish" pos="vvi" xml:id="A32502-001-a-3350">publish</w>
     <w lemma="and" pos="cc" xml:id="A32502-001-a-3360">and</w>
     <w lemma="declare" pos="vvi" xml:id="A32502-001-a-3370">declare</w>
     <w lemma="his" pos="po" xml:id="A32502-001-a-3380">His</w>
     <w lemma="further" pos="jc" xml:id="A32502-001-a-3390">further</w>
     <w lemma="pleasure" pos="n1" xml:id="A32502-001-a-3400">Pleasure</w>
     <w lemma="to" pos="acp" xml:id="A32502-001-a-3410">to</w>
     <w lemma="the" pos="d" xml:id="A32502-001-a-3420">the</w>
     <w lemma="contrary" pos="n1-j" xml:id="A32502-001-a-3430">contrary</w>
     <pc unit="sentence" xml:id="A32502-001-a-3440">.</pc>
    </p>
    <closer xml:id="A32502-e210">
     <dateline xml:id="A32502-e220">
      <w lemma="give" pos="vvn" xml:id="A32502-001-a-3450">Given</w>
      <w lemma="at" pos="acp" xml:id="A32502-001-a-3460">at</w>
      <w lemma="our" pos="po" xml:id="A32502-001-a-3470">Our</w>
      <w lemma="court" pos="n1" xml:id="A32502-001-a-3480">Court</w>
      <w lemma="at" pos="acp" xml:id="A32502-001-a-3490">at</w>
      <hi xml:id="A32502-e230">
       <w lemma="Hampton-Court" pos="nn1" xml:id="A32502-001-a-3500">Hampton-Court</w>
       <pc xml:id="A32502-001-a-3510">,</pc>
      </hi>
      <date xml:id="A32502-e240">
       <w lemma="the" pos="d" xml:id="A32502-001-a-3520">the</w>
       <w lemma="six" pos="crd" xml:id="A32502-001-a-3530">Six</w>
       <w lemma="and" pos="cc" xml:id="A32502-001-a-3540">and</w>
       <w lemma="twenty" pos="ord" xml:id="A32502-001-a-3550">twentieth</w>
       <w lemma="day" pos="n1" xml:id="A32502-001-a-3560">day</w>
       <w lemma="of" pos="acp" xml:id="A32502-001-a-3570">of</w>
       <hi xml:id="A32502-e250">
        <w lemma="July" pos="nn1" xml:id="A32502-001-a-3580">July</w>
        <pc xml:id="A32502-001-a-3590">,</pc>
       </hi>
       <w lemma="1665." pos="crd" xml:id="A32502-001-a-3600">1665.</w>
       <w lemma="in" pos="acp" xml:id="A32502-001-a-3610">in</w>
       <w lemma="the" pos="d" xml:id="A32502-001-a-3620">the</w>
       <w lemma="seventeen" pos="ord" xml:id="A32502-001-a-3630">Seventeenth</w>
       <w lemma="year" pos="n1" xml:id="A32502-001-a-3640">year</w>
       <w lemma="of" pos="acp" xml:id="A32502-001-a-3650">of</w>
       <w lemma="our" pos="po" xml:id="A32502-001-a-3660">Our</w>
       <w lemma="reign" pos="n1" xml:id="A32502-001-a-3670">Reign</w>
       <pc unit="sentence" xml:id="A32502-001-a-3680">.</pc>
      </date>
     </dateline>
     <lb xml:id="A32502-e260"/>
     <w lemma="God" pos="nn1" xml:id="A32502-001-a-3690">God</w>
     <w lemma="save" pos="vvb" xml:id="A32502-001-a-3700">save</w>
     <w lemma="the" pos="d" xml:id="A32502-001-a-3710">the</w>
     <w lemma="King" pos="n1" xml:id="A32502-001-a-3720">King</w>
     <pc unit="sentence" xml:id="A32502-001-a-3721">.</pc>
    </closer>
   </div>
  </body>
  <back xml:id="A32502-e270">
   <div type="colophon" xml:id="A32502-e280">
    <p xml:id="A32502-e290">
     <hi xml:id="A32502-e300">
      <w lemma="LONDON" pos="nn1" xml:id="A32502-001-a-3740">LONDON</w>
      <pc xml:id="A32502-001-a-3750">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A32502-001-a-3760">Printed</w>
     <w lemma="by" pos="acp" xml:id="A32502-001-a-3770">by</w>
     <hi xml:id="A32502-e310">
      <w lemma="John" pos="nn1" xml:id="A32502-001-a-3780">John</w>
      <w lemma="bill" pos="n1" xml:id="A32502-001-a-3790">Bill</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="A32502-001-a-3800">and</w>
     <hi xml:id="A32502-e320">
      <w lemma="Christopher" pos="nn1" xml:id="A32502-001-a-3810">Christopher</w>
      <w lemma="Barker" pos="nn1" xml:id="A32502-001-a-3820">Barker</w>
      <pc xml:id="A32502-001-a-3830">,</pc>
     </hi>
     <w lemma="printer" pos="n2" xml:id="A32502-001-a-3840">Printers</w>
     <w lemma="to" pos="acp" xml:id="A32502-001-a-3850">to</w>
     <w lemma="the" pos="d" xml:id="A32502-001-a-3860">the</w>
     <w lemma="king" pos="n2" xml:id="A32502-001-a-3870">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A32502-001-a-3880">most</w>
     <w lemma="excellent" pos="j" xml:id="A32502-001-a-3890">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A32502-001-a-3900">Majesty</w>
     <pc xml:id="A32502-001-a-3910">,</pc>
     <w lemma="1665." pos="crd" xml:id="A32502-001-a-3920">1665.</w>
     <pc unit="sentence" xml:id="A32502-001-a-3930"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
