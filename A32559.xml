<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A32559">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>By the King, a proclamation for the free exportation of woolen manufacturers until the 25th day of December next</title>
    <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A32559 of text R30903 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing C3484). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A32559</idno>
    <idno type="STC">Wing C3484</idno>
    <idno type="STC">ESTC R30903</idno>
    <idno type="EEBO-CITATION">11687131</idno>
    <idno type="OCLC">ocm 11687131</idno>
    <idno type="VID">48153</idno>
    <idno type="PROQUESTGOID">2240865006</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A32559)</note>
    <note>Transcribed from: (Early English Books Online ; image set 48153)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1482:6)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>By the King, a proclamation for the free exportation of woolen manufacturers until the 25th day of December next</title>
      <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
      <author>Charles II, King of England, 1630-1685.</author>
     </titleStmt>
     <extent>1 broadside.</extent>
     <publicationStmt>
      <publisher>Printed by John Bill and Christopher Barker ...,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1666.</date>
     </publicationStmt>
     <notesStmt>
      <note>"Given at our court at Whitehall the 15th day of April, 1666, in the 18th year of our reign."</note>
      <note>Reproduction of original in the Cambridge University Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Proclamations -- Great Britain.</term>
     <term>Great Britain -- Foreign economic relations.</term>
     <term>Great Britain -- Commercial policy.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>By the King. A proclamation for the free exportation of woollen manufacturers until the 25th day of December next.</ep:title>
    <ep:author>England and Wales. Sovereign </ep:author>
    <ep:publicationYear>1666</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>387</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-07</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-08</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-09</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-09</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A32559-t">
  <body xml:id="A32559-e0">
   <div type="royal_proclamation" xml:id="A32559-e10">
    <pb facs="tcp:48153:1" xml:id="A32559-001-a"/>
    <head xml:id="A32559-e20">
     <figure xml:id="A32559-e30">
      <p xml:id="A32559-e40">
       <w lemma="c" pos="sy" xml:id="A32559-001-a-0010">C</w>
       <w lemma="r" pos="sy" xml:id="A32559-001-a-0020">R</w>
      </p>
      <p xml:id="A32559-e50">
       <w lemma="n/a" pos="ffr" xml:id="A32559-001-a-0030">DIEV</w>
       <w lemma="n/a" pos="ffr" xml:id="A32559-001-a-0040">ET</w>
       <w lemma="n/a" pos="ffr" xml:id="A32559-001-a-0050">MON</w>
       <w lemma="n/a" pos="ffr" xml:id="A32559-001-a-0060">DROIT</w>
      </p>
      <p xml:id="A32559-e60">
       <w lemma="n/a" pos="ffr" xml:id="A32559-001-a-0070">HONI</w>
       <w lemma="n/a" pos="ffr" xml:id="A32559-001-a-0080">SOIT</w>
       <w lemma="n/a" pos="ffr" xml:id="A32559-001-a-0090">QVI</w>
       <w lemma="n/a" pos="ffr" xml:id="A32559-001-a-0100">MAL</w>
       <w lemma="n/a" pos="ffr" xml:id="A32559-001-a-0110">Y</w>
       <w lemma="n/a" pos="ffr" xml:id="A32559-001-a-0120">PENSE</w>
      </p>
      <figDesc xml:id="A32559-e70">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <byline xml:id="A32559-e80">
     <w lemma="by" pos="acp" xml:id="A32559-001-a-0130">By</w>
     <w lemma="the" pos="d" xml:id="A32559-001-a-0140">the</w>
     <w lemma="King" pos="n1" xml:id="A32559-001-a-0150">King</w>
     <pc unit="sentence" xml:id="A32559-001-a-0151">.</pc>
    </byline>
    <head xml:id="A32559-e90">
     <w lemma="a" pos="d" xml:id="A32559-001-a-0170">A</w>
     <w lemma="proclamation" pos="n1" xml:id="A32559-001-a-0180">PROCLAMATION</w>
     <w lemma="for" pos="acp" xml:id="A32559-001-a-0190">For</w>
     <w lemma="the" pos="d" xml:id="A32559-001-a-0200">the</w>
     <w lemma="free" pos="j" xml:id="A32559-001-a-0210">Free</w>
     <w lemma="exportation" pos="n1" xml:id="A32559-001-a-0220">Exportation</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-0230">of</w>
     <w lemma="woollen" pos="j" xml:id="A32559-001-a-0240">Woollen</w>
     <w lemma="manufacture" pos="n2" xml:id="A32559-001-a-0250">Manufactures</w>
     <w lemma="until" pos="acp" xml:id="A32559-001-a-0260">until</w>
     <w lemma="the" pos="d" xml:id="A32559-001-a-0270">the</w>
     <w lemma="25" orig="25ᵗʰ" pos="ord" xml:id="A32559-001-a-0280">25th</w>
     <w lemma="day" pos="n1" xml:id="A32559-001-a-0300">day</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-0310">of</w>
     <w lemma="December" pos="nn1" rend="hi" xml:id="A32559-001-a-0320">December</w>
     <w lemma="next" pos="ord" xml:id="A32559-001-a-0330">next</w>
     <pc unit="sentence" xml:id="A32559-001-a-0340">.</pc>
    </head>
    <opener xml:id="A32559-e120">
     <signed xml:id="A32559-e130">
      <w lemma="CHARLES" pos="nn1" xml:id="A32559-001-a-0350">CHARLES</w>
      <w lemma="r." pos="ab" xml:id="A32559-001-a-0360">R.</w>
      <pc unit="sentence" xml:id="A32559-001-a-0370"/>
     </signed>
    </opener>
    <p xml:id="A32559-e140">
     <w lemma="the" pos="d" rend="decorinit" xml:id="A32559-001-a-0380">THe</w>
     <w lemma="king" pos="ng1" reg="King's" xml:id="A32559-001-a-0390">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A32559-001-a-0400">most</w>
     <w lemma="excellent" pos="j" xml:id="A32559-001-a-0410">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A32559-001-a-0420">Majesty</w>
     <pc xml:id="A32559-001-a-0430">,</pc>
     <w lemma="take" pos="vvg" xml:id="A32559-001-a-0440">taking</w>
     <w lemma="into" pos="acp" xml:id="A32559-001-a-0450">into</w>
     <w lemma="his" pos="po" xml:id="A32559-001-a-0460">His</w>
     <w lemma="princely" pos="j" xml:id="A32559-001-a-0470">Princely</w>
     <w lemma="consideration" pos="n1" xml:id="A32559-001-a-0480">consideration</w>
     <pc xml:id="A32559-001-a-0490">,</pc>
     <w lemma="the" pos="d" xml:id="A32559-001-a-0500">the</w>
     <w lemma="deadness" pos="n1" xml:id="A32559-001-a-0510">deadness</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-0520">of</w>
     <w lemma="trade" pos="n1" xml:id="A32559-001-a-0530">Trade</w>
     <w lemma="in" pos="acp" xml:id="A32559-001-a-0540">in</w>
     <w lemma="cloth" pos="n1" reg="Cloth" xml:id="A32559-001-a-0550">Cloath</w>
     <pc xml:id="A32559-001-a-0560">,</pc>
     <w lemma="and" pos="cc" xml:id="A32559-001-a-0570">and</w>
     <w lemma="other" pos="d" xml:id="A32559-001-a-0580">other</w>
     <w lemma="woollen" pos="j" xml:id="A32559-001-a-0590">Woollen</w>
     <w lemma="manufacture" pos="n2" xml:id="A32559-001-a-0600">Manufactures</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-0610">of</w>
     <w lemma="this" pos="d" xml:id="A32559-001-a-0620">this</w>
     <w lemma="kingdom" pos="n1" xml:id="A32559-001-a-0630">Kingdom</w>
     <pc xml:id="A32559-001-a-0640">,</pc>
     <w lemma="by" pos="acp" xml:id="A32559-001-a-0650">by</w>
     <w lemma="reason" pos="n1" xml:id="A32559-001-a-0660">reason</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-0670">of</w>
     <w lemma="the" pos="d" xml:id="A32559-001-a-0680">the</w>
     <w lemma="present" pos="j" xml:id="A32559-001-a-0690">present</w>
     <w lemma="war" pos="n1" xml:id="A32559-001-a-0700">War</w>
     <pc xml:id="A32559-001-a-0710">,</pc>
     <w lemma="and" pos="cc" xml:id="A32559-001-a-0720">and</w>
     <w lemma="late" pos="av-j" xml:id="A32559-001-a-0730">late</w>
     <w lemma="dreadful" pos="j" xml:id="A32559-001-a-0740">dreadful</w>
     <w lemma="contagion" pos="n1" xml:id="A32559-001-a-0750">Contagion</w>
     <pc xml:id="A32559-001-a-0760">,</pc>
     <w lemma="and" pos="cc" xml:id="A32559-001-a-0770">and</w>
     <w lemma="that" pos="cs" xml:id="A32559-001-a-0780">that</w>
     <w lemma="great" pos="j" xml:id="A32559-001-a-0790">great</w>
     <w lemma="quantity" pos="n2" xml:id="A32559-001-a-0800">quantities</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-0810">of</w>
     <w lemma="woollen" pos="j" xml:id="A32559-001-a-0820">Woollen</w>
     <w lemma="clothes" pos="n2" xml:id="A32559-001-a-0830">Clothes</w>
     <w lemma="do" pos="vvb" xml:id="A32559-001-a-0840">do</w>
     <w lemma="at" pos="acp" xml:id="A32559-001-a-0850">at</w>
     <w lemma="present" pos="j" xml:id="A32559-001-a-0860">present</w>
     <w lemma="lie" pos="n1" xml:id="A32559-001-a-0870">lie</w>
     <w lemma="upon" pos="acp" xml:id="A32559-001-a-0880">upon</w>
     <w lemma="the" pos="d" xml:id="A32559-001-a-0890">the</w>
     <w lemma="hand" pos="n2" xml:id="A32559-001-a-0900">hands</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-0910">of</w>
     <w lemma="many" pos="d" xml:id="A32559-001-a-0920">many</w>
     <w lemma="poor" pos="j" xml:id="A32559-001-a-0930">poor</w>
     <w lemma="clothier" pos="n2" xml:id="A32559-001-a-0940">Clothiers</w>
     <pc xml:id="A32559-001-a-0950">;</pc>
     <w lemma="his" pos="po" xml:id="A32559-001-a-0960">His</w>
     <w lemma="majesty" pos="n1" xml:id="A32559-001-a-0970">Majesty</w>
     <w lemma="therefore" pos="av" xml:id="A32559-001-a-0980">therefore</w>
     <w lemma="out" pos="av" xml:id="A32559-001-a-0990">out</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-1000">of</w>
     <w lemma="his" pos="po" xml:id="A32559-001-a-1010">His</w>
     <w lemma="clemency" pos="n1" xml:id="A32559-001-a-1020">Clemency</w>
     <w lemma="and" pos="cc" xml:id="A32559-001-a-1030">and</w>
     <w lemma="tender" pos="j" xml:id="A32559-001-a-1040">tender</w>
     <w lemma="compassion" pos="n1" xml:id="A32559-001-a-1050">Compassion</w>
     <w lemma="to" pos="acp" xml:id="A32559-001-a-1060">to</w>
     <w lemma="the" pos="d" xml:id="A32559-001-a-1070">the</w>
     <w lemma="necessity" pos="n2" xml:id="A32559-001-a-1080">necessities</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-1090">of</w>
     <w lemma="his" pos="po" xml:id="A32559-001-a-1100">His</w>
     <w lemma="subject" pos="n2" xml:id="A32559-001-a-1110">Subjects</w>
     <pc xml:id="A32559-001-a-1120">,</pc>
     <w lemma="do" pos="vvz" xml:id="A32559-001-a-1130">doth</w>
     <w lemma="by" pos="acp" xml:id="A32559-001-a-1140">by</w>
     <w lemma="the" pos="d" xml:id="A32559-001-a-1150">the</w>
     <w lemma="advice" pos="n1" xml:id="A32559-001-a-1160">Advice</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-1170">of</w>
     <w lemma="his" pos="po" xml:id="A32559-001-a-1180">His</w>
     <w lemma="privy" pos="j" xml:id="A32559-001-a-1190">Privy</w>
     <w lemma="council" pos="n1" xml:id="A32559-001-a-1200">Council</w>
     <pc xml:id="A32559-001-a-1210">,</pc>
     <w lemma="and" pos="cc" xml:id="A32559-001-a-1220">and</w>
     <w lemma="with" pos="acp" xml:id="A32559-001-a-1230">with</w>
     <w lemma="the" pos="d" xml:id="A32559-001-a-1240">the</w>
     <w lemma="free" pos="j" xml:id="A32559-001-a-1250">free</w>
     <w lemma="consent" pos="n1" xml:id="A32559-001-a-1260">consent</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-1270">of</w>
     <w lemma="the" pos="d" xml:id="A32559-001-a-1280">the</w>
     <w lemma="company" pos="n1" xml:id="A32559-001-a-1290">Company</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-1300">of</w>
     <w lemma="merchant-adventurer" pos="n2" xml:id="A32559-001-a-1310">Merchant-Adventurers</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-1320">of</w>
     <hi xml:id="A32559-e150">
      <w lemma="England" pos="nn1" xml:id="A32559-001-a-1330">England</w>
      <pc xml:id="A32559-001-a-1340">,</pc>
     </hi>
     <w lemma="hereby" pos="av" xml:id="A32559-001-a-1350">hereby</w>
     <w lemma="give" pos="vvb" xml:id="A32559-001-a-1360">give</w>
     <w lemma="free" pos="j" xml:id="A32559-001-a-1370">free</w>
     <w lemma="liberty" pos="n1" xml:id="A32559-001-a-1380">Liberty</w>
     <w lemma="and" pos="cc" xml:id="A32559-001-a-1390">and</w>
     <w lemma="licence" pos="n1" xml:id="A32559-001-a-1400">Licence</w>
     <w lemma="to" pos="acp" xml:id="A32559-001-a-1410">to</w>
     <w lemma="all" pos="d" xml:id="A32559-001-a-1420">all</w>
     <w lemma="and" pos="cc" xml:id="A32559-001-a-1430">and</w>
     <w lemma="every" pos="d" xml:id="A32559-001-a-1440">every</w>
     <w lemma="person" pos="n1" xml:id="A32559-001-a-1450">person</w>
     <w lemma="and" pos="cc" xml:id="A32559-001-a-1460">and</w>
     <w lemma="person" pos="n2" xml:id="A32559-001-a-1470">persons</w>
     <w lemma="whatsoever" pos="crq" xml:id="A32559-001-a-1480">whatsoever</w>
     <pc xml:id="A32559-001-a-1490">,</pc>
     <w lemma="as" pos="acp" xml:id="A32559-001-a-1500">as</w>
     <w lemma="well" pos="av" xml:id="A32559-001-a-1510">well</w>
     <w lemma="native" pos="n2-j" xml:id="A32559-001-a-1520">Natives</w>
     <w lemma="and" pos="cc" xml:id="A32559-001-a-1530">and</w>
     <w lemma="denizen" pos="n2" xml:id="A32559-001-a-1540">Denizens</w>
     <pc xml:id="A32559-001-a-1550">,</pc>
     <w lemma="as" pos="acp" xml:id="A32559-001-a-1560">as</w>
     <w lemma="stranger" pos="n2" xml:id="A32559-001-a-1570">Strangers</w>
     <w lemma="and" pos="cc" xml:id="A32559-001-a-1580">and</w>
     <w lemma="foreiner" pos="n2" xml:id="A32559-001-a-1590">Foreiners</w>
     <pc xml:id="A32559-001-a-1600">,</pc>
     <w lemma="from" pos="acp" xml:id="A32559-001-a-1610">from</w>
     <w lemma="the" pos="d" xml:id="A32559-001-a-1620">the</w>
     <w lemma="day" pos="n1" xml:id="A32559-001-a-1630">day</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-1640">of</w>
     <w lemma="the" pos="d" xml:id="A32559-001-a-1650">the</w>
     <w lemma="date" pos="n1" xml:id="A32559-001-a-1660">Date</w>
     <w lemma="hereof" pos="av" xml:id="A32559-001-a-1670">hereof</w>
     <pc xml:id="A32559-001-a-1680">,</pc>
     <w lemma="until" pos="acp" xml:id="A32559-001-a-1690">until</w>
     <w lemma="the" pos="d" xml:id="A32559-001-a-1700">the</w>
     <w lemma="five" pos="crd" xml:id="A32559-001-a-1710">Five</w>
     <w lemma="and" pos="cc" xml:id="A32559-001-a-1720">and</w>
     <w lemma="twenty" pos="ord" xml:id="A32559-001-a-1730">twentieth</w>
     <w lemma="day" pos="n1" xml:id="A32559-001-a-1740">day</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-1750">of</w>
     <w lemma="December" pos="nn1" rend="hi" xml:id="A32559-001-a-1760">December</w>
     <w lemma="next" pos="ord" xml:id="A32559-001-a-1770">next</w>
     <pc xml:id="A32559-001-a-1780">,</pc>
     <w lemma="to" pos="prt" xml:id="A32559-001-a-1790">to</w>
     <w lemma="transport" pos="vvi" xml:id="A32559-001-a-1800">Transport</w>
     <w lemma="and" pos="cc" xml:id="A32559-001-a-1810">and</w>
     <w lemma="carry" pos="vvi" xml:id="A32559-001-a-1820">Carry</w>
     <w lemma="out" pos="av" xml:id="A32559-001-a-1830">out</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-1840">of</w>
     <w lemma="this" pos="d" xml:id="A32559-001-a-1850">this</w>
     <w lemma="kingdom" pos="n1" xml:id="A32559-001-a-1860">Kingdom</w>
     <pc xml:id="A32559-001-a-1870">,</pc>
     <w lemma="all" pos="d" xml:id="A32559-001-a-1880">all</w>
     <w lemma="woollen" pos="j" xml:id="A32559-001-a-1890">Woollen</w>
     <w lemma="manufacture" pos="n2" xml:id="A32559-001-a-1900">Manufactures</w>
     <w lemma="whatsoever" pos="crq" xml:id="A32559-001-a-1910">whatsoever</w>
     <pc xml:id="A32559-001-a-1920">,</pc>
     <w lemma="to" pos="acp" xml:id="A32559-001-a-1930">to</w>
     <w lemma="any" pos="d" xml:id="A32559-001-a-1940">any</w>
     <w lemma="port" pos="n1" xml:id="A32559-001-a-1950">Port</w>
     <w lemma="or" pos="cc" xml:id="A32559-001-a-1960">or</w>
     <w lemma="place" pos="n1" xml:id="A32559-001-a-1970">place</w>
     <w lemma="beyond" pos="acp" xml:id="A32559-001-a-1980">beyond</w>
     <w lemma="the" pos="d" xml:id="A32559-001-a-1990">the</w>
     <w lemma="sea" pos="n2" xml:id="A32559-001-a-2000">Seas</w>
     <pc xml:id="A32559-001-a-2010">,</pc>
     <w lemma="lie" pos="vvg" xml:id="A32559-001-a-2020">lying</w>
     <w lemma="within" pos="acp" xml:id="A32559-001-a-2030">within</w>
     <w lemma="the" pos="d" xml:id="A32559-001-a-2040">the</w>
     <w lemma="limit" pos="n2" xml:id="A32559-001-a-2050">Limits</w>
     <w lemma="and" pos="cc" xml:id="A32559-001-a-2060">and</w>
     <w lemma="bound" pos="n2" xml:id="A32559-001-a-2070">Bounds</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-2080">of</w>
     <w lemma="the" pos="d" xml:id="A32559-001-a-2090">the</w>
     <w lemma="say" pos="j-vn" xml:id="A32559-001-a-2100">said</w>
     <w lemma="merchant-adventurer" pos="n2" xml:id="A32559-001-a-2110">Merchant-Adventurers</w>
     <w lemma="patent" pos="n1" xml:id="A32559-001-a-2120">Patent</w>
     <pc unit="sentence" xml:id="A32559-001-a-2130">.</pc>
     <pc join="right" xml:id="A32559-001-a-2140">(</pc>
     <w lemma="except" pos="acp" xml:id="A32559-001-a-2150">Except</w>
     <w lemma="the" pos="d" xml:id="A32559-001-a-2160">the</w>
     <w lemma="mart" pos="n1" xml:id="A32559-001-a-2170">Mart</w>
     <w lemma="town" pos="n2" xml:id="A32559-001-a-2180">Towns</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-2190">of</w>
     <w lemma="Dort" pos="nn1" rend="hi" xml:id="A32559-001-a-2200">Dort</w>
     <w lemma="and" pos="cc" xml:id="A32559-001-a-2210">and</w>
     <w lemma="hamburgh" pos="nn1" rend="hi" xml:id="A32559-001-a-2220">Hamburgh</w>
     <pc xml:id="A32559-001-a-2230">)</pc>
     <w lemma="yet" pos="av" xml:id="A32559-001-a-2240">Yet</w>
     <w lemma="his" pos="po" xml:id="A32559-001-a-2250">His</w>
     <w lemma="majesty" pos="n1" xml:id="A32559-001-a-2260">Majesty</w>
     <w lemma="will" pos="vmd" xml:id="A32559-001-a-2270">would</w>
     <w lemma="not" pos="xx" xml:id="A32559-001-a-2280">not</w>
     <w lemma="hereby" pos="av" xml:id="A32559-001-a-2290">hereby</w>
     <w lemma="be" pos="vvi" xml:id="A32559-001-a-2300">be</w>
     <w lemma="think" pos="vvn" xml:id="A32559-001-a-2310">thought</w>
     <w lemma="to" pos="prt" xml:id="A32559-001-a-2320">to</w>
     <w lemma="have" pos="vvi" xml:id="A32559-001-a-2330">have</w>
     <w lemma="a" pos="d" xml:id="A32559-001-a-2340">a</w>
     <w lemma="light" pos="j" xml:id="A32559-001-a-2350">light</w>
     <w lemma="esteem" pos="n1" xml:id="A32559-001-a-2360">esteem</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-2370">of</w>
     <w lemma="the" pos="d" xml:id="A32559-001-a-2380">the</w>
     <w lemma="service" pos="n2" xml:id="A32559-001-a-2390">Services</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-2400">of</w>
     <w lemma="that" pos="d" xml:id="A32559-001-a-2410">that</w>
     <w lemma="company" pos="n1" xml:id="A32559-001-a-2420">Company</w>
     <w lemma="to" pos="acp" xml:id="A32559-001-a-2430">to</w>
     <w lemma="himself" pos="pr" xml:id="A32559-001-a-2440">Himself</w>
     <pc xml:id="A32559-001-a-2450">,</pc>
     <w lemma="and" pos="cc" xml:id="A32559-001-a-2460">and</w>
     <w lemma="the" pos="d" xml:id="A32559-001-a-2470">the</w>
     <w lemma="crown" pos="n1" xml:id="A32559-001-a-2480">Crown</w>
     <w lemma="in" pos="acp" xml:id="A32559-001-a-2490">in</w>
     <w lemma="former" pos="j" xml:id="A32559-001-a-2500">former</w>
     <w lemma="time" pos="n2" xml:id="A32559-001-a-2510">times</w>
     <pc xml:id="A32559-001-a-2520">,</pc>
     <w lemma="nor" pos="ccx" xml:id="A32559-001-a-2530">nor</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-2540">of</w>
     <w lemma="their" pos="po" xml:id="A32559-001-a-2550">their</w>
     <w lemma="usefulness" pos="n1" xml:id="A32559-001-a-2560">usefulness</w>
     <w lemma="towards" pos="acp" xml:id="A32559-001-a-2570">towards</w>
     <w lemma="the" pos="d" xml:id="A32559-001-a-2580">the</w>
     <w lemma="advance" pos="n1" xml:id="A32559-001-a-2590">Advance</w>
     <w lemma="and" pos="cc" xml:id="A32559-001-a-2600">and</w>
     <w lemma="increase" pos="vvi" xml:id="A32559-001-a-2610">Increase</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-2620">of</w>
     <w lemma="the" pos="d" xml:id="A32559-001-a-2630">the</w>
     <w lemma="trade" pos="n1" xml:id="A32559-001-a-2640">Trade</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-2650">of</w>
     <w lemma="this" pos="d" xml:id="A32559-001-a-2660">this</w>
     <w lemma="kingdom" pos="n1" xml:id="A32559-001-a-2670">Kingdom</w>
     <pc xml:id="A32559-001-a-2680">;</pc>
     <w lemma="nor" pos="ccx" xml:id="A32559-001-a-2690">nor</w>
     <w lemma="do" pos="vvz" xml:id="A32559-001-a-2700">doth</w>
     <w lemma="his" pos="po" xml:id="A32559-001-a-2710">His</w>
     <w lemma="majesty" pos="n1" xml:id="A32559-001-a-2720">Majesty</w>
     <w lemma="by" pos="acp" xml:id="A32559-001-a-2730">by</w>
     <w lemma="this" pos="d" xml:id="A32559-001-a-2740">this</w>
     <w lemma="temporary" pos="j" xml:id="A32559-001-a-2750">Temporary</w>
     <w lemma="dispensation" pos="n1" xml:id="A32559-001-a-2760">Dispensation</w>
     <w lemma="intend" pos="vvb" reg="intent" xml:id="A32559-001-a-2770">intend</w>
     <w lemma="to" pos="prt" xml:id="A32559-001-a-2780">to</w>
     <w lemma="lessen" pos="vvi" xml:id="A32559-001-a-2790">lessen</w>
     <w lemma="the" pos="d" xml:id="A32559-001-a-2800">the</w>
     <w lemma="authority" pos="n1" xml:id="A32559-001-a-2810">Authority</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-2820">of</w>
     <w lemma="their" pos="po" xml:id="A32559-001-a-2830">their</w>
     <w lemma="charter" pos="n1" xml:id="A32559-001-a-2840">Charter</w>
     <pc xml:id="A32559-001-a-2850">,</pc>
     <w lemma="as" pos="acp" xml:id="A32559-001-a-2860">as</w>
     <w lemma="to" pos="acp" xml:id="A32559-001-a-2870">to</w>
     <w lemma="the" pos="d" xml:id="A32559-001-a-2880">the</w>
     <w lemma="government" pos="n1" xml:id="A32559-001-a-2890">Government</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-2900">of</w>
     <w lemma="that" pos="d" xml:id="A32559-001-a-2910">that</w>
     <w lemma="society" pos="n1" xml:id="A32559-001-a-2920">Society</w>
     <pc xml:id="A32559-001-a-2930">,</pc>
     <w lemma="either" pos="av-d" xml:id="A32559-001-a-2940">either</w>
     <w lemma="at" pos="acp" xml:id="A32559-001-a-2950">at</w>
     <w lemma="home" pos="n1" xml:id="A32559-001-a-2960">home</w>
     <w lemma="or" pos="cc" xml:id="A32559-001-a-2970">or</w>
     <w lemma="abroad" pos="av" xml:id="A32559-001-a-2980">abroad</w>
     <pc unit="sentence" xml:id="A32559-001-a-2990">.</pc>
     <w lemma="and" pos="cc" xml:id="A32559-001-a-3000">And</w>
     <w lemma="his" pos="po" xml:id="A32559-001-a-3010">His</w>
     <w lemma="majesty" pos="n1" xml:id="A32559-001-a-3020">Majesty</w>
     <w lemma="do" pos="vvz" xml:id="A32559-001-a-3030">doth</w>
     <w lemma="hereby" pos="av" xml:id="A32559-001-a-3040">hereby</w>
     <w lemma="require" pos="vvi" xml:id="A32559-001-a-3050">Require</w>
     <w lemma="and" pos="cc" xml:id="A32559-001-a-3060">and</w>
     <w lemma="command" pos="vvi" xml:id="A32559-001-a-3070">Command</w>
     <pc xml:id="A32559-001-a-3080">,</pc>
     <w lemma="that" pos="cs" xml:id="A32559-001-a-3090">That</w>
     <w lemma="during" pos="acp" xml:id="A32559-001-a-3100">during</w>
     <w lemma="the" pos="d" xml:id="A32559-001-a-3110">the</w>
     <w lemma="time" pos="n1" xml:id="A32559-001-a-3120">time</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-3130">of</w>
     <w lemma="this" pos="d" xml:id="A32559-001-a-3140">this</w>
     <w lemma="licence" pos="n1" xml:id="A32559-001-a-3150">Licence</w>
     <w lemma="and" pos="cc" xml:id="A32559-001-a-3160">and</w>
     <w lemma="dispensation" pos="n1" xml:id="A32559-001-a-3170">Dispensation</w>
     <pc xml:id="A32559-001-a-3180">,</pc>
     <w lemma="due" pos="j" xml:id="A32559-001-a-3190">due</w>
     <w lemma="payment" pos="n1" xml:id="A32559-001-a-3200">Payment</w>
     <w lemma="be" pos="vvi" xml:id="A32559-001-a-3210">be</w>
     <w lemma="make" pos="vvn" xml:id="A32559-001-a-3220">made</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-3230">of</w>
     <w lemma="all" pos="d" xml:id="A32559-001-a-3240">all</w>
     <w lemma="duty" pos="n2" xml:id="A32559-001-a-3250">Duties</w>
     <w lemma="for" pos="acp" xml:id="A32559-001-a-3260">for</w>
     <w lemma="license" pos="vvg" xml:id="A32559-001-a-3270">Licensing</w>
     <w lemma="the" pos="d" xml:id="A32559-001-a-3280">the</w>
     <w lemma="exportation" pos="n1" xml:id="A32559-001-a-3290">Exportation</w>
     <w lemma="of" pos="acp" xml:id="A32559-001-a-3300">of</w>
     <w lemma="white" pos="j" xml:id="A32559-001-a-3310">White</w>
     <w lemma="clothes" pos="n2" xml:id="A32559-001-a-3320">Clothes</w>
     <pc xml:id="A32559-001-a-3330">,</pc>
     <w lemma="according" pos="j" xml:id="A32559-001-a-3340">according</w>
     <w lemma="to" pos="acp" xml:id="A32559-001-a-3350">to</w>
     <w lemma="former" pos="j" xml:id="A32559-001-a-3360">former</w>
     <w lemma="use" pos="n1" xml:id="A32559-001-a-3370">use</w>
     <w lemma="and" pos="cc" xml:id="A32559-001-a-3380">and</w>
     <w lemma="practice" pos="n1" xml:id="A32559-001-a-3390">practice</w>
     <pc unit="sentence" xml:id="A32559-001-a-3400">.</pc>
    </p>
    <closer xml:id="A32559-e190">
     <dateline xml:id="A32559-e200">
      <w lemma="give" pos="vvn" xml:id="A32559-001-a-3410">Given</w>
      <w lemma="at" pos="acp" xml:id="A32559-001-a-3420">at</w>
      <w lemma="our" pos="po" xml:id="A32559-001-a-3430">Our</w>
      <w lemma="court" pos="n1" xml:id="A32559-001-a-3440">Court</w>
      <w lemma="at" pos="acp" xml:id="A32559-001-a-3450">at</w>
      <w lemma="Whitehall" pos="nn1" rend="hi" xml:id="A32559-001-a-3460">Whitehall</w>
      <date xml:id="A32559-e220">
       <w lemma="the" pos="d" xml:id="A32559-001-a-3470">the</w>
       <w lemma="15" orig="15ᵗʰ" pos="ord" xml:id="A32559-001-a-3480">15th</w>
       <w lemma="day" pos="n1" xml:id="A32559-001-a-3500">day</w>
       <w lemma="of" pos="acp" xml:id="A32559-001-a-3510">of</w>
       <hi xml:id="A32559-e240">
        <w lemma="April" pos="nn1" xml:id="A32559-001-a-3520">April</w>
        <pc xml:id="A32559-001-a-3530">,</pc>
       </hi>
       <w lemma="1666." pos="crd" xml:id="A32559-001-a-3540">1666.</w>
       <w lemma="in" pos="acp" xml:id="A32559-001-a-3550">in</w>
       <w lemma="the" pos="d" xml:id="A32559-001-a-3560">the</w>
       <w lemma="18" orig="18ᵗʰ" pos="ord" xml:id="A32559-001-a-3570">18th</w>
       <w lemma="year" pos="n1" xml:id="A32559-001-a-3590">year</w>
       <w lemma="of" pos="acp" xml:id="A32559-001-a-3600">of</w>
       <w lemma="our" pos="po" xml:id="A32559-001-a-3610">Our</w>
       <w lemma="reign" pos="n1" xml:id="A32559-001-a-3620">Reign</w>
       <pc unit="sentence" xml:id="A32559-001-a-3630">.</pc>
      </date>
     </dateline>
     <lb xml:id="A32559-e260"/>
     <w lemma="God" pos="nn1" xml:id="A32559-001-a-3640">God</w>
     <w lemma="save" pos="vvb" xml:id="A32559-001-a-3650">save</w>
     <w lemma="the" pos="d" xml:id="A32559-001-a-3660">the</w>
     <w lemma="King" pos="n1" xml:id="A32559-001-a-3670">King</w>
     <pc unit="sentence" xml:id="A32559-001-a-3671">.</pc>
    </closer>
   </div>
  </body>
  <back xml:id="A32559-e270">
   <div type="colophon" xml:id="A32559-e280">
    <p xml:id="A32559-e290">
     <hi xml:id="A32559-e300">
      <w lemma="LONDON" pos="nn1" xml:id="A32559-001-a-3690">LONDON</w>
      <pc xml:id="A32559-001-a-3700">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A32559-001-a-3710">Printed</w>
     <w lemma="by" pos="acp" xml:id="A32559-001-a-3720">by</w>
     <hi xml:id="A32559-e310">
      <w lemma="John" pos="nn1" xml:id="A32559-001-a-3730">John</w>
      <w lemma="bill" pos="n1" xml:id="A32559-001-a-3740">Bill</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="A32559-001-a-3750">and</w>
     <hi xml:id="A32559-e320">
      <w lemma="Christopher" pos="nn1" xml:id="A32559-001-a-3760">Christopher</w>
      <w lemma="Barker" pos="nn1" xml:id="A32559-001-a-3770">Barker</w>
      <pc xml:id="A32559-001-a-3780">,</pc>
     </hi>
     <w lemma="printer" pos="n2" xml:id="A32559-001-a-3790">Printers</w>
     <w lemma="to" pos="acp" xml:id="A32559-001-a-3800">to</w>
     <w lemma="the" pos="d" xml:id="A32559-001-a-3810">the</w>
     <w lemma="king" pos="n2" xml:id="A32559-001-a-3820">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A32559-001-a-3830">most</w>
     <w lemma="excellent" pos="j" xml:id="A32559-001-a-3840">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A32559-001-a-3850">Majesty</w>
     <pc xml:id="A32559-001-a-3860">,</pc>
     <w lemma="1666." pos="crd" xml:id="A32559-001-a-3870">1666.</w>
     <pc unit="sentence" xml:id="A32559-001-a-3880"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
