<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A32497">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>By the King, a proclamation for recalling private commissions, or letters of marque</title>
    <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A32497 of text R30900 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing C3411). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A32497</idno>
    <idno type="STC">Wing C3411</idno>
    <idno type="STC">ESTC R30900</idno>
    <idno type="EEBO-CITATION">11686702</idno>
    <idno type="OCLC">ocm 11686702</idno>
    <idno type="VID">48147</idno>
    <idno type="PROQUESTGOID">2248513025</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A32497)</note>
    <note>Transcribed from: (Early English Books Online ; image set 48147)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1482:3)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>By the King, a proclamation for recalling private commissions, or letters of marque</title>
      <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
      <author>Charles II, King of England, 1630-1685.</author>
     </titleStmt>
     <extent>1 broadside.</extent>
     <publicationStmt>
      <publisher>Printed by Leonard Lichfield ... for John Bill and Christopher Barker ...,</publisher>
      <pubPlace>Oxford :</pubPlace>
      <date>1665.</date>
     </publicationStmt>
     <notesStmt>
      <note>"Given at our court at Oxford the 5th day of January, 1665, in the seventeenth year of our reign."</note>
      <note>Reproduction of original in the Cambridge University Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Great Britain -- History -- Charles II, 1660-1685.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>By the King. A proclamation for recalling private commissions, or letters of marque.</ep:title>
    <ep:author>England and Wales. Sovereign </ep:author>
    <ep:publicationYear>1665</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>492</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-07</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-07</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-08</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-08</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-09</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A32497-t">
  <body xml:id="A32497-e0">
   <div type="royal_proclamation" xml:id="A32497-e10">
    <pb facs="tcp:48147:1" xml:id="A32497-001-a"/>
    <head xml:id="A32497-e20">
     <figure xml:id="A32497-e30">
      <p xml:id="A32497-e40">
       <w lemma="c" pos="sy" xml:id="A32497-001-a-0010">C</w>
       <w lemma="r" pos="sy" xml:id="A32497-001-a-0020">R</w>
      </p>
      <p xml:id="A32497-e50">
       <w lemma="n/a" pos="ffr" xml:id="A32497-001-a-0030">HONI</w>
       <w lemma="n/a" pos="ffr" xml:id="A32497-001-a-0040">SOIT</w>
       <w lemma="n/a" pos="ffr" xml:id="A32497-001-a-0050">QVI</w>
       <w lemma="n/a" pos="ffr" xml:id="A32497-001-a-0060">MAL</w>
       <w lemma="n/a" pos="ffr" xml:id="A32497-001-a-0070">Y</w>
       <w lemma="n/a" pos="ffr" xml:id="A32497-001-a-0080">PENSE</w>
      </p>
      <figDesc xml:id="A32497-e60">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <head xml:id="A32497-e70">
     <w lemma="by" pos="acp" xml:id="A32497-001-a-0090">BY</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-0100">THE</w>
     <w lemma="king" pos="n1" xml:id="A32497-001-a-0110">KING</w>
     <pc unit="sentence" xml:id="A32497-001-a-0120">.</pc>
    </head>
    <head type="sub" xml:id="A32497-e80">
     <hi xml:id="A32497-e90">
      <w lemma="a" pos="d" xml:id="A32497-001-a-0130">A</w>
      <w lemma="proclamation" pos="n1" xml:id="A32497-001-a-0140">PROCLAMATION</w>
      <pc xml:id="A32497-001-a-0150">,</pc>
     </hi>
     <w lemma="for" pos="acp" xml:id="A32497-001-a-0160">For</w>
     <w lemma="recall" pos="vvg" xml:id="A32497-001-a-0170">recalling</w>
     <w lemma="private" pos="j" xml:id="A32497-001-a-0180">Private</w>
     <w lemma="commission" pos="n2" xml:id="A32497-001-a-0190">Commissions</w>
     <pc xml:id="A32497-001-a-0200">,</pc>
     <w lemma="or" pos="cc" xml:id="A32497-001-a-0210">or</w>
     <w lemma="letter" pos="n2" xml:id="A32497-001-a-0220">Letters</w>
     <w lemma="of" pos="acp" xml:id="A32497-001-a-0230">of</w>
     <w lemma="marque" pos="n1" xml:id="A32497-001-a-0240">Marque</w>
     <pc unit="sentence" xml:id="A32497-001-a-0250">.</pc>
    </head>
    <opener xml:id="A32497-e100">
     <signed xml:id="A32497-e110">
      <w lemma="charles" pos="nng1" reg="CHARLES'" xml:id="A32497-001-a-0260">CHARLES</w>
      <w lemma="n/a" pos="fla" xml:id="A32497-001-a-0270">REX</w>
      <pc xml:id="A32497-001-a-0280">,</pc>
     </signed>
    </opener>
    <p xml:id="A32497-e120">
     <w lemma="whereas" pos="cs" rend="decorinit" xml:id="A32497-001-a-0290">WHereas</w>
     <w lemma="by" pos="acp" xml:id="A32497-001-a-0300">by</w>
     <w lemma="his" pos="po" xml:id="A32497-001-a-0310">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A32497-001-a-0320">Majesties</w>
     <w lemma="authority" pos="n1" xml:id="A32497-001-a-0330">Authority</w>
     <pc xml:id="A32497-001-a-0340">,</pc>
     <w lemma="for" pos="acp" xml:id="A32497-001-a-0350">for</w>
     <w lemma="just" pos="j" xml:id="A32497-001-a-0360">just</w>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-0370">and</w>
     <w lemma="urgent" pos="j" xml:id="A32497-001-a-0380">urgent</w>
     <w lemma="cause" pos="n2" xml:id="A32497-001-a-0390">causes</w>
     <w lemma="he" pos="pno" xml:id="A32497-001-a-0400">Him</w>
     <w lemma="move" pos="vvg" xml:id="A32497-001-a-0410">moving</w>
     <pc xml:id="A32497-001-a-0420">,</pc>
     <w lemma="private" pos="j" xml:id="A32497-001-a-0430">private</w>
     <w lemma="commission" pos="n2" xml:id="A32497-001-a-0440">Commissions</w>
     <w lemma="or" pos="cc" xml:id="A32497-001-a-0450">or</w>
     <w lemma="letter" pos="n2" xml:id="A32497-001-a-0460">Letters</w>
     <w lemma="of" pos="acp" xml:id="A32497-001-a-0470">of</w>
     <w lemma="marque" pos="n1" xml:id="A32497-001-a-0480">Marque</w>
     <w lemma="have" pos="vvb" xml:id="A32497-001-a-0490">have</w>
     <w lemma="be" pos="vvn" xml:id="A32497-001-a-0500">been</w>
     <w lemma="grant" pos="vvn" xml:id="A32497-001-a-0510">granted</w>
     <w lemma="to" pos="acp" xml:id="A32497-001-a-0520">to</w>
     <w lemma="some" pos="d" xml:id="A32497-001-a-0530">some</w>
     <w lemma="of" pos="acp" xml:id="A32497-001-a-0540">of</w>
     <w lemma="his" pos="po" xml:id="A32497-001-a-0550">His</w>
     <w lemma="subject" pos="n2" xml:id="A32497-001-a-0560">Subjects</w>
     <pc xml:id="A32497-001-a-0570">,</pc>
     <w lemma="for" pos="acp" xml:id="A32497-001-a-0580">for</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-0590">the</w>
     <w lemma="apprehend" pos="n1-vg" xml:id="A32497-001-a-0600">apprehending</w>
     <pc xml:id="A32497-001-a-0610">,</pc>
     <w lemma="take" pos="vvg" xml:id="A32497-001-a-0620">taking</w>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-0630">and</w>
     <w lemma="seize" pos="vvg" reg="seizing" xml:id="A32497-001-a-0640">seising</w>
     <w lemma="of" pos="acp" xml:id="A32497-001-a-0650">of</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-0660">the</w>
     <w lemma="ship" pos="n2" xml:id="A32497-001-a-0670">Ships</w>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-0680">and</w>
     <w lemma="good" pos="n2-j" xml:id="A32497-001-a-0690">Goods</w>
     <w lemma="belong" pos="vvg" xml:id="A32497-001-a-0700">belonging</w>
     <w lemma="to" pos="acp" xml:id="A32497-001-a-0710">to</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-0720">the</w>
     <w lemma="state" pos="n2" xml:id="A32497-001-a-0730">States</w>
     <w lemma="of" pos="acp" xml:id="A32497-001-a-0740">of</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-0750">the</w>
     <w lemma="unite" pos="j-vn" xml:id="A32497-001-a-0760">United</w>
     <w lemma="province" pos="n2" xml:id="A32497-001-a-0770">Provinces</w>
     <pc xml:id="A32497-001-a-0780">,</pc>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-0790">and</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-0800">the</w>
     <w lemma="inhabitant" pos="n2" xml:id="A32497-001-a-0810">Inhabitants</w>
     <w lemma="there" pos="av" xml:id="A32497-001-a-0820">there</w>
     <pc xml:id="A32497-001-a-0830">,</pc>
     <w lemma="but" pos="acp" xml:id="A32497-001-a-0840">but</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-0850">the</w>
     <w lemma="same" pos="d" xml:id="A32497-001-a-0860">same</w>
     <w lemma="do" pos="vvd" xml:id="A32497-001-a-0870">did</w>
     <w lemma="not" pos="xx" xml:id="A32497-001-a-0880">not</w>
     <w lemma="extend" pos="vvi" xml:id="A32497-001-a-0890">extend</w>
     <pc xml:id="A32497-001-a-0900">,</pc>
     <w lemma="nor" pos="ccx" xml:id="A32497-001-a-0910">nor</w>
     <w lemma="be" pos="vvd" xml:id="A32497-001-a-0920">were</w>
     <w lemma="intend" pos="vvn" xml:id="A32497-001-a-0930">intended</w>
     <w lemma="to" pos="prt" xml:id="A32497-001-a-0940">to</w>
     <w lemma="be" pos="vvi" xml:id="A32497-001-a-0950">be</w>
     <w lemma="any" pos="d" xml:id="A32497-001-a-0960">any</w>
     <w lemma="way" pos="n2" xml:id="A32497-001-a-0970">ways</w>
     <w lemma="prejudicial" pos="j" xml:id="A32497-001-a-0980">prejudicial</w>
     <w lemma="to" pos="acp" xml:id="A32497-001-a-0990">to</w>
     <w lemma="his" pos="po" xml:id="A32497-001-a-1000">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A32497-001-a-1010">Majesties</w>
     <w lemma="friend" pos="n2" xml:id="A32497-001-a-1020">Friends</w>
     <w lemma="or" pos="cc" xml:id="A32497-001-a-1030">or</w>
     <w lemma="ally" pos="n2" xml:id="A32497-001-a-1040">Allies</w>
     <pc xml:id="A32497-001-a-1050">:</pc>
     <w lemma="nevertheless" pos="av" xml:id="A32497-001-a-1060">Nevertheless</w>
     <pc xml:id="A32497-001-a-1070">,</pc>
     <w lemma="his" pos="po" xml:id="A32497-001-a-1080">His</w>
     <w lemma="majesty" pos="n1" xml:id="A32497-001-a-1090">Majesty</w>
     <w lemma="have" pos="vvz" xml:id="A32497-001-a-1100">hath</w>
     <w lemma="receive" pos="vvn" xml:id="A32497-001-a-1110">received</w>
     <w lemma="divers" pos="j" xml:id="A32497-001-a-1120">divers</w>
     <w lemma="advertisement" pos="n2" xml:id="A32497-001-a-1130">Advertisements</w>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-1140">and</w>
     <w lemma="complaint" pos="n2" xml:id="A32497-001-a-1150">Complaints</w>
     <w lemma="from" pos="acp" xml:id="A32497-001-a-1160">from</w>
     <w lemma="foreign" pos="j" xml:id="A32497-001-a-1170">Foreign</w>
     <w lemma="minister" pos="n2" xml:id="A32497-001-a-1180">Ministers</w>
     <pc xml:id="A32497-001-a-1190">,</pc>
     <w lemma="that" pos="cs" xml:id="A32497-001-a-1200">That</w>
     <w lemma="by" pos="acp" xml:id="A32497-001-a-1210">by</w>
     <w lemma="colour" pos="n1" xml:id="A32497-001-a-1220">colour</w>
     <w lemma="of" pos="acp" xml:id="A32497-001-a-1230">of</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-1240">the</w>
     <w lemma="say" pos="vvd" xml:id="A32497-001-a-1250">said</w>
     <w lemma="private" pos="j" xml:id="A32497-001-a-1260">Private</w>
     <w lemma="commission" pos="n2" xml:id="A32497-001-a-1270">Commissions</w>
     <w lemma="or" pos="cc" xml:id="A32497-001-a-1280">or</w>
     <w lemma="letter" pos="n2" xml:id="A32497-001-a-1290">Letters</w>
     <w lemma="of" pos="acp" xml:id="A32497-001-a-1300">of</w>
     <w lemma="marque" pos="n1" xml:id="A32497-001-a-1310">Marque</w>
     <pc xml:id="A32497-001-a-1320">,</pc>
     <w lemma="the" pos="d" xml:id="A32497-001-a-1330">the</w>
     <w lemma="ship" pos="n2" xml:id="A32497-001-a-1340">Ships</w>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-1350">and</w>
     <w lemma="good" pos="n2-j" xml:id="A32497-001-a-1360">Goods</w>
     <w lemma="of" pos="acp" xml:id="A32497-001-a-1370">of</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-1380">the</w>
     <w lemma="subject" pos="n2" xml:id="A32497-001-a-1390">Subjects</w>
     <w lemma="of" pos="acp" xml:id="A32497-001-a-1400">of</w>
     <w lemma="his" pos="po" xml:id="A32497-001-a-1410">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A32497-001-a-1420">Majesties</w>
     <w lemma="friend" pos="n2" xml:id="A32497-001-a-1430">Friends</w>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-1440">and</w>
     <w lemma="ally" pos="n2" xml:id="A32497-001-a-1450">Allies</w>
     <pc xml:id="A32497-001-a-1460">,</pc>
     <w lemma="have" pos="vvb" xml:id="A32497-001-a-1470">have</w>
     <w lemma="be" pos="vvn" xml:id="A32497-001-a-1480">been</w>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-1490">and</w>
     <w lemma="be" pos="vvb" xml:id="A32497-001-a-1500">are</w>
     <w lemma="frequent" pos="av-j" xml:id="A32497-001-a-1510">frequently</w>
     <w lemma="take" pos="vvn" xml:id="A32497-001-a-1520">taken</w>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-1530">and</w>
     <w lemma="seize" pos="vvn" reg="seized" xml:id="A32497-001-a-1540">seised</w>
     <pc xml:id="A32497-001-a-1550">,</pc>
     <w lemma="to" pos="prt" xml:id="A32497-001-a-1560">to</w>
     <w lemma="their" pos="po" xml:id="A32497-001-a-1570">their</w>
     <w lemma="great" pos="j" xml:id="A32497-001-a-1580">great</w>
     <w lemma="vexation" pos="n1" xml:id="A32497-001-a-1590">vexation</w>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-1600">and</w>
     <w lemma="damage" pos="n1" xml:id="A32497-001-a-1610">damage</w>
     <pc xml:id="A32497-001-a-1620">;</pc>
     <w lemma="wherein" pos="crq" xml:id="A32497-001-a-1630">Wherein</w>
     <w lemma="his" pos="po" xml:id="A32497-001-a-1640">His</w>
     <w lemma="majesty" pos="n1" xml:id="A32497-001-a-1650">Majesty</w>
     <w lemma="do" pos="vvz" xml:id="A32497-001-a-1660">doth</w>
     <w lemma="not" pos="xx" xml:id="A32497-001-a-1670">not</w>
     <w lemma="deem" pos="vvi" xml:id="A32497-001-a-1680">deem</w>
     <w lemma="it" pos="pn" xml:id="A32497-001-a-1690">it</w>
     <w lemma="enough" pos="av-d" xml:id="A32497-001-a-1700">enough</w>
     <w lemma="that" pos="cs" xml:id="A32497-001-a-1710">that</w>
     <w lemma="they" pos="pns" xml:id="A32497-001-a-1720">they</w>
     <w lemma="may" pos="vmb" xml:id="A32497-001-a-1730">may</w>
     <w lemma="have" pos="vvi" xml:id="A32497-001-a-1740">have</w>
     <w lemma="reparation" pos="n1" xml:id="A32497-001-a-1750">reparation</w>
     <w lemma="in" pos="acp" xml:id="A32497-001-a-1760">in</w>
     <w lemma="ordinary" pos="j" xml:id="A32497-001-a-1770">ordinary</w>
     <w lemma="course" pos="n1" xml:id="A32497-001-a-1780">course</w>
     <w lemma="of" pos="acp" xml:id="A32497-001-a-1790">of</w>
     <w lemma="justice" pos="n1" reg="justice" xml:id="A32497-001-a-1800">Iustice</w>
     <pc xml:id="A32497-001-a-1810">,</pc>
     <w lemma="but" pos="acp" xml:id="A32497-001-a-1820">but</w>
     <w lemma="for" pos="acp" xml:id="A32497-001-a-1830">for</w>
     <w lemma="prevent" pos="vvg" xml:id="A32497-001-a-1840">preventing</w>
     <w lemma="all" pos="d" xml:id="A32497-001-a-1850">all</w>
     <w lemma="occasion" pos="n2" xml:id="A32497-001-a-1860">occasions</w>
     <w lemma="of" pos="acp" xml:id="A32497-001-a-1870">of</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-1880">the</w>
     <w lemma="like" pos="j" xml:id="A32497-001-a-1890">like</w>
     <w lemma="for" pos="acp" xml:id="A32497-001-a-1900">for</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-1910">the</w>
     <w lemma="future" pos="j" xml:id="A32497-001-a-1920">future</w>
     <pc xml:id="A32497-001-a-1930">,</pc>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-1940">and</w>
     <w lemma="to" pos="acp" xml:id="A32497-001-a-1950">to</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-1960">the</w>
     <w lemma="end" pos="n1" xml:id="A32497-001-a-1970">end</w>
     <w lemma="when" pos="crq" xml:id="A32497-001-a-1980">when</w>
     <w lemma="any" pos="d" xml:id="A32497-001-a-1990">any</w>
     <w lemma="other" pos="pi-d" xml:id="A32497-001-a-2000">other</w>
     <w lemma="shall" pos="vmb" xml:id="A32497-001-a-2010">shall</w>
     <w lemma="be" pos="vvi" xml:id="A32497-001-a-2020">be</w>
     <w lemma="hereafter" pos="av" xml:id="A32497-001-a-2030">hereafter</w>
     <w lemma="grant" pos="vvn" xml:id="A32497-001-a-2040">Granted</w>
     <pc xml:id="A32497-001-a-2050">,</pc>
     <w lemma="care" pos="n1" xml:id="A32497-001-a-2060">care</w>
     <w lemma="may" pos="vmb" xml:id="A32497-001-a-2070">may</w>
     <w lemma="be" pos="vvi" xml:id="A32497-001-a-2080">be</w>
     <w lemma="take" pos="vvn" xml:id="A32497-001-a-2090">taken</w>
     <w lemma="for" pos="acp" xml:id="A32497-001-a-2100">for</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-2110">the</w>
     <w lemma="better" pos="jc" xml:id="A32497-001-a-2120">better</w>
     <w lemma="regulation" pos="n1" xml:id="A32497-001-a-2130">regulation</w>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-2140">and</w>
     <w lemma="govern" pos="n1-vg" xml:id="A32497-001-a-2150">governing</w>
     <w lemma="of" pos="acp" xml:id="A32497-001-a-2160">of</w>
     <w lemma="they" pos="pno" xml:id="A32497-001-a-2170">them</w>
     <pc xml:id="A32497-001-a-2180">,</pc>
     <w lemma="do" pos="vvz" xml:id="A32497-001-a-2190">Doth</w>
     <w lemma="by" pos="acp" xml:id="A32497-001-a-2200">by</w>
     <w lemma="this" pos="d" xml:id="A32497-001-a-2210">this</w>
     <w lemma="his" pos="po" xml:id="A32497-001-a-2220">His</w>
     <w lemma="proclamation" pos="n1" xml:id="A32497-001-a-2230">Proclamation</w>
     <pc join="right" xml:id="A32497-001-a-2240">(</pc>
     <w lemma="by" pos="acp" xml:id="A32497-001-a-2250">by</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-2260">the</w>
     <w lemma="advise" pos="vvb" xml:id="A32497-001-a-2270">Advise</w>
     <w lemma="of" pos="acp" xml:id="A32497-001-a-2280">of</w>
     <w lemma="his" pos="po" xml:id="A32497-001-a-2290">His</w>
     <w lemma="privy" pos="j" xml:id="A32497-001-a-2300">Privy</w>
     <w lemma="council" pos="n1" reg="Council" xml:id="A32497-001-a-2310">Councel</w>
     <pc xml:id="A32497-001-a-2320">)</pc>
     <w lemma="declare" pos="vvb" xml:id="A32497-001-a-2330">Declare</w>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-2340">and</w>
     <w lemma="publish" pos="vvb" xml:id="A32497-001-a-2350">Publish</w>
     <w lemma="his" pos="po" xml:id="A32497-001-a-2360">His</w>
     <w lemma="will" pos="n1" xml:id="A32497-001-a-2370">Will</w>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-2380">and</w>
     <w lemma="pleasure" pos="n1" xml:id="A32497-001-a-2390">Pleasure</w>
     <w lemma="to" pos="prt" xml:id="A32497-001-a-2400">to</w>
     <w lemma="be" pos="vvi" xml:id="A32497-001-a-2410">be</w>
     <pc xml:id="A32497-001-a-2420">,</pc>
     <w lemma="that" pos="cs" xml:id="A32497-001-a-2430">That</w>
     <w lemma="from" pos="acp" xml:id="A32497-001-a-2440">from</w>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-2450">and</w>
     <w lemma="after" pos="acp" xml:id="A32497-001-a-2460">after</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-2470">the</w>
     <w lemma="fourteen" pos="ord" xml:id="A32497-001-a-2480">Fourteenth</w>
     <w lemma="day" pos="n1" xml:id="A32497-001-a-2490">Day</w>
     <w lemma="of" pos="acp" xml:id="A32497-001-a-2500">of</w>
     <w lemma="February" pos="nn1" rend="hi" xml:id="A32497-001-a-2510">February</w>
     <w lemma="now" pos="av" xml:id="A32497-001-a-2520">now</w>
     <w lemma="next" pos="ord" xml:id="A32497-001-a-2530">next</w>
     <w lemma="come" pos="n1-vg" xml:id="A32497-001-a-2540">coming</w>
     <pc xml:id="A32497-001-a-2550">,</pc>
     <w lemma="all" pos="d" xml:id="A32497-001-a-2560">All</w>
     <w lemma="private" pos="j" xml:id="A32497-001-a-2570">Private</w>
     <w lemma="commission" pos="n2" xml:id="A32497-001-a-2580">Commissions</w>
     <w lemma="or" pos="cc" xml:id="A32497-001-a-2590">or</w>
     <w lemma="letter" pos="n2" xml:id="A32497-001-a-2600">Letters</w>
     <w lemma="of" pos="acp" xml:id="A32497-001-a-2610">of</w>
     <w lemma="marque" pos="n1" xml:id="A32497-001-a-2620">Marque</w>
     <w lemma="whatsoever" pos="crq" xml:id="A32497-001-a-2630">whatsoever</w>
     <pc xml:id="A32497-001-a-2640">,</pc>
     <w lemma="grant" pos="vvn" xml:id="A32497-001-a-2650">granted</w>
     <w lemma="to" pos="acp" xml:id="A32497-001-a-2660">to</w>
     <w lemma="any" pos="d" xml:id="A32497-001-a-2670">any</w>
     <w lemma="who" pos="crq" xml:id="A32497-001-a-2680">who</w>
     <w lemma="have" pos="vvb" xml:id="A32497-001-a-2690">have</w>
     <w lemma="thereupon" pos="av" xml:id="A32497-001-a-2700">thereupon</w>
     <w lemma="set" pos="vvn" xml:id="A32497-001-a-2710">set</w>
     <w lemma="forth" pos="av" xml:id="A32497-001-a-2720">forth</w>
     <w lemma="any" pos="d" xml:id="A32497-001-a-2730">any</w>
     <w lemma="ship" pos="n1" xml:id="A32497-001-a-2740">Ship</w>
     <w lemma="or" pos="cc" xml:id="A32497-001-a-2750">or</w>
     <w lemma="vessel" pos="n1" xml:id="A32497-001-a-2760">Vessel</w>
     <w lemma="out" pos="av" xml:id="A32497-001-a-2770">out</w>
     <w lemma="of" pos="acp" xml:id="A32497-001-a-2780">of</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-2790">the</w>
     <w lemma="realm" pos="n2" xml:id="A32497-001-a-2800">Realms</w>
     <w lemma="of" pos="acp" xml:id="A32497-001-a-2810">of</w>
     <w lemma="England" pos="nn1" rend="hi" xml:id="A32497-001-a-2820">England</w>
     <w lemma="or" pos="cc" xml:id="A32497-001-a-2830">or</w>
     <hi xml:id="A32497-e150">
      <w lemma="Ireland" pos="nn1" xml:id="A32497-001-a-2840">Ireland</w>
      <pc xml:id="A32497-001-a-2850">,</pc>
     </hi>
     <w lemma="shall" pos="vmb" xml:id="A32497-001-a-2860">shall</w>
     <w lemma="cease" pos="vvi" xml:id="A32497-001-a-2870">cease</w>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-2880">and</w>
     <w lemma="determine" pos="vvi" xml:id="A32497-001-a-2890">determine</w>
     <pc xml:id="A32497-001-a-2900">,</pc>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-2910">and</w>
     <w lemma="by" pos="acp" xml:id="A32497-001-a-2920">by</w>
     <w lemma="that" pos="d" xml:id="A32497-001-a-2930">that</w>
     <w lemma="time" pos="n1" xml:id="A32497-001-a-2940">time</w>
     <w lemma="be" pos="vvb" xml:id="A32497-001-a-2950">be</w>
     <w lemma="deliver" pos="vvn" xml:id="A32497-001-a-2960">delivered</w>
     <w lemma="up" pos="acp" xml:id="A32497-001-a-2970">up</w>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-2980">and</w>
     <w lemma="vacate" pos="vvn" xml:id="A32497-001-a-2990">vacated</w>
     <pc xml:id="A32497-001-a-3000">,</pc>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-3010">and</w>
     <w lemma="from" pos="acp" xml:id="A32497-001-a-3020">from</w>
     <w lemma="thenceforth" pos="av" xml:id="A32497-001-a-3030">thenceforth</w>
     <w lemma="his" pos="po" xml:id="A32497-001-a-3040">His</w>
     <w lemma="majesty" pos="n1" xml:id="A32497-001-a-3050">Majesty</w>
     <w lemma="do" pos="vvz" xml:id="A32497-001-a-3060">doth</w>
     <w lemma="hereby" pos="av" xml:id="A32497-001-a-3070">hereby</w>
     <w lemma="recall" pos="vvi" xml:id="A32497-001-a-3080">recall</w>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-3090">and</w>
     <w lemma="revoke" pos="vvi" xml:id="A32497-001-a-3100">revoke</w>
     <pc xml:id="A32497-001-a-3110">,</pc>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-3120">and</w>
     <w lemma="declare" pos="vvi" xml:id="A32497-001-a-3130">declare</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-3140">the</w>
     <w lemma="same" pos="d" xml:id="A32497-001-a-3150">same</w>
     <w lemma="to" pos="prt" xml:id="A32497-001-a-3160">to</w>
     <w lemma="be" pos="vvi" xml:id="A32497-001-a-3170">be</w>
     <w lemma="void" pos="j" xml:id="A32497-001-a-3180">void</w>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-3190">and</w>
     <w lemma="determine" pos="vvn" xml:id="A32497-001-a-3200">determined</w>
     <w lemma="to" pos="acp" xml:id="A32497-001-a-3210">to</w>
     <w lemma="all" pos="d" xml:id="A32497-001-a-3220">all</w>
     <w lemma="intent" pos="n2" xml:id="A32497-001-a-3230">intents</w>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-3240">and</w>
     <w lemma="purpose" pos="n2" xml:id="A32497-001-a-3250">purposes</w>
     <pc unit="sentence" xml:id="A32497-001-a-3260">.</pc>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-3270">And</w>
     <w lemma="his" pos="po" xml:id="A32497-001-a-3280">His</w>
     <w lemma="majesty" pos="n1" xml:id="A32497-001-a-3290">Majesty</w>
     <w lemma="do" pos="vvz" xml:id="A32497-001-a-3300">doth</w>
     <w lemma="straight" pos="av-j" xml:id="A32497-001-a-3310">straightly</w>
     <w lemma="charge" pos="n1" xml:id="A32497-001-a-3320">Charge</w>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-3330">and</w>
     <w lemma="command" pos="n1" xml:id="A32497-001-a-3340">Command</w>
     <pc xml:id="A32497-001-a-3350">,</pc>
     <w lemma="that" pos="cs" xml:id="A32497-001-a-3360">That</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-3370">the</w>
     <w lemma="say" pos="j-vn" xml:id="A32497-001-a-3380">said</w>
     <w lemma="commission" pos="n2" xml:id="A32497-001-a-3390">Commissions</w>
     <w lemma="or" pos="cc" xml:id="A32497-001-a-3400">or</w>
     <w lemma="letter" pos="n2" xml:id="A32497-001-a-3410">Letters</w>
     <w lemma="of" pos="acp" xml:id="A32497-001-a-3420">of</w>
     <w lemma="marque" pos="n1" xml:id="A32497-001-a-3430">Marque</w>
     <pc xml:id="A32497-001-a-3440">,</pc>
     <w lemma="be" pos="vvb" xml:id="A32497-001-a-3450">be</w>
     <w lemma="bring" pos="vvn" xml:id="A32497-001-a-3460">brought</w>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-3470">and</w>
     <w lemma="deliver" pos="vvn" xml:id="A32497-001-a-3480">delivered</w>
     <w lemma="into" pos="acp" xml:id="A32497-001-a-3490">into</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-3500">the</w>
     <w lemma="respective" pos="j" xml:id="A32497-001-a-3510">respective</w>
     <w lemma="court" pos="n2" xml:id="A32497-001-a-3520">Courts</w>
     <w lemma="of" pos="acp" xml:id="A32497-001-a-3530">of</w>
     <w lemma="admiralty" pos="n1" xml:id="A32497-001-a-3540">Admiralty</w>
     <pc xml:id="A32497-001-a-3550">,</pc>
     <w lemma="from" pos="acp" xml:id="A32497-001-a-3560">from</w>
     <w lemma="whence" pos="crq" xml:id="A32497-001-a-3570">whence</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-3580">the</w>
     <w lemma="same" pos="d" xml:id="A32497-001-a-3590">same</w>
     <w lemma="issue" pos="vvd" xml:id="A32497-001-a-3600">issued</w>
     <pc xml:id="A32497-001-a-3610">,</pc>
     <w lemma="by" pos="acp" xml:id="A32497-001-a-3620">by</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-3630">the</w>
     <w lemma="time" pos="n1" xml:id="A32497-001-a-3640">time</w>
     <w lemma="aforesaid" pos="j" xml:id="A32497-001-a-3650">aforesaid</w>
     <pc xml:id="A32497-001-a-3660">,</pc>
     <w lemma="there" pos="av" xml:id="A32497-001-a-3670">there</w>
     <w lemma="to" pos="prt" xml:id="A32497-001-a-3680">to</w>
     <w lemma="be" pos="vvi" xml:id="A32497-001-a-3690">be</w>
     <w lemma="vacate" pos="vvn" xml:id="A32497-001-a-3700">vacated</w>
     <pc xml:id="A32497-001-a-3710">:</pc>
     <w lemma="hereby" pos="av" xml:id="A32497-001-a-3720">Hereby</w>
     <w lemma="further" pos="av-j" xml:id="A32497-001-a-3730">further</w>
     <w lemma="declare" pos="vvg" xml:id="A32497-001-a-3740">Declaring</w>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-3750">and</w>
     <w lemma="publish" pos="vvg" xml:id="A32497-001-a-3760">Publishing</w>
     <pc xml:id="A32497-001-a-3770">,</pc>
     <w lemma="that" pos="cs" xml:id="A32497-001-a-3780">That</w>
     <w lemma="from" pos="acp" xml:id="A32497-001-a-3790">from</w>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-3800">and</w>
     <w lemma="after" pos="acp" xml:id="A32497-001-a-3810">after</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-3820">the</w>
     <w lemma="say" pos="vvd" xml:id="A32497-001-a-3830">said</w>
     <w lemma="fourteen" pos="ord" xml:id="A32497-001-a-3840">Fourteenth</w>
     <w lemma="day" pos="n1" xml:id="A32497-001-a-3850">day</w>
     <w lemma="of" pos="acp" xml:id="A32497-001-a-3860">of</w>
     <hi xml:id="A32497-e160">
      <w lemma="February" pos="nn1" xml:id="A32497-001-a-3870">February</w>
      <pc xml:id="A32497-001-a-3880">,</pc>
     </hi>
     <w lemma="all" pos="d" xml:id="A32497-001-a-3890">all</w>
     <w lemma="act" pos="n2-vg" xml:id="A32497-001-a-3900">Actings</w>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-3910">and</w>
     <w lemma="proceed" pos="n2-vg" xml:id="A32497-001-a-3920">Proceedings</w>
     <w lemma="by" pos="acp" xml:id="A32497-001-a-3930">by</w>
     <w lemma="pretext" pos="n1" xml:id="A32497-001-a-3940">pretext</w>
     <w lemma="of" pos="acp" xml:id="A32497-001-a-3950">of</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-3960">the</w>
     <w lemma="say" pos="j-vn" xml:id="A32497-001-a-3970">said</w>
     <w lemma="commission" pos="n2" xml:id="A32497-001-a-3980">Commissions</w>
     <w lemma="or" pos="cc" xml:id="A32497-001-a-3990">or</w>
     <w lemma="letter" pos="n2" xml:id="A32497-001-a-4000">Letters</w>
     <w lemma="of" pos="acp" xml:id="A32497-001-a-4010">of</w>
     <w lemma="marque" pos="n1" xml:id="A32497-001-a-4020">Marque</w>
     <pc xml:id="A32497-001-a-4030">,</pc>
     <w lemma="shall" pos="vmb" xml:id="A32497-001-a-4040">shall</w>
     <w lemma="be" pos="vvi" xml:id="A32497-001-a-4050">be</w>
     <w lemma="take" pos="vvn" xml:id="A32497-001-a-4060">taken</w>
     <w lemma="to" pos="prt" xml:id="A32497-001-a-4070">to</w>
     <w lemma="be" pos="vvi" xml:id="A32497-001-a-4080">be</w>
     <w lemma="as" pos="acp" xml:id="A32497-001-a-4090">as</w>
     <w lemma="without" pos="acp" xml:id="A32497-001-a-4100">without</w>
     <w lemma="any" pos="d" xml:id="A32497-001-a-4110">any</w>
     <w lemma="warrant" pos="n1" xml:id="A32497-001-a-4120">warrant</w>
     <w lemma="or" pos="cc" xml:id="A32497-001-a-4130">or</w>
     <w lemma="authority" pos="n1" xml:id="A32497-001-a-4140">authority</w>
     <pc xml:id="A32497-001-a-4150">,</pc>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-4160">and</w>
     <w lemma="at" pos="acp" xml:id="A32497-001-a-4170">at</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-4180">the</w>
     <w lemma="peril" pos="n2" xml:id="A32497-001-a-4190">perils</w>
     <w lemma="of" pos="acp" xml:id="A32497-001-a-4200">of</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-4210">the</w>
     <w lemma="party" pos="n2" xml:id="A32497-001-a-4220">parties</w>
     <w lemma="who" pos="crq" xml:id="A32497-001-a-4230">who</w>
     <w lemma="shall" pos="vmb" xml:id="A32497-001-a-4240">shall</w>
     <w lemma="act" pos="vvi" xml:id="A32497-001-a-4250">act</w>
     <w lemma="by" pos="acp" xml:id="A32497-001-a-4260">by</w>
     <w lemma="colour" pos="n1" xml:id="A32497-001-a-4270">colour</w>
     <w lemma="thereof" pos="av" xml:id="A32497-001-a-4280">thereof</w>
     <pc xml:id="A32497-001-a-4290">,</pc>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-4300">and</w>
     <w lemma="as" pos="acp" xml:id="A32497-001-a-4310">as</w>
     <w lemma="if" pos="cs" xml:id="A32497-001-a-4320">if</w>
     <w lemma="no" pos="dx" xml:id="A32497-001-a-4330">no</w>
     <w lemma="such" pos="d" xml:id="A32497-001-a-4340">such</w>
     <w lemma="commission" pos="n2" xml:id="A32497-001-a-4350">Commissions</w>
     <w lemma="have" pos="vvd" xml:id="A32497-001-a-4360">had</w>
     <w lemma="be" pos="vvn" xml:id="A32497-001-a-4370">been</w>
     <pc unit="sentence" xml:id="A32497-001-a-4380">.</pc>
    </p>
    <closer xml:id="A32497-e170">
     <dateline xml:id="A32497-e180">
      <w lemma="give" pos="vvn" xml:id="A32497-001-a-4390">Given</w>
      <w lemma="at" pos="acp" xml:id="A32497-001-a-4400">at</w>
      <w lemma="our" pos="po" xml:id="A32497-001-a-4410">Our</w>
      <w lemma="court" pos="n1" xml:id="A32497-001-a-4420">Court</w>
      <w lemma="at" pos="acp" xml:id="A32497-001-a-4430">at</w>
      <w lemma="Oxford" pos="nn1" xml:id="A32497-001-a-4440">Oxford</w>
      <date xml:id="A32497-e190">
       <w lemma="the" pos="d" xml:id="A32497-001-a-4450">the</w>
       <w lemma="5" pos="ord" xml:id="A32497-001-a-4460">5th</w>
       <w lemma="day" pos="n1" xml:id="A32497-001-a-4470">Day</w>
       <w lemma="of" pos="acp" xml:id="A32497-001-a-4480">of</w>
       <w lemma="January" pos="nn1" xml:id="A32497-001-a-4490">January</w>
       <pc xml:id="A32497-001-a-4500">,</pc>
       <w lemma="1665." pos="crd" xml:id="A32497-001-a-4510">1665.</w>
       <w lemma="in" pos="acp" xml:id="A32497-001-a-4520">in</w>
       <w lemma="the" pos="d" xml:id="A32497-001-a-4530">the</w>
       <w lemma="seventeen" pos="ord" xml:id="A32497-001-a-4540">Seventeenth</w>
       <w lemma="year" pos="n1" xml:id="A32497-001-a-4550">Year</w>
       <w lemma="of" pos="acp" xml:id="A32497-001-a-4560">of</w>
       <w lemma="our" pos="po" xml:id="A32497-001-a-4570">Our</w>
       <w lemma="reign" pos="n1" xml:id="A32497-001-a-4580">Reign</w>
       <pc unit="sentence" xml:id="A32497-001-a-4590">.</pc>
      </date>
     </dateline>
     <lb xml:id="A32497-e200"/>
     <w lemma="GOD" pos="nn1" xml:id="A32497-001-a-4600">GOD</w>
     <w lemma="save" pos="acp" xml:id="A32497-001-a-4610">SAVE</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-4620">THE</w>
     <w lemma="king" pos="n1" xml:id="A32497-001-a-4630">KING</w>
     <pc unit="sentence" xml:id="A32497-001-a-4640">.</pc>
    </closer>
   </div>
  </body>
  <back xml:id="A32497-e210">
   <div type="colophon" xml:id="A32497-e220">
    <p xml:id="A32497-e230">
     <hi xml:id="A32497-e240">
      <w lemma="OXFORD" pos="nn1" xml:id="A32497-001-a-4650">OXFORD</w>
      <pc xml:id="A32497-001-a-4660">:</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A32497-001-a-4670">Printed</w>
     <w lemma="by" pos="acp" xml:id="A32497-001-a-4680">by</w>
     <hi xml:id="A32497-e250">
      <w lemma="Leonard" pos="nn1" xml:id="A32497-001-a-4690">Leonard</w>
      <w lemma="Lichfeild" pos="nn1" xml:id="A32497-001-a-4700">Lichfeild</w>
     </hi>
     <w lemma="printer" pos="n1" xml:id="A32497-001-a-4710">Printer</w>
     <w lemma="to" pos="acp" xml:id="A32497-001-a-4720">to</w>
     <w lemma="the" pos="d" xml:id="A32497-001-a-4730">the</w>
     <w lemma="university" pos="n1" xml:id="A32497-001-a-4740">University</w>
     <pc xml:id="A32497-001-a-4750">,</pc>
     <w lemma="for" pos="acp" xml:id="A32497-001-a-4760">For</w>
     <hi xml:id="A32497-e260">
      <w lemma="John" pos="nn1" xml:id="A32497-001-a-4770">John</w>
      <w lemma="bill" pos="n1" xml:id="A32497-001-a-4780">Bill</w>
      <pc xml:id="A32497-001-a-4790">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A32497-001-a-4800">and</w>
     <hi xml:id="A32497-e270">
      <w lemma="Christopher" pos="nn1" xml:id="A32497-001-a-4810">Christopher</w>
      <w lemma="Barker" pos="nn1" xml:id="A32497-001-a-4820">Barker</w>
      <pc xml:id="A32497-001-a-4830">,</pc>
     </hi>
     <w lemma="printer" pos="n2" xml:id="A32497-001-a-4840">Printers</w>
     <w lemma="to" pos="acp" xml:id="A32497-001-a-4850">to</w>
     <w lemma="his" pos="po" xml:id="A32497-001-a-4860">His</w>
     <w lemma="majesty" pos="n1" xml:id="A32497-001-a-4870">Majesty</w>
     <pc xml:id="A32497-001-a-4880">,</pc>
     <hi xml:id="A32497-e280">
      <w lemma="n/a" pos="fla" xml:id="A32497-001-a-4890">Anno</w>
      <w lemma="dom." pos="ab" xml:id="A32497-001-a-4900">Dom.</w>
     </hi>
     <w lemma="1665." pos="crd" xml:id="A32497-001-a-4910">1665.</w>
     <pc unit="sentence" xml:id="A32497-001-a-4920"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
